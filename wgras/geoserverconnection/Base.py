from geoserver.catalog import Catalog
from . import geoserverConfig as config
from ..restmodels.UnpublishedRestModel import UnpublishedRestModel
import base64
import urllib2
import json



class GeoserverConnection(object):
    def __init__(self):
        self.GS_URI = config.GEOSERVER_URI
        self.GS_PW = config.GEOSERVER_PW
        self.GS_USER = config.GEOSERVER_USER
        self.REST_URL = "{}/rest".format(self.GS_URI)
        self.geoServerCatalog = Catalog(self.REST_URL, self.GS_USER, self.GS_PW)

    def getResource(self, resourceName, workspaceName):
        '''Gets a geoserver layer as a resource so that its properties can be changed'''
        if self.getWorkspace(workspaceName=workspaceName) is not None:
            return self.geoServerCatalog.get_resource(name=resourceName, workspace=workspaceName)
        return None

    def getResources(self):
        '''Gets a geoserver layer as a resource so that its properties can be changed'''
        return self.geoServerCatalog.get_resources()

    def getWorkspace(self, workspaceName):
        return self.geoServerCatalog.get_workspace(name=workspaceName)

    def publishLayer(self, resourceName, workspaceName, publish):
        resource = self.getResource(resourceName, workspaceName)
        if resource is not None:
            resource.enabled = publish
            self._saveResource(resource)
            return self.isPublished(resourceName=resourceName, workspaceName=workspaceName)
        return None

    def isPublished(self, resourceName, workspaceName):
        resource = self.getResource(resourceName=resourceName, workspaceName=workspaceName)
        if resource is not None:
            return True if (resource.enabled == "true") else False
        return None

    def getUnpublishedLayers(self):
        resources = self.getResources()
        layers = self._createUnpublishedLayersRepr(resources)
        return layers

    def getAttributes(self, layerName, workspaceName):
        uri = self.getResource(layerName, workspaceName).href.split('.xml')[0] + '.json'
        layer_req = self._makeRequest(uri)
        resource = json.loads(layer_req.read())
        attributes = resource["featureType"]["attributes"]["attribute"]
        found_attr = []
        for attr in attributes:
            print attr
            found_attr.append(
                {"name": attr["name"], "dataType": attr["binding"].split('.')[-1]})
        return {"layerName": layerName, "attributes": found_attr}

    def getLayerKeywords(self, layerName, workspaceName):
        resource = self.getResource(resourceName=layerName, workspaceName=workspaceName)
        if resource is not None:
            return {'keywords': resource.keywords}
        return None

    def updateLayerProperties(self, layerName, workspaceName, properties):
        resource = self.getResource(layerName, workspaceName)
        if resource is not None:
            if "layerTitle" and "keywords" and "layerInfo" and "enabled" in properties:
                resource.abstract = properties["layerInfo"]
                resource.title = properties["layerTitle"]
                resource.keywords = properties["keywords"]
                resource.enabled = properties["enabled"]
                self._saveResource(resource)
                return True
        return False

    def getStoresInWorkspace(self, workspaceName):
        catalog = self.geoServerCatalog
        return catalog.get_stores(workspace=workspaceName)

    def getLayersInStore(self, storeName, workspaceName):
        catalog = self.geoServerCatalog
        store = catalog.get_store(storeName, workspace=workspaceName)
        resources = store.get_resources()
        layers = []
        for r in resources:
            layers.append(catalog.get_layer(r.name))
        return layers





    #####################################
    #          Private methods          #
    #                                   #
    #####################################
    def _makeRequest(self, resturl):
        request = urllib2.Request(resturl)
        base64string = base64.encodestring('%s:%s' % (
            self.GS_USER, self.GS_PW)).replace('\n', '')
        request.add_header("Authorization", "Basic %s" % base64string)
        return urllib2.urlopen(request)

    def _saveResource(self, resource):
        catalog = self.geoServerCatalog
        catalog.save(resource)
        catalog.reload()

    def _createUnpublishedLayersRepr(self, resources):
        layers = []
        for r in resources:
            if r.enabled == 'false':
                model = self._getUnpublishedModel(r)
                layers.append(model.getUnpublishedLayersModel())
        return {'layers': layers}

    def _getUnpublishedModel(self, resource):
        return UnpublishedRestModel(
            resource.name,
            resource.title,
            resource.workspace.name,
            resource.abstract,
            resource.keywords,
            resource.enabled,
            resource.native_bbox[0],
            resource.native_bbox[2],
            resource.native_bbox[1],
            resource.native_bbox[3]
        )
