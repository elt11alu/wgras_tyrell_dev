from Base import GeoserverConnection
from .. import app
from owslib.wcs import WebCoverageService
from ..restmodels import WCSRestModel
from .. import TEMP_RASTER_FOLDER
import os


class WCSConnection(GeoserverConnection):
    def __init__(self):
        super(WCSConnection, self).__init__()
        self.service = WebCoverageService(url='{}/wcs'.format(self.GS_URI), version="1.0.0")

    def getAllWCSMetadata(self):
        capabilities = self.service
        layers = []

        for key, val in capabilities.contents.iteritems():
            model = self._getModel(val)
            layers.append(model.getDict())
        return {'layers': layers}

    def getWMSMetadata(self, name):
        capabilities = self.service
        val = capabilities.contents[name]
        model = self._getModel(val)
        return model.getDict()

    def downloadWCSLayer(self, layerName, crs='EPSG:4326', data_format='GeoTIFF', BBOX=None):
        wcsCrs = crs
        wcsFormat = data_format
        layerId = layerName
        if layerId not in self.service.contents.keys():
            return None
        cvg = self.service[layerId]
        bbox = cvg.boundingBoxWGS84
        self._createTempDirectory()



        if BBOX is None:
            BBOX = bbox



        width = cvg.grid.highlimits[0]
        height = cvg.grid.highlimits[1]
        resx, resy = self._getResolution(bbox, width, height)

        output = self.service.getCoverage(
            identifier=layerId,
            crs=wcsCrs,
            resx=resx,
            resy=resy,
            bbox=BBOX,
            format=wcsFormat)
        fileName = layerId.split(':')[1] + '{}'.format('.tif')
        filePath = os.path.join(TEMP_RASTER_FOLDER, fileName)
        f = open(filePath, 'wb')
        f.write(output.read())
        f.close()
        app.logger.info('Downloaded wcs into {}'.format(filePath))
        return filePath

    ####################################
    #          Private methods         #
    #                                  #
    ####################################

    def _getModel(self, val):
        return WCSRestModel.WCSRestModel(
            layerName=val.id,
            layerTitle=val.title,
            workspaceName=val.id.split(':')[0],
            layerInfo=val.abstract,
            bboxMinX=val.boundingBoxWGS84[0],
            bboxMinY=val.boundingBoxWGS84[1],
            bboxMaxX=val.boundingBoxWGS84[2],
            bboxMaxY=val.boundingBoxWGS84[3],
            keywords=val.keywords,
            gridHighLimits = val.grid.highlimits,
            gridLowLimits = val.grid.lowlimits,
            gridAxisLabels = val.grid.axislabels,
            gridDimensions = val.grid.dimension
        )

    def _getResolution(self, bbox, width, height):
        totBBox = bbox
        x_dim = totBBox[2] - totBBox[0]
        y_dim = totBBox[3] - totBBox[1]

        x_pixels = int(width)
        y_pixels = int(height)

        resx = x_dim / x_pixels
        resy = y_dim / y_pixels
        return resx, resy

    def _createTempDirectory(self):
        directory = TEMP_RASTER_FOLDER
        if not os.path.exists(directory):
            os.makedirs(directory)
