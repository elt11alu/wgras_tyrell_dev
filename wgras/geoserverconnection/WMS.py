from Base import GeoserverConnection
from owslib.wms import WebMapService
from ..restmodels import BaseRestModel


class WMSConnection(GeoserverConnection):
    def __init__(self):
        super(WMSConnection, self).__init__()
        self.service = WebMapService(url='{}/wms'.format(self.GS_URI), version='1.1.1')

    def getAllWMSMetadata(self):
        capabilities = self.service
        layers = []
        for key, val in capabilities.contents.iteritems():
            model = self._getModel(val)
            layers.append(model.getDict())
        return {'layers': layers}

    def getWMSMetadata(self, name):
        capabilities = WebMapService(url='{}/wms'.format(self.GS_URI), version='1.1.1')
        try:
            val = capabilities.contents[name]
            model = self._getModel(val)
            return model.getDict()
        except Exception:
            return None

    ####################################
    #          Private methods         #
    #                                  #
    ####################################

    def _getModel(self, val):
        return BaseRestModel.BaseRestModel(
            layerName=val.name,
            layerTitle=val.title,
            workspaceName=val.name.split(':')[0],
            layerInfo=val.abstract,
            bboxMinX=val.boundingBoxWGS84[0],
            bboxMinY=val.boundingBoxWGS84[1],
            bboxMaxX=val.boundingBoxWGS84[2],
            bboxMaxY=val.boundingBoxWGS84[3],
            keywords=val.keywords

        )
