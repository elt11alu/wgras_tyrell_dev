from .Base import GeoserverConnection
from ..import app

class GsWorkspace(GeoserverConnection):
    def __init__(self):
        super(GsWorkspace, self).__init__()

    def getWorkspaces(self):
        return self.geoServerCatalog.get_workspaces()

    def getWorkspaceNames(self):
        return [workspace.name for workspace in self.getWorkspaces()]

    def getWorkspacesAsDict(self):
        workspaces = self.getWorkspaces()
        list = []
        for ws in workspaces:
            list.append({'name': ws.name, 'href': ws.href})
        return list

    def createWorkspace(self, workspaceName):
        try:
            catalog = self.geoServerCatalog
            wsName = workspaceName
            namespace = "http://wgras/workspaces/" + wsName
            existingWorkspace = catalog.get_workspace(wsName)
            if existingWorkspace is None:
                catalog.create_workspace(
                    wsName, namespace)
                return catalog.get_workspace(wsName)
            return existingWorkspace
        except Exception as e:
            app.logger.warning('Exception in Workspaces.py function createWorkspace, with message: ' + e.message)
            return None

    def deleteWorkspace(self, workspaceName):
        try:
            catalog = self.geoServerCatalog
            workspace = catalog.get_workspace(workspaceName)
            for s in self.getStoresInWorkspace(workspaceName):
                for l in self.getLayersInStore(s.name, workspaceName):
                    catalog.delete(l)
                    catalog.reload()
                catalog.delete(s)
                catalog.reload()
            catalog.delete(workspace)
            catalog.reload()
            return True
        except Exception as e:
            return None



