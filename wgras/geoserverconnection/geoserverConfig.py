from.. import app

GEOSERVER_URI = app.config['GEOSERVER_URI']
GEOSERVER_USER = app.config['GEOSERVER_USER']
GEOSERVER_PW = app.config['GEOSERVER_PW']
