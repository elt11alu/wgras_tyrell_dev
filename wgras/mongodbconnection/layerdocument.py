from ..mongodbmodels import Tmpvectorlayer
from osgeo import ogr
import json
import os


class LayerDocument(object):
    def __init__(self):
        pass

    def shp_to_mongodb(self, file_path, remove_file=False):

        shp_driver = ogr.GetDriverByName("ESRI Shapefile")
        shp_data_source = shp_driver.Open(file_path, 0)
        shp_layer = shp_data_source.GetLayer()

        # get features from shape file, shp files does not distinguish between polygon/multipolygon
        # and linestring/multilinestring. For a unified geometry type def in the system we need to cast it here.
        number_of_feat = shp_layer.GetFeatureCount()
        spatial_ref = shp_layer.GetSpatialRef()
        if spatial_ref.IsProjected:
            spat_ref = spatial_ref.GetAttrValue('geogcs')
        shp_layer_def = shp_layer.GetLayerDefn()
        attributes = []
        for i in range(shp_layer_def.GetFieldCount()):
            field_name = shp_layer_def.GetFieldDefn(i).GetName()
            field_type_code = shp_layer_def.GetFieldDefn(i).GetType()
            field_type_name = shp_layer_def.GetFieldDefn(i).GetFieldTypeName(field_type_code)
            attributes.append({"attrName": field_name, "dataType": field_type_name})

        layer = Tmpvectorlayer(name='tmp_layer', shpfilepath=file_path, attributes=attributes,
                               numberoffeatures=number_of_feat)
        layer.save(safe=True)
        if remove_file:
            self.delete_input_shp(file_path=file_path)
        return str(layer.mongo_id)

    def get_nbr_of_attributes(self, document_id):
        tmp_layer = Tmpvectorlayer.query.get(document_id)
        if tmp_layer is not None:
            return len(tmp_layer.attributes)
        return None

    def get_nbr_of_features(self, document_id):
        tmp_layer = Tmpvectorlayer.query.get(document_id)
        if tmp_layer is not None:
            return tmp_layer.numberoffeatures
        return None

    def get_attributes_for_layer(self, document_id):
        tmp_layer = Tmpvectorlayer.query.get(document_id)
        if tmp_layer is not None:
            return tmp_layer.attributes
        return None

    def get_shp_file_path(self, document_id):
        tmp_layer = Tmpvectorlayer.query.get(document_id)
        if tmp_layer is not None:
            return tmp_layer.shpfilepath
        return None

    def delete_layer(self, document_id):
        tmp_layer = Tmpvectorlayer.query.get(document_id)
        tmp_layer.remove(safe=True)
        if Tmpvectorlayer.query.get(document_id) is None:
            return True
        return None

    def delete_input_shp(self, file_path):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        if os.path.exists(file_path):
            driver.DeleteDataSource(file_path)
