from . import mongo_db


class Tmpvectorlayer(mongo_db.Document):
    name = mongo_db.StringField()
    shpfilepath = mongo_db.StringField()
    numberoffeatures = mongo_db.IntField()
    attributes = mongo_db.ListField(mongo_db.DictField(value_type=mongo_db.StringField()))