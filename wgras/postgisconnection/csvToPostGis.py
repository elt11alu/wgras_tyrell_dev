from base import PostGisConnection
import csv
from osgeo import ogr, osr

class CSVToPostGis(PostGisConnection):

    def __init__(self):
        super(CSVToPostGis, self).__init__()

    def readCSV(self, filePath):
        reader = csv.DictReader(open(filePath, "rb"), quoting=csv.QUOTE_NONE)
        return reader.fieldnames

    def create_postgis_table_from_csv(self, filePath, tableName, latField, lonField):
        try:
            reader1 = csv.DictReader(open(filePath, "rb"), quoting=csv.QUOTE_NONE)
            fieldNames = reader1.fieldnames

            '''create the postgis layer'''
            connString = "PG: host=%s dbname=%s user=%s password=%s" % (self.DB_SERVER, self.DB_NAME, self.DB_USER, self.DB_PASSWORD)
            datastore = ogr.Open(connString)
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(4326)
            layer = datastore.CreateLayer(tableName, srs, ogr.wkbPoint, ['OVERWRITE=NO'])

            '''Check if we can determine data types of the fields'''
            rowcount = 0
            dataTypes = {}
            for row in reader1:
                if rowcount > 1:
                    break
                if rowcount == 1:
                    for field in fieldNames:
                        if field != latField and field != lonField:

                            if self._isint(row[field]) or self._isfloat(row[field]):
                                if self._isint(row[field]):
                                    dataTypes[field] = ogr.OFTInteger
                                elif self._isfloat(row[field]):
                                    dataTypes[field] = ogr.OFTReal
                            else:
                                dataTypes[field] = ogr.OFTString
                rowcount += 1


            layer.StartTransaction()
            '''create fields from csv'''
            for field in fieldNames:
                if field == latField:
                    layer.CreateField(ogr.FieldDefn('latitude', ogr.OFTReal))
                if field == lonField:
                    layer.CreateField(ogr.FieldDefn('longitude', ogr.OFTReal))
                elif field != latField and field != lonField:
                    layer.CreateField(ogr.FieldDefn(field, dataTypes[field]))

            reader2 = csv.DictReader(open(filePath, "rb"), quoting=csv.QUOTE_NONE)
            '''create geometry (points based on csv)'''
            for row in reader2:
                print row['lat']
                feature = ogr.Feature(layer.GetLayerDefn())
                for field in fieldNames:
                    if field == latField:
                        feature.SetField("latitude", row[field])
                    if field == lonField:
                        feature.SetField("longitude", row[field])
                    elif field != latField and field != lonField:
                        feature.SetField(field, row[field])

                wkt = "POINT(%f %f)" % (float(row[lonField]), float(row[latField]))

                point = ogr.CreateGeometryFromWkt(wkt)
                feature.SetGeometry(point)
                layer.CreateFeature(feature)
                feature.Destroy()
            layer.CommitTransaction()
            datastore.Destroy()
            return layer.GetGeomType()

        except Exception as e:
            print 'Exception in csvToPostGis.py function uploadToPostgis ', e.message
            return None

    #####################################
    #          Private methods          #
    #                                   #
    #####################################
    def _isfloat(self, x):
        try:
            a = float(x)
        except ValueError:
            return False
        else:
            return True

    def _isint(self, x):
        try:
            a = float(x)
            b = int(a)
        except ValueError:
            return False
        else:
            return a == b





