from base import PostGisConnection
from osgeo import ogr, osr
from .. import app
from ..mongodbmodels import Tmpvectorlayer
from ..service.mongodbservice import MongoDBService


class MongodbToPostgis(PostGisConnection):

    def __init__(self):
        super(MongodbToPostgis, self).__init__()
        self.connString = "PG: host=%s dbname=%s user=%s password=%s" % (self.DB_SERVER, self.DB_NAME, self.DB_USER, self.DB_PASSWORD)

    def get_shp_file_path(self, mongo_doc_id):
        try:
            layer = Tmpvectorlayer.query.get(mongo_doc_id)
            shp_path = layer.shpfilepath

            # Deleting document in mongodb
            MongoDBService().delete_layer(document_id=mongo_doc_id)

            return shp_path
        except Exception as e:
            app.logger.info('Exception in mongodb_to_posgis, function create_postgis_table_from_mongodb_doc: {}'.format(e.message))
            return None
        finally:
            MongoDBService().delete_layer(document_id=mongo_doc_id)