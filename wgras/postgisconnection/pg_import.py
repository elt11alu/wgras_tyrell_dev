from base import PostGisConnection
from osgeo import ogr, osr
from .. import app
import os


class PostGisImport(PostGisConnection):
    def __init__(self):
        super(PostGisImport, self).__init__()

    #############################################
    #      Create table from different formats  #
    #      supported: .shp, .geojson            #
    #############################################

    def create_table_from_shp(self, path_to_shp, table_name, schema, srs):
        """
        :param path_to_shp: string path to the shape file, including .shp extension
        :param table_name: string table name, upper case will be set to lower case
        :param schema: string schema where to create the table
        :param srs: integer srs, of the shape file, only EPSG:4326 is currently supported
        :return: string layer name if successful creation else None
        """
        driver_name = "Esri Shapefile"
        return self._create_table(driver_name=driver_name, table_name=table_name, d_source=path_to_shp, schema=schema,
                                  srs=srs)

    def create_table_from_geojson(self, geojson, table_name, schema, srs):
        """
        :param geojson: string path to geojson file or string valid GeoJSON
        :param table_name: string table name, upper case will be set to lower case
        :param schema: string schema where to create the table
        :param srs: integer srs, of the shape file, only EPSG:4326 is currently supported
        :return: string layer name if successful creation else None
        """
        driver_name = "GeoJSON"
        return self._create_table(driver_name=driver_name, table_name=table_name, d_source=geojson, schema=schema,
                                  srs=srs)

    #############################################
    #      Create empty geometry table          #
    #                                           #
    #############################################

    def create_empty_point_table(self, table_name, schema_name, fields):
        """
        :param table_name: string table name, upper case will be set to lower case
        :param schema_name: string schema where to create the table
        :param fields: array with fields {name:name, attributeType:attributeType}
        :return: True if created, else False
        """
        options = ['SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbPoint
        return self._create_empty_table(table_name=table_name, fields=fields, ogr_geom_type=geom_type, options=options)

    def create_empty_multipoint_table(self, table_name, schema_name, fields):
        """
        :param table_name: string table name, upper case will be set to lower case
        :param schema_name: string schema where to create the table
        :param fields: array with fields {name:name, attributeType:attributeType}
        :return: True if created, else False
        """
        options = ['SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbMultiPoint
        return self._create_empty_table(table_name=table_name, fields=fields, ogr_geom_type=geom_type, options=options)

    def create_empty_line_table(self, table_name, schema_name, fields):
        """
        :param table_name: string table name, upper case will be set to lower case
        :param schema_name: string schema where to create the table
        :param fields: array with fields {name:name, attributeType:attributeType}
        :return: True if created, else False
        """
        options = ['SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbLineString
        return self._create_empty_table(table_name=table_name, fields=fields, ogr_geom_type=geom_type, options=options)

    def create_empty_multiline_table(self, table_name, schema_name, fields):
        """
        :param table_name: string table name, upper case will be set to lower case
        :param schema_name: string schema where to create the table
        :param fields: array with fields {name:name, attributeType:attributeType}
        :return: True if created, else False
        """
        options = ['SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbMultiLineString
        return self._create_empty_table(table_name=table_name, fields=fields, ogr_geom_type=geom_type, options=options)

    def create_empty_polygon_table(self, table_name, schema_name, fields):
        """
        :param table_name: string table name, upper case will be set to lower case
        :param schema_name: string schema where to create the table
        :param fields: array with fields {name:name, attributeType:attributeType}
        :return: True if created, else False
        """
        options = ['SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbPolygon
        return self._create_empty_table(table_name=table_name, fields=fields, ogr_geom_type=geom_type, options=options)

    def create_empty_multipolygon_table(self, table_name, schema_name, fields):
        """
        :param table_name: string table name, upper case will be set to lower case
        :param schema_name: string schema where to create the table
        :param fields: array with fields {name:name, attributeType:attributeType}
        :return: True if created, else False
        """
        options = ['SCHEMA={}'.format(schema_name)]
        geom_type = ogr.wkbMultiPolygon
        return self._create_empty_table(table_name=table_name, fields=fields, ogr_geom_type=geom_type, options=options)

    #############################################
    #      Private functions                    #
    #                                           #
    #############################################

    def _create_table(self, driver_name, table_name, d_source, schema='spatial', srs=4326):
        """
        :param driver_name: string driver name, currently GeoJSON and Esri Shapefile are supported
        :param table_name: string table name
        :param d_source: string path to file or string valid GeoJSON
        :param schema: string schema
        :param srs: integer srs
        :return: string layer name if successful creation else None
        """

        try:
            driver = ogr.GetDriverByName(driver_name)
            in_data_source = driver.Open(d_source)

            if in_data_source is not None:
                in_layer = in_data_source.GetLayer()
                postgis_data_source = ogr.Open(self.ogrConnString)
                if postgis_data_source is not None:

                    # Creating geom type for postGis table, Shapefiles contain a strange mix of geometries
                    in_layer_geom_type = in_layer.GetGeomType()
                    if driver_name == "Esri Shapefile":
                        if in_layer_geom_type == ogr.wkbPolygon:
                            in_layer_geom_type = ogr.wkbMultiPolygon
                        elif in_layer_geom_type == ogr.wkbLineString:
                            in_layer_geom_type = ogr.wkbMultiLineString

                    # Creating the postgis table
                    srs_def = osr.SpatialReference()
                    srs_def.ImportFromEPSG(srs)
                    postgis_layer = postgis_data_source.CreateLayer(str(table_name), srs_def, in_layer_geom_type,
                                                                    ['SCHEMA={}'.format(schema)])

                    # Adding fields from in_layer to postgis table
                    in_layer_def = in_layer.GetLayerDefn()
                    for i in range(0, in_layer_def.GetFieldCount()):
                        field_def = in_layer_def.GetFieldDefn(i)
                        postgis_layer.CreateField(field_def)

                    # Inserting geometry from in_layer to postgis layer
                    for feature in in_layer:
                        geom = feature.GetGeometryRef()

                        # casting geomtypes, many shapefiles contain both polygon and multipolygon etc.
                        if driver_name == "Esri Shapefile":
                            if geom.GetGeometryType() == ogr.wkbPolygon:
                                geom = ogr.ForceToMultiPolygon(geom)
                                feature.SetGeometryDirectly(geom)
                            elif geom.GetGeometryType() == ogr.wkbLineString:
                                geom = ogr.ForceToMultiLineString(geom)
                                feature.SetGeometryDirectly(geom)

                        postgis_layer.CreateFeature(feature)
                        feature.Destroy()
                    layer_name = postgis_layer.GetName()
                    if driver_name == "Esri Shapefile":
                        self.delete_shp_files(path=d_source)
                    return layer_name
                return None
            return None
        except Exception as e:
            app.logger.warning(e.message)
            return None

    def _create_empty_table(self, table_name, fields, ogr_geom_type, options):
        """
        :param table_name: string table name to create, will be set to lower case
        :param fields: fields to create on the table {name: string name, attributeType: string attributeType}
        :param ogr_geom_type: ogr.wkbPoint, ogr.wkbMultiPoint, ogr.wkbLineString etc..
        :param options: array options e.g. [OVERWRITE=NO SCHEMA=schema_name]
        :return: true if success, false otherwise
        """
        try:
            postgis_data_source = ogr.Open(self.ogrConnString)
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(4326)
            postgis_layer = postgis_data_source.CreateLayer(str(table_name), srs, ogr_geom_type, options)

            for field in fields:
                field_def = self._get_field_def(str(field['name']), str(field['attributeType']))
                if field_def is not None:
                    postgis_layer.StartTransaction()
                    postgis_layer.CreateField(field_def)
                    postgis_layer.CommitTransaction()
            return True
        except Exception as e:
            app.logger.warning(e.message)
            return False

    def _get_field_def(self, field_name, field_type):
        app.logger.info('Attempting to create field: {} with type: {}'.format(field_name, field_type))
        if field_type == 'String':
            return ogr.FieldDefn(field_name, ogr.OFTString)
        elif field_type == 'Integer':
            return ogr.FieldDefn(field_name, ogr.OFTInteger)
        elif field_type == 'Real':
            return ogr.FieldDefn(field_name, ogr.OFTReal)
        elif field_type == 'Date':
            return ogr.FieldDefn(field_name, ogr.OFTDate)
        else:
            app.logger.warning('Found unsupported attribute type: {}, with name: {}'.format(field_type, field_name))
            return None

    def _get_ogr_field_types(self):
        return {

            "ogr.OFTInteger": 0,
            "ogr.OFTIntegerList": 1,
            "ogr.OFTReal": 2,
            "ogr.OFTRealList": 3,
            "ogr.OFTString": 4,
            "OFTStringList": 5,
            "ogr.OFTWideString": 6,
            "ogr.OFTWideStringList": 7,
            "ogr.OFTBinary": 8,
            "ogr.OFTDate": 9,
            "ogr.OFTTime": 10,
            "ogr.OFTDateTime": 11,
            "ogr.OFTInteger64": 12,
            "ogr.OFTInteger64List": 13,
            "ogr.OFTMaxType": 13
        }

    def delete_shp_files(self, path):
        shp_extensions = ('dbf', 'prj', 'shp', 'shx')
        file_name = os.path.splitext(path)[0]
        for shp_ext in shp_extensions:
            try:
                os.remove('{}{}{}'.format(file_name, '.', shp_ext))
            except OSError:
                pass

