from base import PostGisConnection
from osgeo import ogr, osr
from .. import app


class PostGisInsert(PostGisConnection):
    def __init__(self):
        super(PostGisInsert, self).__init__()

    def insert_geojson_geom(self, geojson_geom, table_name, schema):
        """
        :param geojson_geom: string valid GeoJSON
        :param table_name: string table name where to insert geometry
        :param schema: string schema where to find the table
        :return: True if successful insertion, None otherwise
        """
        try:
            postgis_data_source = ogr.Open(self.ogrConnString, 1)
            if postgis_data_source is not None:
                layer = postgis_data_source.GetLayerByName(table_name)
                if layer is not None:
                    layer_geom_type = layer.GetGeomType()
                    geom = ogr.CreateGeometryFromJson(geojson_geom)
                    geojson_geom_type = geom.GetGeometryType()

                    if layer_geom_type == geojson_geom_type:
                        layer_def = layer.GetLayerDefn()
                        feature = ogr.Feature(layer_def)
                        feature.SetGeometry(geom)
                        layer.StartTransaction()
                        layer.CreateFeature(feature)
                        layer.CommitTransaction()
                        return True
                    return None
                return None
            return None
        except Exception as e:
            app.logger.warning(e.message)

    def insert_geojson_geoms(self, geojson_geom_arr, table_name, schema):
        """
        :param geojson_geom_arr: array with valid GeoJSON geometries
        :param table_name: string table name where to insert geometry
        :param schema: string schema where to find the table
        :return: True if successful insertion, None otherwise
        """
        try:
            postgis_data_source = ogr.Open(self.ogrConnString, 1)
            if postgis_data_source is not None:
                layer = postgis_data_source.GetLayerByName(table_name)
                if layer is not None:
                    layer_geom_type = layer.GetGeomType()
                    layer_def = layer.GetLayerDefn()
                    for geojson_geom in geojson_geom_arr:
                        geom = ogr.CreateGeometryFromJson(geojson_geom)
                        geojson_geom_type = geom.GetGeometryType()

                        if layer_geom_type == geojson_geom_type:
                            feature = ogr.Feature(layer_def)
                            feature.SetGeometry(geom)
                            layer.StartTransaction()
                            layer.CreateFeature(feature)
                            layer.CommitTransaction()
                    postgis_data_source.Destroy()
                    return True
                return None
            return None
        except Exception as e:
            app.logger.warning(e.message)
            return None

    def insert_geojson_feature_coll(self, geojson_feature_coll, table_name, schema):
        """
        :param geojson_feature_coll: string valid GeoJSON feature collection
        :param table_name: string table name where to insert features
        :param schema: string schema
        :return: True if successful insertion, None otherwise
        """
        try:
            postgis_data_source = ogr.Open(self.ogrConnString, 1)
            if postgis_data_source is not None:
                postgis_layer = postgis_data_source.GetLayerByName(table_name)
                if postgis_layer is not None:
                    postgis_layer_geom_type = postgis_layer.GetGeomType()
                    postgis_layer_def = postgis_layer.GetLayerDefn()
                    geojson_driver = ogr.GetDriverByName('GeoJSON')
                    geojson_data_source = geojson_driver.Open(geojson_feature_coll)
                    if geojson_data_source is not None:
                        geojson_layer = geojson_data_source.GetLayer()

                        if geojson_layer is not None:
                            geojson_layer_def = geojson_layer.GetLayerDefn()
                            geojson_layer_geom_type = geojson_layer.GetGeomType()
                            if postgis_layer_geom_type == geojson_layer_geom_type:
                                postgis_layer.StartTransaction()
                                for geojson_feature in geojson_layer:
                                    geom = geojson_feature.GetGeometryRef()
                                    postgis_feature = ogr.Feature(postgis_layer_def)
                                    postgis_feature.SetGeometry(geom)
                                    # setting field values
                                    for ii in range(0, postgis_layer_def.GetFieldCount()):
                                        postgis_field_def = postgis_layer_def.GetFieldDefn(ii)
                                        postgis_field_name = postgis_field_def.GetName()
                                        for yy in range(0, geojson_layer_def.GetFieldCount()):
                                            json_field_def = geojson_layer_def.GetFieldDefn(yy)
                                            json_field_name = json_field_def.GetName()
                                            if postgis_field_name == json_field_name:
                                                field_name = postgis_layer_def.GetFieldDefn(ii).GetNameRef()
                                                field_value = geojson_feature.GetField(yy)
                                                postgis_feature.SetField(field_name, field_value)
                                    postgis_layer.CreateFeature(postgis_feature)
                                    postgis_feature.Destroy()
                                return True
                            return None
                        return None
                    return None
                return None
            return None
        except Exception as e:
            app.logger.warning(e.message)
            return None
