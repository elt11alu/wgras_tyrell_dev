#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# ************************************************************************
#
#               GNU LESSER GENERAL PUBLIC LICENSE
#
#  This program is free software: you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License
#  as published by the Free Software Foundation, either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  amosa_23APR_finished_no_constraint.py     : Implementation in Python of the AMOSA Multi-objective
#                 Optimisation Algorithm
#
#  Article      : Bandyopadhyay, S., Saha, S., Maulik, U., & Deb, K. (2008).
#                 A Simulated Annealing-Based Multiobjective Optimization Algorithm: AMOSA.
#                 IEEE Transactions on Evolutionary Computation, 12(3), 269–283.
#                 http://doi.org/10.1109/TEVC.2007.900837
#
#  Coded by     : José Ignacio Díaz Gonźalez <jdiaz@felino.cl>
#
# ********************************************************************************************************************

import math
import time
import random
import copy
import csv
import sys
import colorsys
import simplekml
import numpy
import os

from routing import routing
from deap import base, creator, tools
from scipy import spatial, cluster
from .. import celery

#celery = Celery('email.celery',
#                broker='redis://localhost:6379/0',
#                backend='redis://localhost:6379/0')


@celery.task(name='evacplanning', bind=True)
def startEvacPlanning(self, city):
    # DATASET_METHOD (string): Defines how the input data will be read by the script. To read the data from a postgreSQL
    # database set the variable as 'database' and to read from CSV files set it as 'file'. In case of using the
    # 'database' method the connection parameters to the database server must be changed in 'routing' function of this
    # script and in case of using the 'file' method the CSV must be in the same folder.
    DATASET_METHOD = 'file'

    # CONSTRAINT (boolean): Defines if the algorithm will consider some tye of spatial restriction for the analysis.
    # When it is False there is no constraints for the analysis and each building in the dataset could be allocated to
    # any safe area (which is the worse scenario). If its value is True the allocation will be restricted according to
    # to the available methods (see next paragraph).
    CONSTRAINT = True

    # CONSTRAINT_METHOD (string): If the CONSTRAINT variable was set as True this variable defines the type of
    # constraint to be used (otherwise it is ignored). If it is defined as 'distance' each building can be allocated
    # only to the safes areas within certain threshold around it (if there are no safe areas within that threshold the
    # closest one is selected). If this method is set as 'quantity' each building can be allocated to a fixed number of
    # safe areas (sorted by nearness).
    CONSTRAINT_METHOD = 'distance'

    # CONSTRAINT_VALUE (integer): If the CONSTRAINT variable was set as True this variable defines the threshold to be
    # used for the selected method (otherwise it is ignored). If the selected method was 'distance' this value sets the
    # threshold in meters to be used to select the potential safe areas for each building. If the selected method was
    # 'quantity' this value sets the amount of safe areas to be considered for each building (chosen by nearness) .
    CONSTRAINT_VALUE = 500

    # LOOPS (integer): Defines the number of iterations for the main AMOSA process. The higher the value the higher will
    # be the level of optimisation for each objective function (Capacity and Distance) but it will increase the
    # processing time.
    LOOPS = 22
    celerytask = self

    celerytask.update_state(state='PROGRESS',
                            meta={'current': 0, 'total': 0,
                                  'status': 'Process started.'})
    start = time.time()
    buildings, areas, geometries = import_data(method=DATASET_METHOD, city=city.lower())
    solutions = amosa(celerytask,buildings, areas, LOOPS, constraint=CONSTRAINT, method=CONSTRAINT_METHOD, value=CONSTRAINT_VALUE)
    print '\n5) Printing the archive...\n'
    for element in solutions:
        print element.fitness, element
    end = time.time()
    ptime = end - start
    m, s = divmod(ptime, 60)
    h, m = divmod(m, 60)
    print '\nSolutions in archive (AMOSA):', len(solutions)
    print '\nExecution time:', "%02d:%02d:%02d" % (h, m, s)
    if CONSTRAINT is False:
        settings_str = '_unconstrained_' + str(LOOPS) + 'runs'
    else:
        settings_str = '_constrained_' + CONSTRAINT_METHOD + '_' + str(CONSTRAINT_VALUE) + '_' + str(LOOPS) + 'runs'
    kml_prefix = 'amosa' + settings_str

    geoms = create_routes(celerytask,solutions, buildings, areas, prefix=kml_prefix)

    # *****************************************************************************************************************

    fitnesses_table = create_graphs(solutions)[0]  # A list with all the 'coordinates' to create the graph
    minimum_f1 = create_graphs(solutions)[1]  # The coordinates of the solution that better minimises Capacity function
    minimum_f2 = create_graphs(solutions)[2]  # The coordinates of the solution that better minimises Distance function
    trade_off = create_graphs(solutions)[3]  # The coordinates of a solution around the center of the first Pareto front

    print minimum_f1

    return {'current': 100, 'total': 100, 'status': 'Task completed!',
            'result': geoms}


def import_data(method='file', city='lund'):  # Load spatial data from postgreSQL database or CSV files
    this_dir = os.path.split(__file__)[0]
    buildings_data, areas_data, geometries_data = [], [], []
    if method == 'file':
        with open(os.path.join(this_dir, "data",city, "buildings.csv"), 'rb') as f:
            reader = csv.reader(f)
            buildings_data = [[float(value) for value in row] for row in reader]
        with open(os.path.join(this_dir, "data", city,"areas.csv"), 'rb') as f:
            reader = csv.reader(f)
            areas_data = [[float(value) for value in row] for row in reader]
    elif method == 'database':
        buildings_data, areas_data, geometries_data = routing()
    return buildings_data, areas_data, geometries_data


def perturb(a, buildings_data, areas_data, allocation_data):
    new_solution = copy.deepcopy(a)
    for i in range(2):
        while a == new_solution:
            random_index = random.randrange(len(new_solution))
            new_solution[random_index] = random.choice(allocation_data[random_index])
    evaluate(new_solution, buildings_data, areas_data)
    return new_solution


def f1(a, buildings_data, areas_data):  # Capacity function
    fitness01 = 0
    for i in range(max(a)):
        population_counter = 0.0
        for j in range(len(a)):
            if a[j] == i + 1:
                population_counter += buildings_data[j][3]
        fitness01 += (abs((population_counter / areas_data[i][3]) - 1)) / 10
    return fitness01


def f2(a, buildings_data):  # Distance function
    fitness02 = 0
    for k in range(len(a)):
        fitness02 += (buildings_data[k][a[k] + 3] * buildings_data[k][3]) / 10000000  # Distance x Population
    return fitness02  # Expressed in 10,000 (km·pers)


def evaluate(a, buildings_data, areas_data):
    a.fitness.values = (f1(a, buildings_data, areas_data), f2(a, buildings_data))
    return a


def clustering(source, size):
    fitness_list = [solution.fitness.values for solution in source]
    distance_matrix = spatial.distance.pdist(fitness_list)
    cluster_list = cluster.hierarchy.single(distance_matrix)
    flat_cluster = list(cluster.hierarchy.fcluster(cluster_list, size, criterion='maxclust'))
    cluster_members = [[j for j in range(len(flat_cluster)) if flat_cluster[j] == i + 1] for i in range(size)]
    to_remove = []
    for group in cluster_members:
        while len(group) > 1:
            selected_index = group[random.randrange(len(group))]
            to_remove.append(selected_index)
            group.remove(selected_index)
    for i in sorted(to_remove, reverse=True):
        del source[i]


def allocation_scheme(buildings_data, constraint, method, value):
    allocation_data = []
    if constraint is False:
        allocation_data = [[i + 1 for i in range(len(building) - 4)] for building in buildings_data]
    else:
        if method == 'distance':
            allocation_data = [[(b.index(_) - 3) for _ in b[4:] if _ <= value] for b in buildings_data]
            for i in range(len(allocation_data)):
                if len(allocation_data[i]) == 0:
                    allocation_data[i] = [buildings_data[i].index(min(buildings_data[i][4:])) - 3]
        elif method == 'quantity':
            if value > len(buildings_data[0]) - 4:
                value = len(buildings_data[0]) - 4
            for b in buildings_data:
                area_indexes = list(numpy.unique(b[4:], True, False, False))
                area_indexes = area_indexes[1][0:value]
                area_indexes = sorted([(area + 1) for area in area_indexes])
                allocation_data.append(area_indexes)
    return allocation_data


def update_solution(solution, allocation_data):
    solution[:] = [random.choice(building) for building in allocation_data]
    return solution


def create_archive(celerytask,soft_limit, hard_limit, buildings_data, areas_data, allocation_data):
    gamma = 2
    hill_climbing = 20  # Number of steps for refining the initial random solutions

    creator.create("Fitness", base.Fitness, weights=(-1.0, -1.0))
    creator.create("Candidate", list, fitness=creator.Fitness)
    toolbox = base.Toolbox()

    toolbox.register("cnd_values", random.randint, 0, 0)
    toolbox.register("cnd_solution", tools.initRepeat, creator.Candidate, toolbox.cnd_values, n=len(buildings_data))
    toolbox.register("cnd_set", tools.initRepeat, list, toolbox.cnd_solution, n=None)

    msg1= '\n1) Creating and refining initial random solutions...'
    celerytask.update_state(state='PROGRESS',
                      meta={'current': 0, 'total': 0,
                            'status': msg1})
    initial_solutions = toolbox.cnd_set(n=gamma * soft_limit)
    for solution in initial_solutions:  # Refine initial solutions using simple hill-climbing technique
        update_solution(solution, allocation_data)
        evaluate(solution, buildings_data, areas_data)
        for i in range(hill_climbing):
            potential_solution = perturb(solution, buildings_data, areas_data, allocation_data)
            if potential_solution.fitness.dominates(solution.fitness):
                solution = copy.deepcopy(potential_solution)
    msg2= '  ', len(initial_solutions), 'random solutions were created and refined.'
    print msg2
    celerytask.update_state(state='PROGRESS',
                            meta={'current': 0, 'total': 0,
                                  'status': msg2})

    clustering(initial_solutions, hard_limit)

    print '\n2) Creating archive...'
    initial_archive = []
    for solution in initial_solutions:  # Look for the non-dominated solutions to add them to the archive
        is_dominated = 0
        for i in range(len(initial_solutions)):
            if initial_solutions[i].fitness.dominates(solution.fitness):
                is_dominated += 1
        if is_dominated == 0:
            initial_archive.append(solution)
    print '   Archive created with', len(initial_archive), 'non-dominated solutions.\n'

    if len(initial_archive) > hard_limit:
        print '   Clustering...'
        clustering(initial_archive, hard_limit)

    return initial_archive


def remove_points(target, points):
    for i in sorted(points, reverse=True):
        del target[i]


def check_domination(a, archive):
    list_dominated, list_nodomination, list_dominating = [], [], []
    for solution in archive:
        if solution.fitness.dominates(a.fitness) is True:
            list_dominated.append(archive.index(solution))
        elif a.fitness.dominates(solution.fitness) is False:
            list_nodomination.append(archive.index(solution))
        elif a.fitness.dominates(solution.fitness) is True:
            list_dominating.append(archive.index(solution))
    return list_dominated, list_nodomination, list_dominating


def amount_of_domination(a, b, c, archive):
    f1_values = [a.fitness.values[0], b.fitness.values[0]]
    f2_values = [a.fitness.values[1], b.fitness.values[1]]
    for solution in archive:
        f1_values.append(solution.fitness.values[0])
        f2_values.append(solution.fitness.values[1])
    range_f1 = max(f1_values) - min(f1_values)
    range_f2 = max(f2_values) - min(f2_values)

    summation, delta, index = 0, 0, None

    if a.fitness.dominates(b.fitness) is True:  # Case 1
        if range_f1 != 0 and range_f2 != 0:
            for i in c:
                if f1_values[i + 2] != f1_values[1] and f2_values[i + 2] != f2_values[1]:
                    summation += (abs(f1_values[i + 2] - f1_values[1]) / range_f1) * (
                    abs(f2_values[i + 2] - f2_values[1]) / range_f2)
            summation += (abs(f1_values[0] - f1_values[1]) / range_f1) * (abs(f2_values[0] - f2_values[1]) / range_f2)
            delta = summation / (len(c) + 1)
        else:
            delta = 0

    elif b.fitness.dominates(a.fitness) is False:  # Case 2a
        for i in c:
            if f1_values[i + 2] != f1_values[1] and f2_values[i + 2] != f2_values[1]:
                summation += (abs(f1_values[i + 2] - f1_values[1]) / range_f1) * (
                abs(f2_values[i + 2] - f2_values[1]) / range_f2)
        delta = summation / len(c)

    elif b.fitness.dominates(a.fitness) is True:  # Case 3a
        values, indexes = [], []
        for i in c:
            if f1_values[i + 2] != f1_values[1] and f2_values[i + 2] != f2_values[1]:
                delta = (abs(f1_values[i + 2] - f1_values[1]) / range_f1) * (
                abs(f2_values[i + 2] - f2_values[1]) / range_f2)
                values.append(delta)
                indexes.append(i)
        if len(values) == 0:
            delta = 0
            index = c[0]
        else:
            delta = min(values)
            index = values.index(delta)

    return delta, index


def create_routes(celerytask, archive, buildings_data, areas_data, prefix, n=3):
    celerytask.update_state(state='PROGRESS',
                            meta={'current': 0, 'total': 0,
                                  'status': 'Building geometries'})
    geoms = []
    if n > len(archive):
        n_routes = len(archive)
    else:
        n_routes = n
    solutions_set = create_graphs(archive)[4:7]
    for i in range(n_routes):
        star_route = simplekml.Kml()
        solution_index = solutions_set[i]
        selected_solution = archive[solution_index]
        n_colors = max(selected_solution)
        hsv_colors = [(num*1.0/n_colors, 1, 1) for num in range(n_colors)]
        rgb_colors = map(lambda x: colorsys.hsv_to_rgb(*x), hsv_colors)
        colors = [[int(cvalue * 255) for cvalue in list(tup)] for tup in rgb_colors]
        for j in range(len(selected_solution)):
            building_coord = (buildings_data[j][1], buildings_data[j][2])
            area_coord = (areas_data[selected_solution[j] - 1][1], areas_data[selected_solution[j] - 1][2])
            lname = 'b' + str(j + 1) + '_a' + str(selected_solution[j])
            ldesc = 'Building ' + str(j + 1) + ' is assigned to safe area ' + str(selected_solution[j])
            ls = star_route.newlinestring(name=lname, description=ldesc, coords=[building_coord, area_coord])
            ls.style.linestyle.width = 1
            ls.style.labelstyle.scale = 0
            lcolor = colors[selected_solution[j] - 1]
            ls.style.linestyle.color = simplekml.Color.rgb(lcolor[0], lcolor[1], lcolor[2])
            ls.altitudemode = simplekml.AltitudeMode.clamptoground
        for k in range(len(areas_data)):
            pname = 'sa' + str(k + 1)
            pdesc = 'Safe area ' + str(k + 1)
            area_coord = (areas_data[k][1], areas_data[k][2])
            pt = star_route.newpoint(name=pname, description=pdesc, coords=[area_coord])

        # time_stamp = time.strftime("%Y%m%d_%H%M%S")
        id_string = ''
        if i == 0:
            id_string = '_minimum_f1'
        elif i == 1:
            id_string = '_minimum_f2'
        elif i == 2:
            id_string = '_trade_off'
        # filename = time_stamp + '_star_' + prefix + id_string + '.kml'
        #print star_route.kml()
        #star_route.save(filename)
        geoms.append({'solutionDesc': id_string, 'geom': star_route.kml()})
        #del star_route
    return geoms


def create_graphs(archive):
    fitness_data = [solution.fitness.values for solution in archive]
    f1_values = [fitness[0] for fitness in fitness_data]
    f2_values = [fitness[1] for fitness in fitness_data]
    index_min_f1 = f1_values.index(min(f1_values))
    index_min_f2 = f2_values.index(min(f2_values))
    min_f1 = fitness_data[index_min_f1]
    min_f2 = fitness_data[index_min_f2]
    trade_off_avg = sum(f1_values)/len(f1_values)
    index_trade_off = next(f1_values.index(fitness) for fitness in sorted(f1_values) if fitness >= trade_off_avg)
    t_off = fitness_data[index_trade_off]
    return fitness_data, min_f1, min_f2, t_off, index_min_f1, index_min_f2, index_trade_off


def amosa( celerytask,buildings_data, areas_data, loops, constraint=True, method='distance', value=500):
    tmax = 100
    tmin = 0.0001
    hard_limit = 50
    soft_limit = 75
    iterations = loops
    alpha = 0.9

    temperature = tmax
    allocations = allocation_scheme(buildings_data, constraint, method, value)
    archive = create_archive(celerytask, soft_limit, hard_limit, buildings_data, areas_data, allocations)
    current_point = copy.deepcopy(archive[random.randrange(len(archive))])

    while temperature > tmin:
        tinit = temperature
        msg = 'Annealing... (cooling from ' + str(round(tinit, 4)) + '° to ' + str(tmin) + '°)'
        sys.stdout.write('\r3) Annealing... (cooling from ' + str(round(tinit, 4)) + '° to ' + str(tmin) + '°)')
        celerytask.update_state(state='PROGRESS',
                          meta={'current': 0, 'total': 0,
                                'status': msg})
        sys.stdout.flush()

        for i in range(iterations):

            new_point = perturb(current_point, buildings_data, areas_data, allocations)
            if current_point.fitness.dominates(new_point.fitness) is True:  # Case 1
                dominated = check_domination(new_point, archive)[0]
                delta_domavg = amount_of_domination(current_point, new_point, dominated, archive)[0]
                probability = 1 / (1 + math.exp(delta_domavg * temperature))
                if random.random() < probability:
                    current_point = copy.deepcopy(new_point)

            elif new_point.fitness.dominates(current_point.fitness) is False:  # Case 2
                dominated, nodomination, dominating = check_domination(new_point, archive)
                if len(dominated) >= 1:
                    delta_domavg = amount_of_domination(current_point, new_point, dominated, archive)[0]
                    probability = 1 / (1 + math.exp(delta_domavg * temperature))
                    if random.random() < probability:
                        current_point = copy.deepcopy(new_point)

                elif len(nodomination) == len(archive):
                    current_point = copy.deepcopy(new_point)
                    archive.append(new_point)
                    if len(archive) > soft_limit:
                        clustering(archive, hard_limit)

                elif len(dominating) >= 1:
                    current_point = copy.deepcopy(new_point)
                    archive.append(new_point)
                    remove_points(archive, dominating)

            elif new_point.fitness.dominates(current_point.fitness) is True:  # Case 3
                dominated, nodomination, dominating = check_domination(new_point, archive)
                if len(dominated) >= 1:
                    delta_dommin, min_index = amount_of_domination(current_point, new_point, dominated, archive)
                    probability = 1 / (1 + math.exp(-delta_dommin))
                    if random.random() < probability:
                        current_point = copy.deepcopy(archive[min_index])
                    else:
                        current_point = copy.deepcopy(new_point)

                elif len(nodomination) == len(archive):
                    try:
                        archive.remove(current_point)
                    except ValueError:
                        pass
                    finally:
                        current_point = copy.deepcopy(new_point)
                        archive.append(new_point)
                        if len(archive) > soft_limit:
                            clustering(archive, hard_limit)

                elif len(dominating) >= 1:
                    current_point = copy.deepcopy(new_point)
                    archive.append(new_point)
                    remove_points(archive, dominating)

        temperature *= alpha

    if len(archive) >= soft_limit:
        print '\n4) Clustering the archive...'
        clustering(archive, hard_limit)

    return archive

'''
if __name__ == "__main__":
    # DATASET_METHOD (string): Defines how the input data will be read by the script. To read the data from a postgreSQL
    # database set the variable as 'database' and to read from CSV files set it as 'file'. In case of using the
    # 'database' method the connection parameters to the database server must be changed in 'routing' function of this
    # script and in case of using the 'file' method the CSV must be in the same folder.
    DATASET_METHOD = 'file'

    # CONSTRAINT (boolean): Defines if the algorithm will consider some tye of spatial restriction for the analysis.
    # When it is False there is no constraints for the analysis and each building in the dataset could be allocated to
    # any safe area (which is the worse scenario). If its value is True the allocation will be restricted according to
    # to the available methods (see next paragraph).
    CONSTRAINT = True

    # CONSTRAINT_METHOD (string): If the CONSTRAINT variable was set as True this variable defines the type of
    # constraint to be used (otherwise it is ignored). If it is defined as 'distance' each building can be allocated
    # only to the safes areas within certain threshold around it (if there are no safe areas within that threshold the
    # closest one is selected). If this method is set as 'quantity' each building can be allocated to a fixed number of
    # safe areas (sorted by nearness).
    CONSTRAINT_METHOD = 'distance'

    # CONSTRAINT_VALUE (integer): If the CONSTRAINT variable was set as True this variable defines the threshold to be
    # used for the selected method (otherwise it is ignored). If the selected method was 'distance' this value sets the
    # threshold in meters to be used to select the potential safe areas for each building. If the selected method was
    # 'quantity' this value sets the amount of safe areas to be considered for each building (chosen by nearness) .
    CONSTRAINT_VALUE = 500

    # LOOPS (integer): Defines the number of iterations for the main AMOSA process. The higher the value the higher will
    # be the level of optimisation for each objective function (Capacity and Distance) but it will increase the
    # processing time.
    LOOPS = 20

    # *****************************************************************************************************************

    start = time.time()
    buildings, areas, geometries = import_data(method=DATASET_METHOD)
    solutions = amosa(buildings, areas, LOOPS, constraint=CONSTRAINT, method=CONSTRAINT_METHOD, value=CONSTRAINT_VALUE)
    print '\n5) Printing the archive...\n'
    for element in solutions:
        print element.fitness, element
    end = time.time()
    ptime = end - start
    m, s = divmod(ptime, 60)
    h, m = divmod(m, 60)
    print '\nSolutions in archive (AMOSA):', len(solutions)
    print '\nExecution time:', "%02d:%02d:%02d" % (h, m, s)
    if CONSTRAINT is False:
        settings_str = '_unconstrained_' + str(LOOPS) + 'runs'
    else:
        settings_str = '_constrained_' + CONSTRAINT_METHOD + '_' + str(CONSTRAINT_VALUE) + '_' + str(LOOPS) + 'runs'
    kml_prefix = 'amosa' + settings_str
    create_routes(solutions, buildings, areas, prefix=kml_prefix)

    # *****************************************************************************************************************

    fitnesses_table = create_graphs(solutions)[0]  # A list with all the 'coordinates' to create the graph
    minimum_f1 = create_graphs(solutions)[1]  # The coordinates of the solution that better minimises Capacity function
    minimum_f2 = create_graphs(solutions)[2]  # The coordinates of the solution that better minimises Distance function
    trade_off = create_graphs(solutions)[3]  # The coordinates of a solution around the center of the first Pareto front

    '''