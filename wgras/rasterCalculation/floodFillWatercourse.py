import os
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr
from osgeo import osr
from .. import TEMP_RASTER_RESULT_FOLDER

class floodAnalysis(object):
    '''
    Calculates a flooded area from a user-selected water source and a given water level increase
    '''

    def __init__(self, databaseName, databaseUser, databasePW, raster_name, output_name, output_table_name, coords, water_increase):
        self.databaseName = databaseName
        self.databaseUser = databaseUser
        self.databasePW = databasePW
        self.raster_name = raster_name
        self.output_name = output_name
        self.output_table_name = output_table_name
        self.coords = coords
        self.water_increase = water_increase

    def floodFill(self):

        self._createTempDirectory()
        self._emptyTempDirectory()

        # Read the WCS-imported raster with GDAL
        gtif = gdal.Open(self.raster_name, GA_ReadOnly)

        gt = gtif.GetGeoTransform()
        rb = gtif.GetRasterBand(1)

        # set the standard spatial reference
        spatialRef = osr.SpatialReference()
        spatialRef.ImportFromEPSG(4326)

        # Create ogr point at input coordinates
        coord_pixels = []
        point_shapes = []
        for coord in self.coords:
            lat, lng = coord
            wkt = "POINT (" + str(lat) + " " + str(lng) + ")"
            point = ogr.CreateGeometryFromWkt(wkt, spatialRef)
            point_shapes.append(point)
            mx = point.GetX()
            my = point.GetY()
            # Find pixels at coordinate
            px = int((mx - gt[0]) / gt[1])
            py = int((my - gt[3]) / gt[5])
            coord_pixels.append((px, py))


        # Turn pixel values into numpy array
        elev_data = rb.ReadAsArray()

        # FLOOD FILL
        neighbours = [(-1, -1), (-1, 0), (-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1)]

        # Mask for saving flooded pixels
        mask = numpy.empty_like(elev_data, dtype=bool)
        aspect_mask = numpy.empty_like(elev_data, dtype=bool)
        # Start coordinate from where the flood fill algorithm will begin
        startCoord = []

        for i, coord in enumerate(coord_pixels):
            threshold_level = elev_data[coord[1]][coord[0]] + self.water_increase
            startCoord.append(coord)

            # Put a limit at next point down-stream.
            if i is not len(coord_pixels) - 1:
                aspect_x = abs(coord_pixels[i][0] - coord_pixels[i + 1][0])
                aspect_y = abs(coord_pixels[i][1] - coord_pixels[i + 1][1])

                if aspect_x >= aspect_y:
                    for a in xrange(elev_data.shape[0]):
                        nx = coord_pixels[i + 1][0]
                        ny = a
                        aspect_mask[ny, nx] = True
                elif aspect_x < aspect_y:
                    for a in xrange(elev_data.shape[1]):
                        nx = a
                        ny = coord_pixels[i + 1][1]
                        aspect_mask[ny, nx] = True

            # Start the flood filling
            # Loop through all neighbouring coordinates and set "lower" pixels to flooded
            while startCoord:
                x, y = startCoord.pop()
                elev_data[y][x] = threshold_level
                mask[y, x] = True
                for dx, dy in neighbours:
                    nx = x + dx
                    ny = y + dy
                    if (0 <= nx < elev_data.shape[1] and 0 <= ny < elev_data.shape[0] and not mask[ny, nx]and not
                        aspect_mask[ny, nx] and (elev_data[ny, nx] - elev_data[y, x]) < 0):
                        startCoord.append((nx, ny))

            # Take away limit at next point down-stream
            if i is not len(coord_pixels) - 1:
                if aspect_x >= aspect_y:
                    for a in xrange(elev_data.shape[0]):
                        nx = coord_pixels[i + 1][0]
                        ny = a
                        aspect_mask[ny, nx] = False
                elif aspect_x < aspect_y:
                    for a in xrange(elev_data.shape[1]):
                        nx = a
                        ny = coord_pixels[i + 1][1]
                        aspect_mask[ny, nx] = False


        # Create raster image of the flooded pixels
        rst_layername = os.path.join(TEMP_RASTER_RESULT_FOLDER, self.output_name)
        rst_driver = gdal.GetDriverByName("GTiff")
        ds_out = rst_driver.Create(rst_layername, gtif.RasterXSize, gtif.RasterYSize, 1, gdal.GDT_Byte)
        CopyDatasetInfo(gtif, ds_out)
        band_out = ds_out.GetRasterBand(1)
        band_out.SetNoDataValue(0)
        BandWriteArray(band_out, mask)

        # Create GeoJSON
        outDriver = ogr.GetDriverByName('GeoJSON')
        outDataSource = outDriver.CreateDataSource(os.path.join(TEMP_RASTER_RESULT_FOLDER, 'staircaseFlood.geojson'))
        outLayer = outDataSource.CreateLayer(os.path.join(TEMP_RASTER_RESULT_FOLDER, 'staircaseFlood.geojson'), geom_type=ogr.wkbPolygon)
        gdal.Polygonize(band_out, band_out, outLayer, -1, [], callback=None)
        outDataSource.Destroy()

        gsfile = os.path.join(TEMP_RASTER_RESULT_FOLDER, 'staircaseFlood.geojson')
        dataSource = outDriver.Open(gsfile, 0)
        layer = dataSource.GetLayer()

        geometries = []
        for feature in layer:
            geom = feature.GetGeometryRef()
            geometries.append(geom.ExportToWkt())

        # Creating PostgreSQL driver
        #database = self.databaseName
        #usr = self.databaseUser
        #pw = self.databasePW
        #table = self.output_table_name

        #connectionString = "PG:dbname='%s' user='%s' password='%s'" % (database,usr,pw)
        #ogrds = ogr.Open(connectionString)
        #dst_layer = ogrds.CreateLayer(table, spatialRef, ogr.wkbPolygon, ['OVERWRITE=YES'])
        #gdal.Polygonize(band_out, band_out, dst_layer, -1, [], callback=None)

        return geometries

    def _createTempDirectory(self):
        directory = TEMP_RASTER_RESULT_FOLDER
        if not os.path.exists(directory):
            os.makedirs(directory)

    def _emptyTempDirectory(self):
        directory = TEMP_RASTER_RESULT_FOLDER
        fileList = os.listdir(directory)
        for fileName in fileList:
            os.remove(os.path.join(directory, fileName))

    def removeRasters(self):
        os.remove(os.path.join(TEMP_RASTER_RESULT_FOLDER, self.output_name))
        os.remove(os.path.join(TEMP_RASTER_RESULT_FOLDER,'staircaseFlood.geojson'))


