import os
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
from osgeo import ogr
from osgeo import osr
from wgras import applicationConfig
from wgras import app


class rasterEvaluator(object):

    def __init__(self):
        self.tmprasterdir = app.config['DOWNLOAD_TMP_RASTER']
        self.tmpresultdir = app.config['DOWNLOADS_RESULT_FOLDER']

    def getRasterStatisticsFromFile(self, filename):
        raster = gdal.Open(filename, GA_ReadOnly)
        band = raster.GetRasterBand(1)
        stats = band.GetStatistics(True, True)
        minValue = stats[0]
        maxValue = stats[1]
        meanValue = stats[2]
        stdvValue = stats[3]

        return {
            'minValue':minValue,
            'maxValue':maxValue,
            'meanValue':meanValue,
            'stdvValue':stdvValue
        }

    def floodFillLocal(self, fileName, incoords, water_increase):

        # Read the WCS-imported raster with GDAL
        gtif = gdal.Open(os.path.join(self.tmprasterdir, fileName), GA_ReadOnly)
        coord = incoords
        gt = gtif.GetGeoTransform()
        rb = gtif.GetRasterBand(1)
        elev_data = rb.ReadAsArray()

        filled = set()
        fill = set()
        width = elev_data.shape[1] - 1
        height = elev_data.shape[0] - 1

        spatialRef = osr.SpatialReference()
        spatialRef.ImportFromEPSG(4326)
        lat, lng = coord
        wkt = "POINT (" + str(lat) + " " + str(lng) + ")"
        point = ogr.CreateGeometryFromWkt(wkt, spatialRef)
        mx = point.GetX()
        my = point.GetY()
        # Find pixels at coordinate
        px = int((mx - gt[0]) / gt[1])
        py = int((my - gt[3]) / gt[5])
        threshold = elev_data[px][py] + water_increase
        seed = elev_data[px][py]

        fill.add((px, py))
        flood = numpy.zeros_like(elev_data, dtype=numpy.int8)

        while fill:
            x, y = fill.pop()
            if y == height-1 or x == width-1 or x < 0 or y < 0:
                continue

            if elev_data[x][y] < threshold:#elev_data[x][y] < threshold: #Math.abs(seedR - cr) < delta
                flood[x][y] = 1
                filled.add((x, y))

                west = (x - 1, y)
                east = (x + 1, y)
                north = (x, y - 1)
                south = (x, y + 1)

                if west not in filled:
                    fill.add(west)
                if east not in filled:
                    fill.add(east)
                if north not in filled:
                    fill.add(north)
                if south not in filled:
                    fill.add(south)

        geotiffLayername = 'downloads/results/result.tiff'
        geotiffdriver = gdal.GetDriverByName("GTiff")
        ds_out = geotiffdriver.Create(geotiffLayername, gtif.RasterXSize, gtif.RasterYSize, 1, gdal.GDT_Byte)
        CopyDatasetInfo(gtif, ds_out)
        band_out = ds_out.GetRasterBand(1)
        band_out.SetNoDataValue(0)
        BandWriteArray(band_out, flood)
        outDriver = ogr.GetDriverByName('GeoJSON')
        outDataSource = outDriver.CreateDataSource('downloads/results/flood.geojson')
        outLayer = outDataSource.CreateLayer('downloads/results/flood.geojson', geom_type=ogr.wkbPolygon)
        gdal.Polygonize(band_out, band_out, outLayer, -1, [], callback=None)
        outDataSource.Destroy()

        gsfile = 'downloads/results/flood.geojson'
        # outDriver = ogr.GetDriverByName("GeoJSON")
        dataSource = outDriver.Open(gsfile, 0)
        layer = dataSource.GetLayer()

        geometries = []
        for feature in layer:
            geom = feature.GetGeometryRef()
            geometries.append(geom.ExportToWkt())

        return geometries


    def floodFillGlobal(self, fileName, water_increase):
        src_ds = gdal.Open(os.path.join(self.tmprasterdir, fileName), GA_ReadOnly)
        if src_ds is None:
            app.logger.warning('Unable to open {}'.format(fileName))
            sys.exit(1)

        try:
            srcband = src_ds.GetRasterBand(1)
        except RuntimeError, e:
            # for example, try GetRasterBand(10)
            app.logger.warning('Band ( {} ) not found'.format(1))
            sys.exit(1)

        #
        #  create output datasource
        #
        dst_layername = os.path.join(self.tmpresultdir, 'polygonized')
        drv = ogr.GetDriverByName('GeoJSON')
        dst_ds = drv.CreateDataSource(dst_layername + ".geojson")
        dst_layer = dst_ds.CreateLayer(dst_layername, srs=None)

        gdal.Polygonize(srcband, None, dst_layer, -1, [], callback=None)

