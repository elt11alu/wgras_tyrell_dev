import psycopg2


def routing():
    db_host = "localhost"
    db_user = "postgres"
    db_passwd = "olive"
    db_database = "Test_routing_DB"
    db_port = "5432"

    conn = psycopg2.connect(host=db_host, user=db_user, port=db_port, password=db_passwd, database=db_database)
    cur = conn.cursor()

    buildings_query = "SELECT gid, x, y, pop FROM buildings;"
    areas_query = "SELECT gid, x, y, capacity FROM areas;"
    cur.execute(buildings_query)
    buildings_data = cur.fetchall()
    cur.execute(areas_query)
    areas_data = cur.fetchall()

    buildings_data = [list(building) for building in buildings_data]
    areas_data = [list(area) for area in areas_data]
    geometries_data = [[] for _ in range(len(buildings_data))]

    for b in range(len(buildings_data)):
        x1 = buildings_data[b][1]
        y1 = buildings_data[b][2]
        start_node_query = "SELECT id FROM ways_vertices_pgr ORDER BY the_geom <-> ST_GeometryFromText('POINT(" + str(
            x1) + " " + str(y1) + ")',4326) LIMIT 1;"
        cur.execute(start_node_query)  # get the start node id as an integer
        source = int(cur.fetchone()[0])
        for s in range(len(areas_data)):
            x2 = areas_data[s][1]
            y2 = areas_data[s][2]
            end_node_query = "SELECT id FROM ways_vertices_pgr ORDER BY the_geom <-> ST_GeometryFromText('POINT(" + str(
                x2) + " " + str(y2) + ")',4326) LIMIT 1;"
            cur.execute(end_node_query)  # get the end node id as an integer
            target = int(cur.fetchone()[0])

            routing_query = "SELECT seq, id1 AS node, id2 AS edge, cost FROM pgr_astar('SELECT gid AS id, source::integer, target::integer, length::double precision AS cost, x1, y1, x2, y2 FROM ways', " + str(
                source) + ", " + str(target) + ", false, false);"
            cur.execute(routing_query)
            routes_segment = cur.fetchall()

            summation = 0
            for route in routes_segment:
                summation += route[3]

            buildings_data[b].append(summation)
            geometries_data[b].append(routes_segment)

    return buildings_data, areas_data, geometries_data