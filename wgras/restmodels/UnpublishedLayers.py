class UnpublishedLayersModel(object):

    def __init__(
            self,
            layerName,
            layerTitle,
            workspaceName,
            layerInfo,
            keywords,
            enabled

    ):
        self.layerName = layerName
        self.layerTitle = layerTitle
        self.workspaceName = workspaceName
        self.layerInfo = layerInfo
        self.keywords = keywords
        self.enabled = enabled

    def getUnpublishedLayersModel(self):
        return self.__dict__
