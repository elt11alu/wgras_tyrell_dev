from BaseRestModel import BaseRestModel


class UnpublishedRestModel(BaseRestModel):

    def __init__(
            self,
            layerName,
            layerTitle,
            workspaceName,
            layerInfo,
            keywords,
            enabled,
            bboxMinX,
            bboxMinY,
            bboxMaxX,
            bboxMaxY


    ):
        super(UnpublishedRestModel, self).__init__(layerName,layerTitle,workspaceName,layerInfo,bboxMinX,bboxMinY,bboxMaxX,bboxMaxY,keywords)
        self.enabled = enabled

    def getUnpublishedLayersModel(self):
        return self.__dict__
