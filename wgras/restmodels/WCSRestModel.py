from BaseRestModel import BaseRestModel


class WCSRestModel(BaseRestModel):
    def __init__(self,
                 layerName,
                 layerTitle,
                 workspaceName,
                 layerInfo,
                 bboxMinX,
                 bboxMinY,
                 bboxMaxX,
                 bboxMaxY,
                 keywords,
                 gridHighLimits,
                 gridLowLimits,
                 gridAxisLabels,
                 gridDimensions,
                 ):
        super(WCSRestModel, self).__init__(layerName,layerTitle,workspaceName,layerInfo,bboxMinX,bboxMinY,bboxMaxX,bboxMaxY,keywords)
        self.gridHighLimits = gridHighLimits
        self.gridLowLimits = gridLowLimits
        self.gridAxisLabels = gridAxisLabels
        self.gridDimensions = gridDimensions

