from BaseRestModel import BaseRestModel


class WFSRestModel(BaseRestModel):
    def __init__(self,
                 layerName,
                 layerTitle,
                 workspaceName,
                 layerInfo,
                 bboxMinX,
                 bboxMinY,
                 bboxMaxX,
                 bboxMaxY,
                 keywords
                 ):
        super(WFSRestModel, self).__init__(layerName, layerTitle, workspaceName, layerInfo, bboxMinX, bboxMinY,
                                           bboxMaxX, bboxMaxY, keywords)
