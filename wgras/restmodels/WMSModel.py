class WmsModel(object):

    def __init__(
            self,
            layerName,
            layerTitle,
            workspaceName,
            layerInfo,
            layerType,
            crsForBoundingBox,
            bboxMinX,
            bboxMinY,
            bboxMaxX,
            bboxMaxY,
            hasElevationData,
            getMapUrl,
            baseUrl,
            keywords

    ):
        self.layerName = layerName
        self.layerTitle = layerTitle
        self.workspaceName = workspaceName
        self.layerInfo = layerInfo
        self.layerType = layerType
        self.crsForBoundingBox = crsForBoundingBox
        self.bboxMinX = bboxMinX
        self.bboxMinY = bboxMinY
        self.bboxMaxX = bboxMaxX
        self.bboxMaxY = bboxMaxY
        self.hasElevationData = hasElevationData
        self.getMapUrl = getMapUrl
        self.baseUrl = baseUrl
        self.keywords = keywords

    def getWMSModel(self):
        return self.__dict__

    def getSimpleWMSModel(self):
        return {
            "layerName": self.layerName,
            "workspaceName": self.workspaceName,
            "loadWMS": True,
            "loadWFS": True,
            "bboxMinX": self.bboxMinX,
            "bboxMinY": self.bboxMinY,
            "bboxMaxX": self.bboxMaxX,
            "bboxMaxY": self.bboxMaxY
        }
