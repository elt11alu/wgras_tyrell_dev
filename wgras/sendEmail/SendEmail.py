from flask_mail import Message
from .. import mail
from .. import app, celery


@celery.task(name='email', bind=True)
def send_wgras_mail(celerytask, recipient, message, subject):
    celerytask.update_state(state='PROGRESS', meta=getMeta('Sending email to {}{}'.format(recipient,'...')))
    msg = Message(subject, sender='wgras.app@gmail.com', recipients=[recipient])
    msg.body = message
    with app.app_context():
        mail.send(msg)
    return {'processResult': 'Email was sent to {}'.format(recipient)}

def getStatus(task_id):
    task = send_wgras_mail.AsyncResult(task_id)
    if task.state == 'PENDING':
        # job did not start yet
        response = {
            'state': task.state,
            'processResult': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'processResult': task.info.get('processResult', ''),

        }
        if 'result' in task.info:
            print task.info['result']
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'processResult': str(task.info)  # this is the exception raised
        }
    return response

def getMeta(processResult):
    return {'processResult' : processResult}