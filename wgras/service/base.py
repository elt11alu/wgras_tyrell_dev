from .. import db
import uuid
from .. import app


class Service(object):
    def __init__(self):
        self.database = db
        self.logger = app.logger

    def generateUUID(self):
        return str(uuid.uuid4())
