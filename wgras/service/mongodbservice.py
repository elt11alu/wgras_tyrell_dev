from .base import Service
from ..mongodbconnection import layerdocument


class MongoDBService(Service):
    def __init__(self):
        super(MongoDBService, self).__init__()

    def shp_to_mongodb(self, file_path):
        return layerdocument.LayerDocument().shp_to_mongodb(file_path=file_path)

    def get_nbr_of_attributes(self, document_id):
        return layerdocument.LayerDocument().get_nbr_of_attributes(document_id=document_id)

    def get_nbr_of_features(self, document_id):
        return layerdocument.LayerDocument().get_nbr_of_features(document_id=document_id)

    def get_attributes_for_layer(self, document_id):
        return layerdocument.LayerDocument().get_attributes_for_layer(document_id=document_id)

    def delete_layer(self, document_id):
        return layerdocument.LayerDocument().delete_layer(document_id=document_id)
