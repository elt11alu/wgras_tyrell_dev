from ..geoserverconnection import WCS, WFS, WMS, Workspaces, Stores, Base, Resource, bbox
from ..postgisconnection.shpToPostgis import ShapeFileToPostGis
from ..postgisconnection.csvToPostGis import CSVToPostGis
from ..postgisconnection.dbFunctions import DbFunctions
from ..postgisconnection.schema import Schema
from ..postgisconnection.table import Table
from ..postgisconnection.base import PostGisConnection
from ..postgisconnection import geometryfunctions
from ..spatialprocess.raster import statistics, floodfill
from .base import Service
from ..namefactory.name_factory import NameFactory
from ..models import *
from ..wmsproxy import wmsproxy
import logging


class SpatialService(Service):
    def __init__(self):
        super(SpatialService, self).__init__()
        self.GsBase = Base.GeoserverConnection()
        self.WCS = WCS.WCSConnection()
        self.WMS = WMS.WMSConnection()
        self.WFS = WFS.WFSConnection()
        self.Workspace = Workspaces.GsWorkspace()
        self.Store = Stores.GsStore()
        self.Resource = Resource.Resource()
        self.name_fac = NameFactory()
        self.pgConn = PostGisConnection()
        self.bbox = bbox.BboxFactory()
        self.WMSProxy = wmsproxy.WMSProxy()
        self.logger = logging.getLogger()
        self.GeometryFunctions = geometryfunctions.GeometryFunctions()

    # def getAllWMSMetadata(self):
    #     layers = self.WMS.getAllWMSMetadata()
    #     wmsList = []
    #     for layer in layers['layers']:
    #         db_layer = Rasterlayer.query.filter_by(name=layer['layerName'].split(':')[1]).first()
    #         if db_layer is not None:
    #             layer['layerId'] = db_layer.layerid
    #             wmsList.append(layer)
    #     return {'layers': wmsList}
    #
    # def getWMSMetadata(self, name):
    #     return self.WMS.getWMSMetadata(name)
    #
    # def getAllWCSMetadata(self):
    #     return self.WCS.getAllWCSMetadata()
    #
    # def getWCSMetadata(self, name):
    #     return self.WCS.getWMSMetadata(name)
    #
    # def getAllWFSMetadata(self):
    #     layers = self.WFS.getAllWFSMetadata()
    #     wfsList = []
    #     for layer in layers['layers']:
    #         db_layer = Vectorlayer.query.filter_by(name=layer['layerName'].split(':')[1]).first()
    #         if db_layer is not None:
    #             layer['layerId'] = db_layer.layerid
    #             wfsList.append(layer)
    #     return {'layers': wfsList}
    #
    # def getWFSMetadata(self, name):
    #     return self.WFS.getWFSMetadata(name)
    #
    # def get_resource_repr(self, layer_name, workspace_name):
    #     return self.Resource.get_gs_resource_metadata(layer_name=layer_name, workspace_name=workspace_name)
    #
    # def get_resources_repr(self):
    #     return self.Resource.get_gs_resources_metadata()
    #
    # def getWorkspaceNames(self):
    #     return self.Workspace.getWorkspacesAsDict()
    #
    # def createGsWorkspace(self, workspaceName):
    #     return self.Workspace.createWorkspace(workspaceName)

    # def createCountryWorkspace(self, isocode):
    #     # create workspace with country name
    #     # create countryworkspace in user db
    #     # create permissions (superadmin r/w, countryadmin r/w , countryusers r)
    #     country = Country.query.filter_by(isocode=isocode).first()
    #     if country is not None:
    #         country_ws_name = self.name_fac.create_country_workspace_name(country_name=country.name)
    #         ws = self.createGsWorkspace(workspaceName=country_ws_name)
    #         if ws is not None:
    #             countryws = Countryworkspace(name=ws.name, country=country)
    #             db.session.add(countryws)
    #             db.session.commit()
    #             ac = AccessCreator()
    #             ac.createCountryWorkspaceAccess(countryws.wsid, isocode)
    #             return ws
    #         return None
    #     return None
    #
    # def deleteCountryWorkspaces(self, workspaceNames):
    #     try:
    #         for workspaceName in workspaceNames:
    #             self.Workspace.deleteWorkspace(workspaceName=workspaceName)
    #         return True
    #     except Exception as e:
    #         print e
    #         return False
    #
    # def createOrganizationWorkspace(self, isocode, orgid):
    #
    #     country = Country.query.filter_by(isocode=isocode).first()
    #     if country is not None:
    #         org = Organization.query.filter_by(orgid=orgid).first()
    #         if org is not None:
    #             workspace_name = self.name_fac.create_org_workspace_name(country.name, org.shortname)
    #             ws = self.createGsWorkspace(workspaceName=workspace_name)
    #             if ws is not None:
    #                 orgws = Orgworkspace(name=ws.name, organization=org)
    #                 db.session.add(orgws)
    #                 db.session.commit()
    #                 ac = AccessCreator()
    #                 ac.createOrganizationWorkspaceAccess(orgws.wsid, orgid)
    #                 return orgws
    #             else:
    #                 self.Workspace.deleteWorkspace(workspaceName=workspace_name)
    #                 return None
    #         else:
    #             return None
    #     else:
    #         return None
    #
    # def deleteOrgWorkspaces(self, workspaceNames):
    #     try:
    #         for workspaceName in workspaceNames:
    #             self.Workspace.deleteWorkspace(workspaceName=workspaceName)
    #         return True
    #     except Exception as e:
    #         print e
    #         return False

    # def create_vector_layer_from_shp(self, shpPath, shpFileName, tableName, workspaceId):
    #     '''Begin upload to postgis'''
    #     pgConn = ShapeFileToPostGis(shpPath=shpPath,
    #                                 tablename=tableName)
    #     fields = pgConn.getShapefileFields()
    #     pgConn.addFieldsToPostGisLayer(fields)
    #     pgConn.addGeometryToPostGisLayer()
    #     pgConn.commitToPostGis()
    #     geom_type = pgConn.get_geom_type_for_table(table_name=tableName)
    #     return self._publish_postgis_table_in_geoserver(tableName, workspaceId, geom_type)
    #
    # def readCSV(self, filePath):
    #     return CSVToPostGis().readCSV(filePath=filePath)
    #
    # def createVectorLayerFromCSV(self, filePath, csvFileName, tableName, workspaceId, latField='lat', lonField='lon'):
    #
    #     CSVToPostGis().uploadToPostgis(filePath=str(filePath), tableName=str(tableName), latField=str(latField),
    #                                    lonField=str(lonField))
    #     geom_type = self.pgConn.get_geom_type_for_table(table_name=tableName)
    #     return self._publish_postgis_table_in_geoserver(table_name=tableName, workspaceId=workspaceId,
    #                                                     geom_type=geom_type)
    #
    # def createRasterLayerFromGeoTIFF(self, filePath, workspaceName, storeName):
    #     gsWorkspace = Workspaces.GsWorkspace()
    #     workspace = gsWorkspace.createWorkspace(workspaceName=str(workspaceName))
    #     return self.Store.createCoverageStore(datastoreName=storeName, workspace=workspace, filePath=filePath)

    #####################################
    #          Layer funtions           #
    #                                   #
    #####################################
    # def create_new_empty_vector_layer(self, workspace_id, layer_name_dirty, layer_title, geometry_type, schema, fields):
    #     workspace = Workspace.query.filter_by(wsid=workspace_id).first()
    #
    #     layer_name = self.name_fac.create_layer_name(suffix=layer_name_dirty)
    #     store_name = self.Store.createPostGisStore(workspaceName=workspace.name, schema='public',
    #                                                store_name=self.name_fac.create_store_name(
    #                                                    workspace_name=workspace.name))
    #     table_created = None
    #     vector_metadata = None
    #     raster_metadata = None
    #
    #     if workspace is not None and store_name is not None:
    #         if geometry_type == 'Point':
    #             '''create postgis point table'''
    #             table_created = self.create_empty_point_table(layer_name=layer_name, schema_name='public',
    #                                                           fields=fields)
    #             vector_metadata = Pointlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name)
    #             raster_metadata = Rasterlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name,
    #                                           haselevationdata=False)
    #
    #         elif geometry_type == 'LineString':
    #             '''create postgis line table'''
    #             table_created = self.create_empty_line_table(layer_name=layer_name, schema_name='public', fields=fields)
    #             vector_metadata = Linelayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name)
    #             raster_metadata = Rasterlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name,
    #                                           haselevationdata=False)
    #
    #         elif geometry_type == 'Polygon':
    #             table_created = self.create_empty_polygon_table(layer_name=layer_name, schema_name='public',
    #                                                             fields=fields)
    #             vector_metadata = Polygonlayer(name=layer_name, title=layer_title, workspace=workspace,
    #                                            store=store_name)
    #             raster_metadata = Rasterlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name,
    #                                           haselevationdata=False)
    #
    #         if table_created is not None and vector_metadata is not None and raster_metadata is not None:
    #             '''publish to geoServer'''
    #             epsgCode = 'EPSG:4326'
    #
    #             if store_name is not None:
    #                 feature_published = self.Store.publishFeatureType(layerName=layer_name, datastoreName=store_name,
    #                                                                   workspaceName=workspace.name, epsgCode=epsgCode)
    #                 if feature_published:
    #                     '''add definition to database'''
    #                     db.session.add(vector_metadata)
    #                     db.session.add(raster_metadata)
    #                     db.session.commit()
    #                     self._set_world_bbox(layer_name=layer_name)
    #                     return True
    #                 self.logger.info('Feature could not be published')
    #                 return None
    #             self.logger.info('Store name was none')
    #             return None
    #         self.logger.info('Table was non or metadata was none')
    #         return None
    #     self.logger.info('Workspace or store name was none')
    #     return None
    #
    # def create_new__vector_layer_with_geometry(self, workspace_id, layer_name_dirty, layer_title, geometry_type, schema,
    #                                            fields, geometry):
    #     workspace = Workspace.query.filter_by(wsid=workspace_id).first()
    #
    #     layer_name = self.name_fac.create_layer_name(suffix=layer_name_dirty)
    #     store_name = self.Store.createPostGisStore(workspaceName=workspace.name, schema='public',
    #                                                store_name=self.name_fac.create_store_name(
    #                                                    workspace_name=workspace.name))
    #     table_created = None
    #     vector_metadata = None
    #     raster_metadata = None
    #
    #     if workspace is not None and store_name is not None:
    #         if geometry_type == 'Point':
    #             '''create postgis point table'''
    #             table_created = self.create_point_table_from_geojson(layer_name=layer_name, schema_name='public',
    #                                                                  field_definition=fields, points=geometry)
    #             vector_metadata = Pointlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name)
    #             raster_metadata = Rasterlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name,
    #                                           haselevationdata=False)
    #
    #         elif geometry_type == 'LineString':
    #             '''create postgis line table'''
    #             table_created = self.create_line_table_from_geojson(layer_name=layer_name, schema_name='public',
    #                                                                 field_definition=fields, lines=geometry)
    #             vector_metadata = Linelayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name)
    #             raster_metadata = Rasterlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name,
    #                                           haselevationdata=False)
    #
    #         elif geometry_type == 'Polygon':
    #             table_created = self.create_polygon_table_from_geojson(layer_name=layer_name, schema_name='public',
    #                                                                    field_definition=fields, polygons=geometry)
    #             vector_metadata = Polygonlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name)
    #             raster_metadata = Rasterlayer(name=layer_name, title=layer_title, workspace=workspace, store=store_name,
    #                                           haselevationdata=False)
    #
    #         if table_created is not None and vector_metadata is not None and raster_metadata is not None:
    #             '''publish to geoServer'''
    #             epsgCode = 'EPSG:4326'
    #
    #             if store_name is not None:
    #                 feature_published = self.Store.publishFeatureType(layerName=layer_name, datastoreName=store_name,
    #                                                                   workspaceName=workspace.name, epsgCode=epsgCode)
    #                 if feature_published:
    #                     '''add definition to database'''
    #                     db.session.add(vector_metadata)
    #                     db.session.add(raster_metadata)
    #                     db.session.commit()
    #                     self._recalculate_bbox(layer_name=layer_name)
    #                     return vector_metadata.getObj()

    # def getAttributesForLayer(self, layerName, workspaceName):
    #     res = Table().get_layer_attributes(layer_name=layerName)
    #     return res
    #
    # def get_attributes_for_layer_by_id(self, layer_id):
    #     layer = Layer.query.filter_by(layerid=layer_id).first()
    #     return Table().get_layer_attributes(layer_name=str(layer.name))
    #
    # def publishLayer(self, layerName, workspaceName, publish):
    #     return self.WMS.publishLayer(resourceName=layerName, workspaceName=workspaceName, publish=publish)
    #
    # def getPublishedLayers(self):
    #     return self.getAllWMSMetadata()
    #
    # def getUnpublishedLayers(self):
    #     return Base.GeoserverConnection().getUnpublishedLayers()
    #
    # def getKeywordsForLayer(self, layerName, workspaceName):
    #     return Base.GeoserverConnection().getLayerKeywords(layerName=layerName, workspaceName=workspaceName)
    #
    # def updatePropertiesForLayer(self, layerName, workspaceName, properties):
    #     return Base.GeoserverConnection().updateLayerProperties(layerName=layerName, workspaceName=workspaceName,
    #                                                           properties=properties)

    #####################################
    #          Type funtions            #
    #                                   #
    #####################################
    def get_geom_types(self):
        return [geom_type.getObj() for geom_type in Geomtype.query.all()]

    def get_attribute_types(self):
        return [attr_type.getObj() for attr_type in Attributetype.query.all()]

    #####################################
    #          Process funtions         #
    #                                   #
    #####################################
    # def extractPolygonFromRaster(self, layerName, coords, increase):
    #     filePath = self.WCS.downloadWCSLayer(layerName=layerName)
    #     return floodfill.FloodFill().floodFillLocal(filePath=filePath, incoords=coords, water_increase=increase)
    #
    # def buffer(self, geometry, distance):
    #     return DbFunctions().buffer(wkt=geometry, distance=distance)
    #
    # def getRasterStatistics(self, filePath):
    #     return statistics.RasterStatistics().getRasterStatisticsFromFile(filePath)
    #
    # def get_external_wms(self, url):
    #     self.WMSProxy.connect(url)
    #     return self.WMSProxy.get_layers()
    #
    # def calculate_within(self, point_layer_id, polygon_layer_id):
    #     point_layer = Layer.query.filter_by(layerid=point_layer_id).first()
    #     polygon_layer = Layer.query.filter_by(layerid=polygon_layer_id).first()
    #     return self.GeometryFunctions.within(points_table_name=point_layer.name, polygon_table_name=polygon_layer.name)

    #####################################
    #          WCS funtions             #
    #                                   #
    #####################################

    def downloadWCS(self, layerName):
        return self.WCS.downloadWCSLayer(layerName=layerName)

    #####################################
    #          Schema funtions          #
    #                                   #
    #####################################

    def createSpatialSchema(self, schemaName):
        Schema().createSchema(schemaName=schemaName)

    #####################################
    #          Postgis funtions         #
    #                                   #
    #####################################


    # def create_point_table_from_geojson(self, layer_name, schema_name, points, field_definition):
    #     return Table().create_point_table_with_geometry(schema=schema_name, table_name=layer_name, json_points=points,
    #                                                     field_definition=field_definition)
    #
    # def create_line_table_from_geojson(self, layer_name, schema_name, lines, field_definition):
    #     return Table().create_line_table_with_geometry(schema=schema_name, table_name=layer_name, json_lines=lines,
    #                                                    field_definition=field_definition)
    #
    # def create_polygon_table_from_geojson(self, layer_name, schema_name, polygons, field_definition):
    #     return Table().create_polygon_table_with_geometry(schema=schema_name, table_name=layer_name,
    #                                                       json_polygons=polygons, field_definition=field_definition)
    #
    # # --- Point --- #
    # def create_empty_point_table(self, layer_name, schema_name, fields):
    #     return Table().create_empty_point_table(table_name=layer_name, schema_name=schema_name, fields=fields)
    #
    # def insert_geojson_point(self, layer_id, geojson_point):
    #     layer = Pointlayer.query.filter_by(layerid=layer_id).first()
    #     inserted = Table().insert_json_geom(table_name=str(layer.name), json_geom=geojson_point)
    #     if inserted:
    #         self._recalculate_bbox(layer.name)
    #     return inserted
    #
    # # --- Line --- #
    # def create_empty_line_table(self, layer_name, schema_name, fields):
    #     return Table().create_empty_line_table(table_name=layer_name, schema_name=schema_name, fields=fields)
    #
    # def insert_geojson_line(self, layer_id, geojson_line):
    #     layer = Linelayer.query.filter_by(layerid=layer_id).first()
    #     inserted = Table().insert_json_geom(table_name=str(layer.name), json_geom=geojson_line)
    #     if inserted:
    #         self._recalculate_bbox(layer.name)
    #     return inserted
    #
    # # --- Polygon --- #
    # def create_empty_polygon_table(self, layer_name, schema_name, fields):
    #     return Table().create_empty_polygon_table(table_name=layer_name, schema_name=schema_name, fields=fields)
    #
    # def insert_geojson_polygon(self, layer_id, geojson_polygon):
    #     layer = Polygonlayer.query.filter_by(layerid=layer_id).first()
    #     inserted = Table().insert_json_geom(table_name=str(layer.name), json_geom=geojson_polygon)
    #     if inserted:
    #         self._recalculate_bbox(layer.name)
    #     return inserted

    # --- PRIVATE METHODS --- #

    # def _recalculate_bbox(self, layer_name):
    #     layer = Layer.query.filter_by(name=layer_name).first()
    #     self.bbox.recalculate_bbox(resource_name=layer_name, workspace_name=layer.workspace.name)
    #
    # def _set_world_bbox(self, layer_name):
    #     layer = Layer.query.filter_by(name=layer_name).first()
    #     self.bbox.set_bbox_on_resource_to_max(resource_name=layer.name, workspace_name=layer.workspace.name)

    # def _publish_postgis_table_in_geoserver(self, table_name, workspaceId, geom_type):
    #     workspace = Workspace.query.filter_by(wsid=workspaceId).first()
    #
    #     if workspace is not None:
    #         '''Creating store in geoserver and publishing'''
    #         store_name = self.name_fac.create_store_name(workspace_name=workspace.name)
    #         created_store_name = self.Store.createPostGisStore(workspaceName=workspace.name, store_name=store_name)
    #         if created_store_name is not None:
    #             feature_created = self.Store.publishFeatureType(table_name, created_store_name, workspace.name,
    #                                                             'EPSG:4326')
    #             if feature_created:
    #                 self._create_db_layer(geom_type=geom_type, name=table_name, title=table_name,
    #                                       workspaceId=workspace.wsid, store_name=created_store_name)
    #                 return feature_created
    #             return None
    #         return None
    #     return None

    '''Creates a layer repr in the wgrasusers db, so that we can keep track of the layer metadata'''
    #
    # def _create_db_layer(self, geom_type, name, title, workspaceId, store_name):
    #
    #     workspace = Workspace.query.filter_by(wsid=workspaceId).first()
    #     if geom_type == 'Point':
    #         layer = Pointlayer(name=name, title=title, workspace=workspace, store=store_name)
    #         db.session.add(layer)
    #         db.session.commit()
    #     elif geom_type == 'LineString':
    #         layer = Linelayer(name=name, title=title, workspace=workspace, store=store_name)
    #         db.session.add(layer)
    #         db.session.commit()
    #     elif geom_type == 'Polygon':
    #         layer = Polygonlayer(name=name, title=title, workspace=workspace, store=store_name)
    #         db.session.add(layer)
    #         db.session.commit()

    def _is_geomtype_supported(self, geom_type):
        return Geomtype.query.filter_by(geom_type=geom_type).first() is not None

    def _is_attribute_type_supported(self, attribute_type):
        return Attributetype.query.filter_by(attributetype=attribute_type).first() is not None
