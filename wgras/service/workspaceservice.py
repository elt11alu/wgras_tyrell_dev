from base import Service
from ..models import *
from ..systemaccess import accessControl, accessCreator
from . import geoserverservice
from ..namefactory.name_factory import NameFactory


class WorkspaceService(Service):
    def __init__(self):
        super(WorkspaceService, self).__init__()
        self.name_fac = NameFactory()

    def createCountryWorkspace(self, isocode):
        # create workspace with country name
        # create countryworkspace in user db
        # create permissions (superadmin r/w, countryadmin r/w , countryusers r)
        country = Country.query.filter_by(isocode=isocode).first()
        gs_service = geoserverservice.GeoServerService()
        if country is not None:
            country_ws_name = self.name_fac.create_country_workspace_name(country_name=country.name)
            ws = gs_service.createGsWorkspace(workspaceName=country_ws_name)
            if ws is not None:
                countryws = Countryworkspace(name=ws.name, country=country)
                db.session.add(countryws)
                db.session.commit()
                ac = accessCreator.AccessCreator()
                ac.createCountryWorkspaceAccess(countryws.wsid, isocode)
                return ws
            return None
        return None

    def deleteCountryWorkspaces(self, workspaceNames):
        try:
            gs_service = geoserverservice.GeoServerService()
            for workspaceName in workspaceNames:
                gs_service.deleteGsWorkspace(workspaceName=workspaceName)
            return True
        except Exception as e:
            print e
            return False

    def createOrganizationWorkspace(self, isocode, orgid):

        gs_service = geoserverservice.GeoServerService()
        country = Country.query.filter_by(isocode=isocode).first()
        if country is not None:
            org = Organization.query.filter_by(orgid=orgid).first()
            if org is not None:
                workspace_name = self.name_fac.create_org_workspace_name(country.name, org.shortname)
                ws = gs_service.createGsWorkspace(workspaceName=workspace_name)
                if ws is not None:
                    orgws = Orgworkspace(name=ws.name, organization=org)
                    db.session.add(orgws)
                    db.session.commit()
                    ac = accessCreator.AccessCreator()
                    ac.createOrganizationWorkspaceAccess(orgws.wsid, orgid)
                    return orgws
                else:
                    gs_service.deleteGsWorkspace(workspaceName=workspace_name)
                    return None
            else:
                return None
        else:
            return None

    def deleteOrgWorkspaces(self, workspaceNames):

        try:
            gs_service = geoserverservice.GeoServerService()
            for workspaceName in workspaceNames:
                gs_service.deleteGsWorkspace(workspaceName=workspaceName)
            return True
        except Exception as e:
            print e
            return False

    def get_workspace_names(self):
        return [ws.name for ws in Workspace.query.all()]

    def get_workspace_objects(self):
        return [ws.getObj() for ws in Workspace.query.all()]

    def get_country_workspaces(self, isocode):
        return [ws.getObj() for ws in Countryworkspace.quer.all()]

    def get_org_workspaces(self):
        return [ws.getObj() for ws in Orgworkspace.query.all()]

    def get_public_workspaces(self):
        return [ws.getObj() for ws in Publicworkspace.query.all()]

    def get_private_workspaces(self, user_id):
        user = User.query.filter_by(userid = user_id).first()
        if user is not None:
            return [ws.getObj() for ws in Privateworkspace.qyery.filter_by(user=user).all()]
        return None

    def get_admin_workspaces(self):
        return [ws.getObj() for ws in Adminworkspace.query.all()]

    def get_admin_workspace_for_admin(self, adminid):
        admin = Superadmin.query.filter_by(userid=adminid).first()
        if admin is not None:
            return [ws.getObj() for ws in Adminworkspace.query.filter_by(user=admin).all()]
        return None

    def get_gs_workspace_names_as_list(self):
        gs_service = geoserverservice.GeoServerService()
        return gs_service.get_workspace_names_as_list()

    def delete_workspace_in_gs(self, name):
        gs_service = geoserverservice.GeoServerService()
        return gs_service.deleteGsWorkspace(workspaceName=name)



