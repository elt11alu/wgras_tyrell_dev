import psycopg2
from .. import DB_USER, DB_NAME, DB_PASSWORD, DB_PORT, DATABASE_URI

class ConcaveHull():
    def __init__(self):
        pass

    def calculate(self, target_percent):

        conn = psycopg2.connect(DATABASE_URI)
        cur = conn.cursor()
        cur.execute("""SELECT ST_AsText(
            ST_ConcaveHull(
                ST_Collect(wkb_geometry),0.70,true
            )
        ) FROM concavetest;""")

        return cur.fetchall()
