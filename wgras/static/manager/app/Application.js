/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
//loadingMsk = Ext.get("loadingMask");
//loadingMessage = document.getElementById('loadingMessage');

loadingMsk = Ext.get("loadingMask");

Ext.define('VramApp.Application', {
    extend: 'Ext.app.Application',
    requires: ['VramApp.WGRASURL'],
    name: 'VramApp',

    stores: [
        'VramApp.store.UsersStore',
        'VramApp.store.WorkspaceStore',
        'VramApp.store.ShpAttrStore',
        'VramApp.store.CsvAttrStore',
        'VramApp.store.Layers',
        'VramApp.store.LayerKeywords',
        'VramApp.store.UnpublishedLayers',
        'VramApp.store.Roles',
        'VramApp.store.ManagementAreaByCountry',
        'VramApp.store.Countries',
        'VramApp.store.Organizations',
        'VramApp.store.Superadmin',
        'VramApp.store.Countryadmin',
        'VramApp.store.Countryuser',
        'VramApp.store.OrganizationAdmin',
        'VramApp.store.UnpublishedCountries',
        'VramApp.store.ProcessInfoStore'
    ],
    init: function () {

        Ext.setGlyphFontFamily('FontAwesome');
        this.setDefaultToken('home');

    },

    listen: {
        controller: {
            '*': {
                unmatchedroute: 'onUnmatchedRoute'
            }
        }
    },

    onUnmatchedRoute: function () {
        Ext.History.add('home');
    },

    launch: function () {

        Ext.onReady(function () {

            setTimeout(function () {
                loadingMsk.fadeOut({
                    duration: 500,
                    remove: true
                });

            }, 2000);

        }, {
            dom: false
        });

    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
