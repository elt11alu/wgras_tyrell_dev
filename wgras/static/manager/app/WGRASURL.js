/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.WGRASURL', {
    singleton:true,

    PRODUCTION: false,
    GEOSERVERURL: function(){
        if(this.PRODUCTION){
            return "https://wgras-geo.gis.lu.se/geoserver/"
        }
        return "http://localhost:8080/geoserver/"
    },
    REST_USER_URL:"/users"
});
