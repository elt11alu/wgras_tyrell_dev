Ext.define('VramApp.model.LayerModel', {
    extend: 'Ext.data.Model',


    fields: [
        { name: 'layerName',     type: 'string' },
        { name: 'loadwms',      type: 'boolean' },
        { name: 'loadwfs',    type: 'boolean' }
    ]
})
