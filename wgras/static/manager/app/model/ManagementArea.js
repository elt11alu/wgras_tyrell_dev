Ext.define('VramApp.model.ManagementArea', {
    extend: 'Ext.data.Model',


    fields: [
        { name: 'countryName'},
        {name: 'countryId'},
        {name: 'managementareaId'},
        {name: 'managementareaName'}
    ]
});
