/**
 * Created by anton on 2016-07-14.
 */

Ext.define('VramApp.model.Organization', {
    extend: 'Ext.data.Model',


    fields: [
        { name: 'isocode', type: 'string'},
        {name: 'countryName', type:'string'},
        {name: 'orgid', type: 'string'},
        {name: 'fullname', type: 'string'},
        {name: 'shortname', type: 'string'}
    ]
});
