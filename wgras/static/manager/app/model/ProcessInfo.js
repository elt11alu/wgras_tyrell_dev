/**
 * Created by anton on 8/4/16.
 */

Ext.define('VramApp.model.ProcessInfo', {
    extend: 'Ext.data.Model',

    idProperty: 'processId',

    fields: [
        { name: 'processName'},
        {name: 'processStatus'},
        {name: 'processResult'},
        {name: 'processId'}
    ]
});