Ext.define('VramApp.model.ShpAttrModel', {
    extend: 'Ext.data.Model',

    fields:[
        {name: 'attrName'},
        {name: 'dataType'},
        {name: 'nbrOfAttributes'},
        {name: 'nbrOfGeometries'},
        {name: 'shapeFileName'},
        {name: 'projField'}
    ]
})
