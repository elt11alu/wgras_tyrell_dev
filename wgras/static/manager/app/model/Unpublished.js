Ext.define('VramApp.model.Unpublished', {
    extend: 'Ext.data.Model',


    fields: [
        { name: 'layerInfo'},
        { name: 'layerName'},
        { name: 'layerTitle'},
        { name: 'enabled'},
        { name: 'workspaceName'}
    ]
});
