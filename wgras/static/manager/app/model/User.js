/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.model.User', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'VramApp.WGRASURL'
    ],

    fields: [


        { name: 'username',     type: 'string' },
        { name: 'userId',     type: 'string' },
        { name: 'type',      type: 'string' },
        { name: 'created',    type: 'string' },
        {name: 'organization', type: 'string'},
        {name: 'country', type: 'string'},
        {name: 'email', type:'string'}

    ],

    proxy: {
        type: 'ajax',
        url : VramApp.WGRASURL.REST_USER_URL,
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'users'

        }
    }
});
