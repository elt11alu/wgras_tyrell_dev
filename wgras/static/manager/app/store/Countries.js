Ext.define('VramApp.store.Countries', {
    extend: 'Ext.data.Store',

    alias: 'store.countries',
    storeId:'countries',

    autoLoad:true,
    model:'VramApp.model.Countries',
    sorters:'name',

    proxy: {
        type: 'ajax',
        url:"/countries/published",
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'countries'
        }
    }
});
