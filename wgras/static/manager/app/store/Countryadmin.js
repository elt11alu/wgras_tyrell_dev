/**
 * Created by anton on 2016-07-14.
 */

Ext.define('VramApp.store.Countryadmin', {
    extend: 'Ext.data.Store',

    alias:'store.countryadminstore',
    storeId:'countryadminstore',

    requires: [
        'VramApp.model.Countryadmin'
    ],

    model: 'VramApp.model.Countryadmin',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        url: '/users/countryadmins',
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'countryadmins'
        }
    },

    loadAdmins: function (isocode, grid) {
        if (grid != undefined) {
            grid.setLoading(true)
        }
        this.load({
            url: '/users/countryadmins/'.concat(isocode),
            callback: function () {
                if (grid != undefined) {
                    grid.setLoading(false)
                }

            }

        })
    }

});

