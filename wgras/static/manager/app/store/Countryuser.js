/**
 * Created by anton on 2016-07-14.
 */

Ext.define('VramApp.store.Countryuser', {
    extend: 'Ext.data.Store',

    alias: 'store.countryuserstore',
    storeId: 'countryuserstore',

    requires: [
        'VramApp.model.Countryuser'
    ],

    model: 'VramApp.model.Countryuser',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        url: '/users/countryusers',
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'countryusers'
        }
    },

    loadUsers: function (isocode, gridId) {
        var grid = Ext.getCmp(gridId);
        if (grid instanceof Ext.grid.Panel) {
            grid.setLoading(true)
        }
        this.load({
            url: '/users/countryusers/'.concat(isocode),
            callback: function () {
                if (grid instanceof Ext.grid.Panel) {
                    grid.setLoading(false)
                }

            }

        })
    }

});
