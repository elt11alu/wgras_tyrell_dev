/**
 * Created by anton on 4/14/16.
 */


Ext.define('VramApp.store.LayerKeywords', {
    extend: 'Ext.data.Store',
    alias: 'store.layerKeywords',
    storeId:'layerkeywords',

    fields:[{name:'keyword'}]

});
