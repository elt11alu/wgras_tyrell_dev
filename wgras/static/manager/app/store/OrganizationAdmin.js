/**
 * Created by anton on 2016-07-14.
 */
Ext.define('VramApp.store.OrganizationAdmin', {

    extend: 'Ext.data.Store',
    alias:'store.organizationadminstore',
    storeId:'organizationadminstore',

    requires: [
        'VramApp.model.OrganizationAdmin'
    ],

    model: 'VramApp.model.OrganizationAdmin',

    autoLoad:false,

        proxy: {
        type: 'ajax',
        url: '/users/orgadmins',
        headers: {
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'orgadmins'
        }
    },

    loadAdmins: function (orgid, grid) {
        if (grid != undefined) {
            grid.setLoading(true)
        }
        this.load({
            url: '/users/orgadmins/'.concat(orgid),
            callback: function () {
                if (grid != undefined) {
                    grid.setLoading(false)
                }

            }

        })
    }

});