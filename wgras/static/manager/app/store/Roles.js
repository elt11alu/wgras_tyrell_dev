Ext.define('VramApp.store.Roles', {
    extend: 'Ext.data.Store',

    alias: 'store.roles',
    storeId:'roles',

    autoLoad:true,
    model:'VramApp.model.Roles',
    sorters:'rolename',

    proxy: {
        type: 'ajax',
        url:"/users/roles",
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'roles'
        }
    }
});
