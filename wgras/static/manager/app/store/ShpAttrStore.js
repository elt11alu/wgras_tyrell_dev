Ext.define('VramApp.store.ShpAttrStore', {
    extend: 'Ext.data.Store',

    alias: 'store.shpattrstore',
    storeId:'shpattrstore',

    autoLoad:false,

    model:'VramApp.model.ShpAttrModel',

    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty:'fields'
        }
    }
});
