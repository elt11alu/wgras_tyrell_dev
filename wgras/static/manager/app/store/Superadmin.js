/**
 * Created by anton on 2016-07-14.
 */

Ext.define('VramApp.store.Superadmin', {
    extend: 'Ext.data.ChainedStore',
    source: 'existingsusersstore',

    alias:'store.superadminstore',
    storeId:'superadminstore',

    requires: [
        'VramApp.model.Superadmin'
    ],

    model: 'VramApp.model.Superadmin',

    autoLoad:true

});
