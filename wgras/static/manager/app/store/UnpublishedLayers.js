Ext.define('VramApp.store.UnpublishedLayers', {
    extend: 'Ext.data.Store',

    alias: 'store.unpublishedlayers',
    storeId:'unpublishedlayers',

    autoLoad:true,
    model:'VramApp.model.Layers',
    sorters:'layerName',

    proxy: {
        type: 'ajax',
        url:'/layer/unpublished',
        headers:{
            'Accept': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty:'layers'
        }
    }
});
