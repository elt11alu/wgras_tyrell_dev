/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.store.UsersStore', {
    extend: 'Ext.data.Store',

    alias:'store.userstore',
    storeId:'existingsusersstore',

    requires: [
        'VramApp.model.User'
    ],

    model: 'VramApp.model.User',

    autoLoad:true

});
