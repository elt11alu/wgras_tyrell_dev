/**
 * Created by anton on 2015-12-27.
 */

Ext.define('VramApp.utilities.HTTPRequests', {
    singleton: true,

    GET: function (url, successCallback, failureCallback, id) {
        Ext.Ajax.request({
            url: url,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            success: function (response, opts) {
                successCallback(response, id);
            },

            failure: function (response, opts) {
                failureCallback(response, id);
            }
        });
    }
    ,

    POST: function (url, jsonData, successCallback, failureCallback) {
        Ext.Ajax.request({
            timeout: 60000,
            url: url,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            jsonData: jsonData,

            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failureCallback(response);
            }
        });
    }
    ,

    PUT: function (url, jsonData, successCallback, failureCallback) {
        Ext.Ajax.request({
            timeout: 60000,
            url: url,
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            jsonData: jsonData,

            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failureCallback(response);
            }
        });
    }
    ,

    DELETE: function (url, jsonData, successCallback, failureCallback) {
        Ext.Ajax.request({
            timeout: 60000,
            url: url,
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            jsonData: jsonData,

            success: function (response, opts) {
                successCallback(response);
            },

            failure: function (response, opts) {
                failureCallback(response);
            }
        });
    }

})
;
