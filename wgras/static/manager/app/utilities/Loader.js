/**
 * Created by anton on 8/17/16.
 */

Ext.define('VramApp.utilities.Loader', {
    singleton: true,

    getLoader: function (target_component, msg) {
        return new Ext.LoadMask({
            msg: msg,
            target: target_component
        });
    }
});