/**
 * Created by anton on 8/1/16.
 */

Ext.define('VramApp.utilities.MapLegend', {
    singleton: true,

    getMapLegend: function (items) {
        var me = this;
        var legendDiv = this.getLegendDiv();
        var legendList = this.getLegendList();
        Ext.Object.each(items, function (key, val) {
                var entry = me.getLegendItem(val.color, val.description);
                legendList.appendChild(entry);
            }
        );
        legendDiv.appendChild(legendList);
        return legendDiv;
    },

    getLegendDiv: function () {
        var div = document.createElement('div');
        div.setAttribute('class', 'legend ol-unselectable ol-control');
        div.innerHTML = 'Legend';
        return div;
    },

    getLegendList: function () {
        var ul = document.createElement('ul');
        ul.setAttribute('class', 'legendList');
        return ul;
    },

    getLegendItem: function (color, descriptionText) {
        var li = document.createElement('li');
        var symbol = document.createElement('div');
        var description = document.createElement('div');
        li.setAttribute('class', 'legendItem');
        symbol.setAttribute('class', 'legendSymbol');
        symbol.style.backgroundColor = color;
        description.setAttribute('class', 'legendDescription');
        description.innerHTML = descriptionText;
        li.appendChild(symbol);
        li.appendChild(description);
        return li;
    }
});