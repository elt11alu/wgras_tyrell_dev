/**
 * Created by anton on 2016-07-14.
 */

Ext.define('VramApp.view.country.CountryGridController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'VramApp.view.country.addCountry.AddCountry',
        'VramApp.view.country.viewCountryAdmin.CountryAdminWindow',
        'VramApp.view.country.viewCountryUser.CountryUserWindow'
    ],

    alias: 'controller.countrygridcontroller',
    id: 'countrygridcontroller',

    listen: {
        controller: {
            '#addcountrycontroller': {
                onCountryAdded: 'reloadGrid'
            }
        }
    },

    init: function () {
    },

    reloadGrid: function () {
        Ext.data.StoreManager.lookup('countries').reload();
    },

    /********************************************
     *
     * Publish country form functionality
     *
     ********************************************/


    onCreateCountry: function () {
        var win = Ext.create({
            xtype: 'addCountryWindow'
        });
        win.show()
    },

    onPublishCountry: function (btn) {
        var me = this;
        var win = btn.up('window');
        var loadMask = new Ext.LoadMask({
            msg: 'Please wait...',
            target: win
        });
        var isocode = this.lookupReference('unpublishedcountry').getValue();
        var success = function (resp) {
            var obj = Ext.decode(resp.responseText);
            loadMask.hide();
            me.onPublishSuccess(obj, win)

        };
        var fail = function (resp) {
            var obj = Ext.decode(resp.responseText);
            console.log(obj);
            loadMask.hide()
        };

        var data = JSON.stringify({'isocode': isocode})
        loadMask.show()
        VramApp.utilities.HTTPRequests.POST('/country', data, success, fail)
    },

    onSelectUnpublished: function (combo, rec) {
        this.lookupReference('unpubl_name').setValue(rec.get('name'));
        this.lookupReference('unpubl_isocode').setValue(rec.get('isocode'));

    },

    onPublishSuccess: function (result, win) {
        var msg = 'Country '.concat(result.country.name, ' was published!');
        Ext.MessageBox.alert('Status', msg);
        this.reloadGrid();
        this.fireEvent('onCountryAdded', result.country.isocode);
        this.fireEvent('onProcessInitiated',{processName:'Add country', processStatus:'SUCCESS',processId:'ksljf', processResult: msg});
        win.close();

    },

    /********************************************
     *
     * Delete country functionality
     *
     ********************************************/

    onDeleteCountry: function (btn) {
        var rec = btn.getWidgetRecord();
        var isocode = rec.get('isocode');
        var cname = rec.get('name')

        var me = this;
        var proceedDeletion = function () {
            var grid = btn.up('grid');
            var loadMask = new Ext.LoadMask({
                msg: 'Please wait...',
                target: grid
            });

            var success = function (resp) {
                var obj = Ext.decode(resp.responseText);
                loadMask.hide()
                me.onDeleteSuccess(obj)

            };
            var fail = function (resp) {
                var obj = Ext.decode(resp.responseText);
                loadMask.hide();
                me.onDeleteFail(obj);
            };
            var data = JSON.stringify({'isocode': isocode});
            loadMask.show();
            VramApp.utilities.HTTPRequests.DELETE('/country', data, success, fail)
        };
        this.checkDelete(cname, proceedDeletion);

    },

    onDeleteSuccess: function (obj) {
        Ext.toast('Country '.concat(obj.country.name, ' was deleted.'));
        Ext.MessageBox.alert('Status', 'Country '.concat(obj.country.name, ' was unpublished!'));
        this.reloadGrid();
        this.fireEvent('onCountryDeleted', obj.country.isocode)
    },

    onDeleteFail: function(obj){
        Ext.MessageBox.alert('Status', obj.errorMessage);
    },

    checkDelete: function (countryName, proceed) {
        Ext.Msg.show({
            title: 'Delete?',
            message: 'Do you really want to delete '.concat(countryName, '?', 'This will also delete the users and organizations associated with this country. Proceed?'),
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            fn: function (btn) {
                if (btn === 'yes') {
                    proceed()
                }
            }
        });
    },

    /********************************************
     *
     * Country admin functionality
     *
     ********************************************/

    onViewAdmins: function (btn) {

        var me = this;
        var isocode = btn.getWidgetRecord().get('isocode');
        var win = Ext.create({
            xtype: 'countryAdminWindow',
            controller: me
        });

        win.on('afterrender', function () {
            me.fireEvent('onCountryAdminWindowOpen', isocode)
        });
        win.show()


    },

    /********************************************
     *
     * Country user functionality
     *
     ********************************************/

    onViewUsers: function(btn){
        var me = this;
        var isocode = btn.getWidgetRecord().get('isocode');
        var win = Ext.create({
            xtype: 'countryUserWindow',
            controller: me
        });

        win.on('afterrender', function () {
            me.fireEvent('onCountryUserWindowOpen', isocode);
        });
        win.show();

    }


});
