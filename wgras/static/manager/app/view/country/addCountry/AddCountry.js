/**
 * Created by anton on 2016-07-15.
 */
Ext.define('VramApp.view.country.addCountry.AddCountry', {
    extend: 'Ext.window.Window',
    controller: 'addcountrycontroller',
    requires: [
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'VramApp.view.country.addCountry.AddCountryController'
    ],
    title: 'Publish country',
    viewModel: true,
    width: 300,
    bodyPadding: 10,

    xtype: 'addCountryWindow',

    items: [
        {
            xtype: 'combo',
            valueField: 'isocode',
            displayField: 'name',
            queryMode: 'local',
            reference: 'unpublishedcountry',
            store: {
                type: 'unpublishedcountries',
                autoLoad: true
            },
            fieldLabel: 'Choose country',
            allowBlank: false,
            publishes: 'value',
            listeners: {
                select: {
                    fn: 'onSelectUnpublished'
                }
            }
        },
        {
            xtype: 'displayfield',
            value: '',
            fieldLabel: 'Name',
            reference: 'unpubl_name'
        },
        {
            xtype: 'displayfield',
            value: '',
            fieldLabel: 'ISO',
            reference: 'unpubl_isocode'
        },
        {
            xtype: 'button',
            text: 'Publish',
            handler: 'onPublishCountry',
            bind: {
                disabled: '{!unpublishedcountry.value}'
            }
        }

    ]
});
