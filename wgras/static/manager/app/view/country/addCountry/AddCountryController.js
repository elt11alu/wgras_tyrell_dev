/**
 * Created by anton on 8/1/16.
 */

Ext.define('VramApp.view.country.addCountry.AddCountryController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.addcountrycontroller',
    id: 'addcountrycontroller',

    init: function(){
    },

    onPublishCountry: function (btn) {
        var me = this;
        var win = btn.up('window');
        var loadMask = this.getLoadMask(win);
        var isocode = this.lookupReference('unpublishedcountry').getValue();
        var success = function (resp) {
            var obj = Ext.decode(resp.responseText);
            loadMask.hide();
            me.onPublishSuccess(obj);

        };
        var fail = function (resp) {
            var obj = Ext.decode(resp.responseText);
            console.log(obj);
            loadMask.hide()
        };

        var data = JSON.stringify({'isocode': isocode});
        loadMask.show();
        VramApp.utilities.HTTPRequests.POST('/country', data, success, fail)
    },

    onSelectUnpublished: function (combo, rec) {
        this.lookupReference('unpubl_name').setValue(rec.get('name'))
        this.lookupReference('unpubl_isocode').setValue(rec.get('isocode'))

    },

    onPublishSuccess: function (result) {
        Ext.MessageBox.alert('Status', 'Country '.concat(result.country.name, ' was published!'));
        this.fireEvent('onCountryAdded', result.country.isocode);
        this.getView().close();

    },

    getLoadMask: function(view){
        var loadMask = new Ext.LoadMask({
            msg: 'Please wait...',
            target: view
        });
        return loadMask;
    }
});
