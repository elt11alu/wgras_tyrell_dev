/**
 * Created by anton on 2016-07-21.
 */

Ext.define('VramApp.view.country.addCountry.addCountryFromMap.MapPanel', {
    extend: 'Ext.panel.Panel',

    requires: [
        'VramApp.view.country.addCountry.addCountryFromMap.Map',
        'VramApp.view.country.addCountry.addCountryFromMap.MapPanelController'
    ],

    controller: 'mappanelcontroller',
    reference: 'addcountrymappanel',
    id: 'addcountrymappanel',
    xtype: 'addcountrymappanel',

    layout: 'fit',
    title: 'Add country from map',
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'top',
                    layout: {
            type: 'vbox',
            pack: 'start',
            align: 'begin'
        },
            items: [
                {
                    xtype: 'textfield',
                    readOnly: true,
                    value: '',
                    reference: 'name',
                    fieldLabel: 'Name'
                },
                {
                    xtype: 'textfield',
                    readOnly: true,
                    value: '',
                    reference: 'isocode',
                    fieldLabel: 'Code'
                }]
        },
        {
            xtype: 'toolbar',
            dock: 'top',
            overflowHandler: 'menu',
            items: [
                {
                    xtype: 'button',
                    text: 'Add country',
                    glyph: 0xf067,
                    handler: 'onAddCountry'
                },
                {
                    xtype: 'button',
                    text: 'Clear selection',
                    glyph: 0xf068,
                    handler: 'onClearSelection'
                }]
        },
        {
            xtype: 'toolbar',
            dock: 'top',
            items: [
                {
                    xtype: 'displayfield',
                    reference: 'addCountryFromMapMessageField'
                }]
        }
    ],

    items: [


        {
            xtype: 'createcountrymapview'
        }


    ]

});
