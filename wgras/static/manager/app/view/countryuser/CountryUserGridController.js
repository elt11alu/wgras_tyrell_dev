/**
 * Created by anton on 2016-07-21.
 */


Ext.define('VramApp.view.countryuser.CountryUserGridController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'VramApp.view.countryuser.addCountryUser.AddCountryUser'
    ],


    alias: 'controller.countryusergridcontroller',
    id: 'countryusergridcontroller',

    listen: {
        controller: {
            '#countrygridcontroller':{
                onCountryUserWindowOpen: function(isocode){
                    this.isocode = isocode;
                    this.reloadGrid()
                }
            },
            '*':{
                onUserAdded: function(username){
                    this.reloadGrid()
                    this.setSuccessMessage('User '.concat(username, ' was added.'))
                },
                onUserDeleted: function(username){
                    this.reloadGrid()
                    this.setSuccessMessage('User '.concat(username, ' was deleted.'))
                },
                onFail: function(msg){
                    this.reloadGrid()
                    this.setFailMessage('The user could not be updated. Reason: '.concat(msg))
                }
            }
        }
    },

    init: function (view) {

    },

    reloadGrid: function(){
        var gridId = this.getView().getId();
        Ext.data.StoreManager.lookup('countryuserstore').loadUsers(this.isocode, gridId)
    },

    onCreateCountryUser: function(){
        Ext.create({
            xtype: 'addCountryUserWindow'
        }).show();

        this.fireEvent('onAddCountryUserWindowOpen', this.isocode)

    },

    onEmail: function(btn){
        var recieverId = btn.getWidgetRecord().get('userId');

    },

    onDeleteCountryUser: function(btn){
        var adminId = btn.getWidgetRecord().get('userId');
        var me = this;
        var success = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.onDeleteSuccess(obj);
        };
        var fail = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.onDeleteFail(obj);
        };

        var data = JSON.stringify({'userId':adminId});

        VramApp.utilities.HTTPRequests.DELETE('/users/countryusers',data,success,fail);
    },

    onDeleteSuccess: function(obj){
        var username = obj.countryuser.username;
        this.fireEvent('onUserDeleted', username);
        Ext.toast('User '.concat(username, ' was deleted.'));
    },

    onDeleteFail: function(obj){
        var username = obj.errorMessage;
        this.fireEvent('onFail', username);
        Ext.toast('User '.concat(username, ' could not be deleted, try again later.'));
    },

    setSuccessMessage: function(msg){
        this.lookupReference('countryUserGridMsgField').setValue('<p style="color:green">' + msg + '</p>');
    },

    setFailMessage: function(msg){
        this.lookupReference('countryUserGridMsgField').setValue('<p style="color:red">' + msg + '</p>');
    }


});


