/**
 * Created by anton on 9/14/16.
 */

Ext.define('VramApp.view.externalows.ExternalOws', {
    extend: 'Ext.panel.Panel',

    requires: [
        'VramApp.view.externalows.ExternalOwsController',
        'VramApp.view.externalows.ExternalOwsViewModel'
    ],

    xtype: 'externalowspanel',

    controller: 'externalowscontroller',
    viewModel: 'extermalowsviewmodel',

    layout: 'column',

    bodyPadding: 5,

    defaults: {
        bodyPadding: 15,
        margin: 5
    },

    items: [
        {
            xtype: 'grid',
            maxHeight: 600,
            scrollable:'y',

            bind: {
                store: '{externalowsstore}',
                title: '{owsgridtitle}'
            },
            columnWidth: 0.6,
            columns: [
                {
                    text: 'Name',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'name'
                },
                {
                    text: 'URL',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'url'
                },
                {
                    text: 'Description',
                    flex: 1,
                    sortable: false,
                    dataIndex: 'description'
                },
                {
                    text: 'Category',
                    flex: 1,
                    sortable: false,
                    dataIndex: 'category'
                },

                {
                    width: 80,
                    xtype: 'widgetcolumn',
                    padding: 3,
                    widget: {
                        glyph: 0xf014,
                        width: 40,
                        xtype: 'button',
                        dataIndex: 'owsid',
                        tooltip: 'Delete country',
                        style: {
                            backgroundColor: 'LightCoral',
                            border: 0
                        },
                        handler: 'onDeleteOWS'

                    }
                }
            ]
        },
        {
            xtype: 'form',
            scrollable: true,
            bind: {
                title: '{formtitle}'
            },
            columnWidth: 0.4,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'User Info',
                    defaultType: 'textfield',
                    defaults: {
                        anchor: '100%'
                    },

                    items: [
                        {
                            xtype: 'displayfield',
                            bind:{
                                value: '{informationText}'
                            }
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'URL',
                            name: 'url',
                            emptyText: 'URL',
                            vtype: 'url',
                            bind: {
                                value: '{servicemetadata.url}'
                            }
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Name',
                            name: 'name',
                            emptyText: 'Name',
                            vtype: 'alphanum',
                            bind: {
                                value: '{servicemetadata.name}'
                            }
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Category',
                            vtype: 'alphanum',
                            name: 'category',
                            emptyText: 'Category',
                            bind: {
                                value: '{servicemetadata.category}'
                            }
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Description',
                            regex: /^[A-Za-z0-9 ]+$/,
                            name: 'description',
                            emptyText: 'Description',
                            bind: {
                                value: '{servicemetadata.description}'
                            }
                        },
                        {
                            xtype: 'displayfield',
                            bind: {
                                value: '{messageText}'
                            }
                        },
                        {
                            xtype: 'button',
                            formBind: true,
                            anchor: '50%',
                            text: 'Submit',
                            handler: 'onAddOWS'
                        }
                    ]
                }
            ]
        }
    ]


});
