/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.layerEdit.EditLayerGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'editlayergrid',
    flex: 1,
    title: 'Published layers',
    scrollable: true,
    height: 300,
    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'updateGrids'
        },
        {
            type: 'print',
            tooltip: 'Print list',
            handler: 'printPublishedLayerList'
        }

    ],
    store: {
        type: 'layers'
    },
    columns: [
        {xtype: 'rownumberer'},
        {
            text: 'Title',
            flex: 1,
            sortable: false,
            dataIndex: 'layerTitle'
        },
        {
            text: 'Name',
            flex: 1,
            sortable: false,
            dataIndex: 'layerName'
        }, {
            text: 'Info',
            flex: 1,
            sortable: false,
            dataIndex: 'layerInfo'
        }, {
            text: 'Category',
            flex: 1,
            sortable: false,
            dataIndex: 'workspaceName'
        }, {
            text: 'SRS',
            flex: 1,
            sortable: false,
            dataIndex: 'crsForBoundingBox'
        }, {
            text: 'Edit',
            width: 80,
            xtype: 'widgetcolumn',
            widget: {
                glyph: 0xf013,
                width: 40,
                xtype: 'button',
                dataIndex: 'layerName',
                tooltip: 'Edit layer',
                style: {
                    //backgroundColor: 'lightgrey',
                    border: 0
                },
                handler: 'onEditLayer'

            }
        },
        {
            text: 'Delete',
            width: 80,
            xtype: 'widgetcolumn',
            padding: 3,
            widget: {
                glyph: 0xf014,
                width: 40,
                xtype: 'button',
                dataIndex: 'layerName',
                tooltip: 'Delete layer',
                style: {
                    backgroundColor: 'LightCoral',
                    border: 0
                },
                handler: 'onDeleteLayer'

            }
        }]
})
