/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.layerEdit.UnpublishedLayerGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'unpublishedlayergrid',

    flex: 1,
    title: 'Unpublished layers',
    scrollable: true,
    height: 300,
    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'updateGrids'
        }

    ],
    store: {
        type: 'unpublishedlayers'
    },
    columns: [
        {xtype: 'rownumberer'},
        {
            text: 'Title',
            flex: 1,
            sortable: false,
            dataIndex: 'layerTitle'
        },
        {
            text: 'Name',
            flex: 1,
            sortable: false,
            dataIndex: 'layerName'
        }, {
            text: 'Info',
            flex: 1,
            sortable: false,
            dataIndex: 'layerInfo'
        }, {
            text: 'Category',
            flex: 1,
            sortable: false,
            dataIndex: 'workspaceName'
        }, {
            text: 'Publish',
            width: 80,
            xtype: 'widgetcolumn',
            widget: {
                glyph: 0xf0ee,
                width: 40,
                xtype: 'button',
                dataIndex: 'layerName',
                tooltip: 'Edit layer',
                style: {
                    border: 0
                },
                handler: 'onPublishLayer'

            }
        }]
})
