/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('VramApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    id: 'maincontroller',

    requires: [
        'VramApp.utilities.HTTPRequests',
        'VramApp.view.sendEmail.EmailWindow'
    ],

    listen: {
        controller: {
            '*': {
                onEmail: 'onEmail'
            }
        }
    },

    init: function () {
        var me = this;
        this.getView().addListener('beforetabchange', function (tabPanel, newCard, oldCard, eOpts) {
            me.doRoute(newCard, null)
        });

    },

    routes: {
        ':hash': {
            action: 'updateView'
        }
    },

    reloadGrid: function () {
        this.lookupReference('usersGrid').getStore().reload();
    },

    updateView: function (hash) {
        this.getView().setActiveTab(hash);

    },

    doRoute: function (btn, evt) {
        this.redirectTo(btn.redirectPath);

    },

    onRemoveUser: function (btn) {
        alert(btn.getWidgetRecord().get('username'))

    },


    /***************************
     *                           *
     * EDIT USER FUNTIONALITY    *
     *                           *
     ****************************/

    onEditUser: function (btn) {
        var win = Ext.create('Ext.window.Window', {
            bodyPadding: 10,
            title: 'Edit user',
            items: [{
                xtype: 'userEditForm'
            }]
        });
        var user = btn.getWidgetRecord();
        win.down('form').loadRecord(user);
        win.userToEdit = user;
        win.show()

    },


    /***************************
     *                           *
     * CREATE USER FUNCTIONALITY *
     *                           *
     ****************************/

    onCreateUser: function (btn) {
        var win = Ext.create('Ext.window.Window', {
            bodyPadding: 10,
            title: 'New user',
            items: [{
                xtype: 'userCreationForm'
            }]
        });
        win.show()

    },


    /*****************************
     *                           *
     * DELETE USER FUNCTIONALITY *
     *                           *
     *****************************/


    onDeleteUser: function (btn) {
        var me = this;
        var userJson = JSON.stringify({
            userId: btn.getWidgetRecord().get("userId")
        });
        var url = '/users';
        var request = VramApp.utilities.HTTPRequests;

        var success = function (response, opts) {
            var jsonResp = Ext.decode(response.responseText);
            me.reloadGrid();
            me.setUserDeletionSuccessMessage('User \'' + jsonResp.username + '\' deleted', me)
        };

        var failure = function (response, opts) {
            var jsonResp = Ext.decode(response.responseText);
            me.setUserDeletionErrorMessage(jsonResp.errorMessage, me)
        };

        Ext.Msg.show({
            title: 'Delete user?',
            message: 'This will permanently delete the user, do you wish to proceed?',
            buttons: Ext.Msg.YESNOCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn === 'yes') {
                    request.DELETE(url, userJson, success, failure)
                }
            }
        });
    },

    setUserDeletionErrorMessage: function (msg, me) {
        this.lookupReference('userGridMsgField').setValue('<div style="color:red;">' + msg + '</div>');
    },

    setUserDeletionSuccessMessage: function (msg, me) {
        this.lookupReference('userGridMsgField').setValue('<div style="color:green;">' + msg + '</div>');
    },

    /*******************************
     *                              *
     * EMAIL USER FUNCTIONALITY     *
     *                              *
     *******************************/

    onEmail: function (userRecord) {
        var me = this;
        if (typeof Ext.getCmp('emailwindow') === 'undefined') {
            var win = Ext.create({
                xtype: 'emailwindow'
            });



            win.on('afterrender', function () {
                me.fireEvent('onEmailWindowOpened', userRecord);
            });

            win.show();


        }


    }

});
