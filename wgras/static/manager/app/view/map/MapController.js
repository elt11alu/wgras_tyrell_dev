/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.map.MapController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.map',

    /**
     * Called when the view is created
     */
    init: function () {

    },

    onRefresh: function () {
        this.lookupReference('layerGrid').store.reload();
    },

    onLayerSelect: function (rowModel, record, index) {
        this.lookupReference('metadataForm').loadRecord(record)
        this.resetMessage()
    },

    onMetadataUpdate: function (btn) {
        var me = this;
        var values = this.lookupReference('metadataForm').getValues();
        var layerId = values.layerId;
        var description = values.description;
        var category = values.category;

        var success = function (resp) {
            var respObj = Ext.decode(resp.responseText);
            console.log(respObj);
            me.onRefresh();
            me.setSuccessMessage('Layer metadata was updated successfully!')
        };

        var fail = function (resp) {
            var respObj = Ext.decode(resp.responseText);
            console.log(respObj);

            me.setFailMessage('Layer metadata could not be updated!')
        };

        var data = JSON.stringify({
            layerId: layerId,
            description: description,
            category: category
        });

        VramApp.utilities.HTTPRequests.PUT('/metadata', data, success, fail);
    },

    setSuccessMessage: function (message) {
        var msg = '<p style="color:#45d099"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>&nbsp; ' + message + '</i></p>'
        this.lookupReference('messageField').setValue(msg)
    },

    setFailMessage: function (message) {
        var msg = '<p style="color:#d03f3c"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>&nbsp; ' + message + '</i></p>'
        this.lookupReference('messageField').setValue(msg)
    },
    resetMessage: function () {
        this.lookupReference('messageField').setValue('')
    },

    addToMap: function (btn) {
        var map = Ext.getCmp('mapView').getMap();
        var rec = btn.getWidgetRecord();
        this.lookupReference('metadataForm').loadRecord(rec);
        this.resetMessage();
        var layerName = rec.get('layerName');

        var workspace = layerName.split(':')[0];
        var URL = VramApp.WGRASURL.GEOSERVERURL().concat('wms');
        this.layer ? map.removeLayer(this.layer) : false;
        var layer = new ol.layer.Tile({
            wrapX: false,
            source: new ol.source.TileWMS({
                crossOrigin: 'Anonomous',
                url: URL,
                projection: 'EPSG:3857',
                params: {
                    'LAYERS': layerName,
                    'CRS': 'EPSG:3857',
                    'Tiled': true
                },
                serverType: 'geoserver'
            })
        });
        this.layer = layer;
        map.addLayer(layer);
        var me = this;
        var success = function (resp) {
            var obj, bbox, jsonResp;
            jsonResp = Ext.decode(resp.responseText);
            obj = jsonResp.extent;
            bbox = [parseFloat(obj.minx), parseFloat(obj.miny), parseFloat(obj.maxx), parseFloat(obj.maxy)];
            me.panTo4326Extent(bbox);
        };
        var fail = function (resp) {
        };
        this._get_layer_extent(rec.get('layerId'), success, fail)
    },

    _get_layer_extent: function (layerId, success, fail) {
        var map = Ext.getCmp('mapView');
        var loader = VramApp.utilities.Loader.getLoader(map,'Loading..');
        loader.show();
        Ext.Ajax.request({
            url: '/layer/extent/'.concat(layerId),
            method: 'GET',

            success: function (response, opts) {
                loader.hide()
                success(response, id);
            },

            failure: function (response, opts) {
                loader.hide()
                fail(response, id);

            }
        });
    },

    panTo4326Extent: function (extent) {
        var EXTENT, SIZE, OPTIONS;
        try {
            if (!this._valid4326Extent(extent)) {
                extent = this._fix4326Extent(extent)
            }

            // [minx, miny, maxx, maxy]
            EXTENT = this.reprojectExtentTo3857(extent, 'EPSG:4326');
            SIZE = Ext.getCmp('mapView').getMap().getSize();
            OPTIONS = {
                maxZoom: 12
            };
            Ext.getCmp('mapView').getView().fit(EXTENT, SIZE, OPTIONS);

        } catch (e) {

        }
    },

    _valid4326Extent: function (extent) {
        if (extent[0] < -180 || !isFinite(extent[0])) {
            return false;
        }
        if (extent[1] < -90 || !isFinite(extent[1])) {
            return false;
        }
        if (extent[2] > 180 || !isFinite(extent[2])) {
            return false;
        }
        if (extent[3] > 90 || !isFinite(extent[3])) {
            return false;
        }
        return true;
    },

    _fix4326Extent: function (extent) {
        if (extent[0] < -180 || !isFinite(extent[0])) {
            extent[0] = -170;
        }
        if (extent[1] < -90 || !isFinite(extent[1])) {
            extent[1] = -85;
        }
        if (extent[2] > 180 || !isFinite(extent[2])) {
            extent[2] = 180;
        }
        if (extent[3] > 90 || !isFinite(extent[3])) {
            extent[3] = 85;
        }
    },

    reprojectExtentTo3857: function (extent, from) {
        if (extent[0] < -170 || !isFinite(extent[0])) {
            extent[0] = -170;
        }
        if (extent[1] < -85 || !isFinite(extent[1])) {
            extent[1] = -85;
        }
        if (extent[2] > 175 || !isFinite(extent[2])) {
            extent[2] = 175;
        }
        if (extent[3] > 85 || !isFinite(extent[3])) {
            extent[3] = 85;
        }

        return ol.proj.transformExtent(extent, from, 'EPSG:3857');
    },

    onFilterByWorkspace: function (combo, record) {
        var grid = this.lookupReference('layerGrid'),
            workspaceName = record.get('name'),
            filters = grid.store.getFilters();

        if (workspaceName) {
            try{
                filters.remove(this.workspaceFilter);
            }catch (err){
                console.log('Filters empty')
            }

            this.workspaceFilter = filters.add({
                id: 'nameFilter',
                property: 'workspaceName',
                value: workspaceName,
                anyMatch: true,
                caseSensitive: false
            });
        }

    },

    resetFilter: function(btn){
        var grid = this.lookupReference('layerGrid'),
            store = grid.store;

            try{
                store.clearFilter();
            }catch (err){
                console.log('Filters empty')
            }
    }
});
