/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.map.MapGrid', {
    extend: 'Ext.panel.Panel',

    xtype: 'mapgrid',
    controller: 'map',
    viewModel: {
        type: 'mapviewmodel'
    },

    requires: [
        'Ext.button.Button',
        'Ext.grid.column.Widget',
        'VramApp.WGRASURL',
        'VramApp.store.LayerStore',
        'VramApp.view.map.MapController',
        'Ext.grid.column.Check',
        'VramApp.view.map.MapViewModel'
    ],
    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'grid',
            reference: 'layerGrid',
            flex: 1,
            scrollable: true,
            title: 'Layers',

            border: 1,
            cls: 'mapGrid',

            store: {
                type: 'layers'
            },

            tools: [{
                type: 'refresh',
                handler: 'onRefresh',
                tooltip: 'Reload Data'
            }],

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Filter by workspace',
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: 'name',
                        bind: {
                            store: '{workspaces}'
                        },
                        listeners: {
                            select: 'onFilterByWorkspace'
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'View All',
                        handler: 'resetFilter'
                    }
                ]
            }],

            columns: [
                {
                    text: 'Layer',
                    dataIndex: 'layerName',
                    flex: 3
                },
                {
                    text: 'Show',
                    flex: 1,
                    xtype: 'widgetcolumn',
                    widget: {
                        width: 30,
                        glyph: 0xf041,
                        xtype: 'button',
                        tooltip: 'Preview',
                        dataIndex: 'layerName',
                        handler: 'addToMap',
                        style: {
                            backgroundColor: '#b3d9ff',
                            border: 0
                        }
                    }
                }
            ],
            listeners: {
                select: 'onLayerSelect'
            }
        },
        {
            xtype: 'form',
            reference: 'metadataForm',
            flex: 1,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Layer Metadata',
                    defaultType: 'textfield',
                    defaults: {
                        anchor: '100%'
                    },

                    items: [
                        {
                            xtype: 'displayfield',
                            value: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; You may alter metadata by selecting record in the list and provide new entries below. <i>Please note: only description and category can be changed here.</i></p>'
                        },
                        {
                            xtype: 'displayfield',
                            reference: 'messageField',
                            value: ''
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Name',
                            name: 'layerName',
                            emptyText: 'Name',
                            readOnly: true
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Title',
                            name: 'layerTitle',
                            emptyText: 'Title',
                            readOnly: true
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Workspace',
                            name: 'workspaceName',
                            emptyText: 'Workspace',
                            readOnly: true
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Category',
                            name: 'category',
                            emptyText: 'Category',
                            regex: new RegExp("^[a-zA-Z0-9_ -]{3,40}$"),
                            invalidText: 'Use only a-z, 0-9 and _'
                        },
                        {
                            allowBlank: false,
                            fieldLabel: 'Description',
                            name: 'description',
                            emptyText: 'Description',
                            xtype: 'textareafield',
                            regex: new RegExp("^[a-zA-Z0-9_\n .,]{0,300}$"),
                            invalidText: 'Use only a-z, 0-9 and _ . ,'
                        },
                        {
                            name: 'layerId',
                            hidden: true
                        },
                        {
                            xtype: 'button',
                            text: 'Submit',
                            anchor: '30%',
                            handler: 'onMetadataUpdate'
                        }
                    ]
                }
            ]

        }
    ]

    /*"category": "Private layers",
     "description": "A LineString layer created by antonlundkvist",
     "layerId": "d3d18970-7113-484d-84c3-4d4332205a2f",
     "layerName": "antonlundkvist_linestring",
     "layerTitle": "LineString",
     "maxx": 0.0,
     "maxy": -1.0,
     "minx": 0.0,
     "miny": -1.0,
     "store": "private_antonlundkvist_549",
     "type": "Raster",
     "workspaceName": "private_antonlundkvist"
     */

});
