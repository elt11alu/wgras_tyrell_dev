/**
 * Created by anton on 12/12/16.
 */

Ext.define('VramApp.view.map.MapViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.mapviewmodel',

    data: {},

    stores: {
        workspaces: {

            fields: [
                {name: 'name'},
                {name: 'workspaceId'}
            ],

            autoLoad: true,

            proxy: {
                type: 'ajax',
                url: '/workspaces/readable',
                headers: {
                    'Accept': 'application/json'
                },
                reader: {
                    type: 'json',
                    rootProperty: 'workspaces'
                }
            }
        }


    }
});
