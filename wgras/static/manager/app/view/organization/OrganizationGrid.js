/**
 * Created by anton on 2016-07-14.
 */
Ext.define('VramApp.view.organization.OrganizationGrid',{
    extend: 'Ext.grid.Panel',


    xtype: 'organizationgrid',
    reference: 'organizationgrid',

    requires: [
        'VramApp.store.Organizations',
        'VramApp.view.organization.OrganizationGridController'
    ],

    controller: 'organizationgridcontroller',

    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'reloadGrid'
        },
        {
            type: 'print',
            tooltip: 'Print list'
        }

    ],

    tbar: [
        {
            xtype: 'button',
            text: 'Add organization',
            glyph:0xf067,
            handler:'onCreateOrganization'
        },
        {
            xtype:'displayfield',
            reference:'organizationGridMsgField'
        }
    ],

    store: {
        type: 'organizations'
    },

    title: 'Organizations',

    columns: [
        {
            text: 'Full name',
            flex: 1,
            sortable: true,
            dataIndex: 'fullname'
        },
        {
            text: 'Short name',
            flex: 1,
            sortable: false,
            dataIndex: 'shortname'
        },
        {
            text: 'Country',
            flex: 1,
            sortable: false,
            dataIndex: 'countryName'
        },

        {
            text: 'Admins',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf007 ,
                width: 40,
                xtype: 'button',
                dataIndex: 'orgid',
                tooltip: 'View admins',
                style: {
                    backgroundColor: 'LightBlue',
                    border: 0
                },
                handler: 'onViewAdmins'

            }
        },

        {
            text: 'Delete',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf014,
                width: 40,
                xtype: 'button',
                dataIndex: 'orgid',
                tooltip: 'Delete organization',
                style: {
                    backgroundColor: 'LightCoral',
                    border: 0
                },
                handler: 'onDeleteOrganization'

            }
        }
    ]
});