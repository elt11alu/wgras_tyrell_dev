/**
 * Created by anton on 2016-07-14.
 */


Ext.define('VramApp.view.organization.OrganizationGridController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'VramApp.view.organization.viewOrganizationAdmin.OrganizationAdminWindow',
        'VramApp.view.organization.addOrganization.AddOrganization'
    ],

    alias: 'controller.organizationgridcontroller',

    id: 'organizationgridcontroller',

    listen: {
        controller: {
            '#addorganizationcontroller': {
                onOrganizationAdded: function (msg) {
                    this.setSuccessMessage(msg);
                    this.reloadGrid()
                },
                onFail: function (msg) {
                    this.setFailMessage(msg)
                }
            }
        }
    },


    init: function () {

    },

    onViewAdmins: function (btn) {
        var me = this;
        var orgid = btn.getWidgetRecord().get('orgid')
        var win = Ext.create({
            xtype: 'organizationadminwindow'
        });
        win.on('afterrender', function () {
            me.fireEvent('onOrganizationAdminWindowOpen', orgid)
        });
        win.show();
    },

    onCreateOrganization: function () {
        Ext.create({
            xtype: 'addOrganizationWindow'
        }).show();
    },

    onDeleteOrganization: function (btn) {
        var me = this;
        var record = btn.getWidgetRecord();
        var orgname = record.get('fullname');
        var country = record.get('isocode');
        Ext.MessageBox.confirm(
            'Confirm', 'Are you sure you want to delete organization '.concat(orgname,'. All associated admins in this organization will also be deleted.'),
            function (resp) {
                if (resp === 'yes') {
                    me.onDeleteOrg(btn);
                }
            });
    },

    onDeleteOrg: function (btn) {
        var grid = this.getView();
        var record = btn.getWidgetRecord();
        var orgId = record.get('orgid');

        var loadMask = new Ext.LoadMask({
            msg: 'Please wait...',
            target: grid
        });
        loadMask.show();

        var me = this;

        var success = function (resp) {
            var obj = Ext.decode(resp.responseText);
            me.setSuccessMessage('Organization '.concat('\'', obj.organization.fullname, '\'', ' was deleted'))
            me.reloadGrid();
            loadMask.hide();
        };

        var fail = function (resp) {
            var obj = Ext.decode(resp.responseText);
            me.setFailMessage('Organization could not be deleted.');
            loadMask.hide();
        };

        var data = JSON.stringify({'orgid': orgId});

        VramApp.utilities.HTTPRequests.DELETE('/organizations', data, success, fail);


    },


    setSuccessMessage: function (msg) {
        this.lookupReference('organizationGridMsgField').setValue('<p style="color:green">' + msg + '</p>')
    },

    setFailMessage: function (msg) {
        this.lookupReference('organizationGridMsgField').setValue('<p style="color:red">' + msg + '</p>')
    },

    reloadGrid: function () {
        Ext.data.StoreManager.lookup('organizations').reload();
    }


});