/**
 * Created by anton on 2016-07-31.
 */

Ext.define('VramApp.view.organization.addOrganization.AddOrganizationController',{
    extend: 'Ext.app.ViewController',
    
    alias: 'controller.addorganizationcontroller',
    
    id: 'addorganizationcontroller',
    
    onPublishOrganization: function (btn) {
        var values = btn.up('form').getValues();

        var me = this;
        var win = btn.up('window');
        var loadMask = new Ext.LoadMask({
            msg: 'Please wait...',
            target: win
        });

        var success = function (resp) {
            var obj = Ext.decode(resp.responseText);
            loadMask.hide();
            me.onPublishSuccess(obj)

        };
        var fail = function (resp) {
            var obj = Ext.decode(resp.responseText);
            me.onPublishFail();
            loadMask.hide();
        };

        var data = JSON.stringify(values);
        loadMask.show();
        VramApp.utilities.HTTPRequests.POST('/organizations', data, success, fail);
    },

    onPublishSuccess: function (obj, win) {
        this.fireEvent('onOrganizationAdded','Organization '.concat('\'',obj.organization.fullname,'\'', ' was added.'));
        this.getView().close();
    },

    onPublishFail: function(obj){
        this.fireEvent('onFail', 'Organization could not be added.')
    }
});
