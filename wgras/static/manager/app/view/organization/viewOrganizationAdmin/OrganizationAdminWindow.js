/**
 * Created by anton on 2016-07-31.
 */

Ext.define('VramApp.view.organization.viewOrganizationAdmin.OrganizationAdminWindow',{
    extend: 'Ext.window.Window',

    requires: [
        'VramApp.view.organizationadmin.OrganizationAdminGrid'
    ],

    title: 'Organization admins',
    viewModel: true,
    width: 500,
    height: 500,
    bodyPadding: 10,
    scrollable:true,

    xtype: 'organizationadminwindow',

    items:[
        {
            xtype: 'organizationadmingrid'
        }
    ]
});
