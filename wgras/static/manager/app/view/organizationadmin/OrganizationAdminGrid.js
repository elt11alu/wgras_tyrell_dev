/**
 * Created by anton on 2016-07-31.
 */

Ext.define('VramApp.view.organizationadmin.OrganizationAdminGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'organizationadmingrid',
    reference: 'organizationadmingrid',
    controller: 'organizationadmingridcontroller',
    requires: [
        'VramApp.store.OrganizationAdmin',
        'VramApp.view.organizationadmin.OrganizationAdminGridController'
    ],

    tools: [
        {
            type: 'refresh',
            tooltip: 'Refresh',
            handler: 'loadOrganizationAdminStore'
        },
        {
            type: 'print',
            tooltip: 'Print list'
        }

    ],

    tbar: [
        {
            xtype: 'button',
            text: 'Add admin',
            glyph:0xf067,
            handler:'onCreateOrgAdmin'
        },
        {
            xtype:'displayfield',
            reference:'orgAdminGridMsgField'
        }
    ],

    store: {
        type: 'organizationadminstore'
    },

    title: 'Organization admin',

    columns: [
        {
            text: 'Name',
            flex: 1,
            sortable: true,
            dataIndex: 'username'
        },
        {
            text: 'Country',
            flex: 1,
            sortable: false,
            dataIndex: 'countryName'
        },
        {
            text: 'Organization',
            flex: 1,
            sortable: false,
            dataIndex: 'organization'
        },

                {
            text: 'Email',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf003,
                width: 40,
                xtype: 'button',
                dataIndex: 'userId',
                tooltip: 'Send email',
                style: {
                    backgroundColor: 'LightBlue',
                    border: 0
                },
                handler: 'onEmail'

            }
        },

        {
            text: 'Delete',
            width: 80,
            xtype: 'widgetcolumn',
            padding:3,
            widget: {
                glyph: 0xf014,
                width: 40,
                xtype: 'button',
                dataIndex: 'userId',
                tooltip: 'Delete admin',
                style: {
                    backgroundColor: 'LightCoral',
                    border: 0
                },
                handler: 'onDeleteOrgAdmin'

            }
        }
    ]
});
