/**
 * Created by anton on 2016-07-31.
 */

Ext.define('VramApp.view.organizationadmin.OrganizationAdminGridController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'VramApp.view.organizationadmin.addOrganizationAdmin.AddOrganizationAdmin'
    ],

    alias: 'controller.organizationadmingridcontroller',
    id: 'organizationadmingridcontroller',

    listen: {
        controller:{
            '#organizationgridcontroller':{
                onOrganizationAdminWindowOpen: function(orgid){
                    this.orgid = orgid;
                    this.loadOrganizationAdminStore();
                }
            },
            '#addorganizationadmincontroller':{
                onUserAdded: function(msg){
                    this.loadOrganizationAdminStore();
                    this.setSuccessMessage('User '.concat(msg, ' was added.'))
                },

            },
            '#organizationadmingridcontroller':{
                onUserDeleted: function(msg){
                    this.loadOrganizationAdminStore();
                    this.setSuccessMessage('User '.concat(msg, ' was deleted.'))
                },
                onFail: function(msg){
                    this.setFailMessage(msg);
                    this.loadOrganizationAdminStore();
                }
            }
        }
    },

    init: function(view){
    },

    loadOrganizationAdminStore: function(){
        var panel = this.getView();
        Ext.data.StoreManager.lookup('organizationadminstore').loadAdmins(this.orgid, panel);
    },

    onCreateOrgAdmin: function(){
        var me = this;
        var win = Ext.create({
            xtype: 'addOrganizationAdminWindow'
        });

        win.on('afterrender',function(){
            me.fireEvent('onAddOrganizationAdminWindowOpen', me.orgid);
        });

        win.show();


    },

    onDeleteOrgAdmin: function(btn){
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete '.concat(btn.getWidgetRecord().get('username'),'?'), function(button){
            if (button==='yes'){
                this.onDeleteAdmin(btn)
            }
        }, this);
    },

    onDeleteAdmin: function(btn){

        var adminId = btn.getWidgetRecord().get('userId');
        var me = this;
        var success = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.onDeleteSuccess(obj);

        };
        var fail = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.onDeleteFail(obj)
        };

        var data = JSON.stringify({'userId':adminId});

        VramApp.utilities.HTTPRequests.DELETE('/users/orgadmins',data,success,fail);



    },

    onDeleteSuccess: function(obj){
        var username = obj.orgadmin.username;
        this.fireEvent('onUserDeleted', username);
        Ext.toast('User '.concat(username, ' was deleted.'));
    },

    onDeleteFail: function(obj){
        var error = obj.errorMessage;
        this.fireEvent('onFail', error);
        Ext.toast(error);
    },

    setSuccessMessage: function(msg){
        this.lookupReference('orgAdminGridMsgField').setValue('<p style="color:green">' + msg + '</p>');
    },

    setFailMessage: function(msg){
        this.lookupReference('orgAdminGridMsgField').setValue('<p style="color:red">' + msg + '</p>');
    }


});
