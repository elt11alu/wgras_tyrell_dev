Ext.define('VramApp.view.organizationadmin.addOrganizationAdmin.AddOrganizationAdminController', {
    extend: 'Ext.app.ViewController',

    requires: [],

    alias: 'controller.addorganizationadmincontroller',
    id: 'addorganizationadmincontroller',

        listen: {
        controller: {
            '#organizationadmingridcontroller':{
                onAddOrganizationAdminWindowOpen: function(orgid){
                    this.orgid = orgid;
                }
            },
            
        }
    },

    init: function () {
    },

    onCreateOrganizationAdmin: function (btn) {
        var values = this.lookupReference('addorganizationadminform').getValues();
        var msg = '';
        var me = this;
        var loader = VramApp.utilities.Loader.getLoader(this.getView(), 'Creating user and setting permissions, please wait..');
        if (values.pw1 === values.pw2) {
            values['orgid'] = me.orgid;
            var success = function (resp) {
                loader.hide();
                var obj = Ext.decode(resp.responseText)
                me.fireEvent('onUserAdded', obj.orgadmin.username);
                me.closeWindow();
            };
            var fail = function (resp) {
                loader.hide();
                var obj = Ext.decode(resp.responseText);
                me.fireEvent('onFail', obj.errorMessage);
                me.setAddAdminErrorMessage(obj.errorMessage);
            };
            var data = JSON.stringify(values);
            loader.show();
            VramApp.utilities.HTTPRequests.POST('/users/orgadmins', data, success, fail);
        } else {
            msg = 'Please confirm password.';
            alert(msg)
        }
    },

    setAddAdminErrorMessage: function(msg){
        this.lookupReference('addOrganizationAdminMsgField').setValue('<p style="color:red">'+msg+'</p>')
    },

    closeWindow: function(){
        this.getView().close();
    }


});
