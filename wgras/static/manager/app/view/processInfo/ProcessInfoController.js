/**
 * Created by anton on 8/4/16.
 */

Ext.define('VramApp.view.processInfo.ProcessInfoController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.processinfocontroller',
    id: 'preocessinfocontroller',

    requires: [
        'VramApp.utilities.HTTPRequests'
    ],

    listen: {
        controller: {
            '*': {
                onProcessInitiated: 'initiateProcessInfo',
                onProcessUpdated: 'updateProcessInfo'
            }
        }
    },

    init: function () {


    },

    initiateProcessInfo: function (processInfo) {
        if (!this._existsModel(processInfo.processId)) {
            var process = Ext.create('VramApp.model.ProcessInfo', {
                processName: processInfo.processName,
                processStatus: processInfo.processStatus,
                processId: processInfo.processId,
                processResult: processInfo.processResult
            });
            Ext.data.StoreManager.lookup('processinfo').insert(0, process);
        }


    },

    updateProcessInfo: function (processInfo) {
        if (this._existsModel(processInfo.processId)) {
            Ext.data.StoreManager.lookup('processinfo').getById(processInfo.processId).set({
                'processStatus': processInfo.processStatus,
                processResult: processInfo.processResult
            })
        }
    },

    _onProcessCompleted: function (processInfo) {
        if (this._existsModel(processInfo.processId)) {
            Ext.data.StoreManager.lookup('processinfo').getById(processInfo.processId).set({
                'processStatus': processInfo.processStatus,
                processResult: processInfo.processResult
            })
        }
    },

    _onProcessFailed: function (processInfo) {
        if (this._existsModel(processInfo.processId)) {
            Ext.data.StoreManager.lookup('processinfo').getById(processInfo.processId).set({
                'processStatus': processInfo.processStatus,
                processResult: processInfo.processResult
            })
        }

    },

    _existsModel: function (processId) {
        return Ext.data.StoreManager.lookup('processinfo').getById(processId) != null
    }


});

