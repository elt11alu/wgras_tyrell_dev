/**
 * Created by anton on 8/4/16.
 */

Ext.define('VramApp.view.sendEmail.EmailWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'VramApp.view.sendEmail.EmailWindowController'
    ],

    controller: 'emailwindowcontroller',
    id: 'emailwindow',

    xtype: 'emailwindow',

    width: 500,
    bodyPadding: 10,
    resizable: false,

    items: [
        {
            xtype: 'form',
            reference: 'emailform',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Send email',
                    defaults: {
                        anchor: '100%'
                    },

                    items: [
                        {
                            xtype: 'textfield',
                            allowBlank: false,
                            fieldLabel: 'User',
                            name: 'username',
                            emptyText: 'User',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            allowBlank: false,
                            fieldLabel: 'Email',
                            name: 'email',
                            emptyText: 'email adress',
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            allowBlank: false,
                            fieldLabel: 'Subject',
                            name: 'subject',
                            emptyText: 'Subject',
                            readOnly: false,
                            validator: function (val) {
                                var errMsg = 'Use only \'a-z, A-Z, . ! ?\'';
                                var reg = new RegExp("^[a-zA-Z0-9 ,.!?]+$");
                                var valid = reg.test(val);
                                return valid ? true : errMsg;
                            }
                        },
                        {
                            xtype: 'textarea',
                            allowBlank: false,
                            fieldLabel: 'Message',
                            name: 'message',
                            emptyText: 'Message',
                            validator: function (val) {
                                var errMsg = 'Use only \'a-z, A-Z, 0-9 . ! ? _ - () ""\'';
                                var reg = new RegExp("^[a-zA-Z0-9 ,.!?()\"\'\r\n]+$");
                                var valid = reg.test(val);
                                return valid ? true : errMsg;
                            }
                        }
                    ]
                }
            ],
            buttons: [{
                text: 'Send',
                formBind: true,
                handler: 'sendEmail'
            }]
        }

    ]
});
