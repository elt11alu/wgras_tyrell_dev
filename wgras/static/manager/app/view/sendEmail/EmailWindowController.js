/**
 * Created by anton on 8/4/16.
 */

Ext.define('VramApp.view.sendEmail.EmailWindowController',{
    extend: 'Ext.app.ViewController',

    alias: 'controller.emailwindowcontroller',
    
    listen: {
        controller:{
            '#maincontroller':{
                onEmailWindowOpened: 'loadUserToForm'
            }
        }
    },

    init: function(){
    },

    loadUserToForm: function(userRecord){
        this.lookupReference('emailform').loadRecord(userRecord)
    },

    sendEmail: function(){
        var values = this.lookupReference('emailform').getValues();
        var data = JSON.stringify(values);
        var me = this;

        var success = function(resp){
            var obj = Ext.decode(resp.responseText);
            me.checkStatus(obj);
        };

        var fail = function(resp){
            var obj = Ext.decode(resp.responseText);
        };

        VramApp.utilities.HTTPRequests.POST('/sendEmail', data, success, fail)
    },

    checkStatus: function (obj) {
        var url = obj.emailStatus;
        var processId = obj.processId;
        var me = this;
        var success = function (resp, id) {
            var obj = Ext.decode(resp.responseText);
            if (obj.state == 'SUCCESS') {
                clearInterval(id);
                setTimeout(function(){
                    Ext.Msg.alert('Email', obj.processResult);
                    me.fireEvent('onProcessUpdated', {'processName': 'Email', 'processStatus': obj.state,'processId':processId, 'processResult': obj.processResult})
                }, 1000);


            } else if (obj.state == 'FAILURE') {
                clearInterval(id);
                me.fireEvent('onProcessUpdated', {'processName': 'Email', 'processStatus': obj.state,'processId':processId,'processResult': obj.processResult})
            }else{
                me.fireEvent('onProcessInitiated', {'processName': 'Email', 'processStatus': obj.state, 'processId':processId,'processResult': obj.processResult})
            }
        };
        var fail = function (resp, id) {
            clearInterval(id);
        };
        var id = window.setInterval(function () {
            VramApp.utilities.HTTPRequests.GET(url, success, fail, id)
        }, 500);

    },

});
