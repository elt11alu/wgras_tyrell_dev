/**
 * Created by anton on 6/2/16.
 */

Ext.define("VramApp.view.upload.uploadCSVWin.UploadCSVWindowController", {
    extend: 'Ext.app.ViewController',
    alias: "controller.uploadcsvwindowcontroller",

    listen: {
        controller: {
            '*': {
                'csvUploaded': 'setcsvDetails'
            }
        }
    },

    init: function (view) {
        store = Ext.StoreManager.lookup('workspacestore');
        store.load()
    },



    setcsvDetails:function(csvInfo){
        this.lookupReference('fileName').setValue(csvInfo.fileName);
    },

    onUploadCSV:function(){
        var layerName, fileName, workspaceId, me, category, description;
        layerName = this.lookupReference('layerName').getValue();
        fileName = this.lookupReference('fileName').getValue();
        workspaceId =  this.lookupReference('existingWorkspaceName').getValue();
        description = this.lookupReference('description').getValue();
        category = this.lookupReference('category').getValue();
        me = this;

        Ext.Ajax.request({
            url: '/vector/csv/createLayer',
            method:'POST',
            params:{
                storeName: layerName,
                fileName: fileName,
                workspaceId: workspaceId,
                category: category,
                description: description

            },
            success: function(response, opts) {
                var obj, msg;
                obj = Ext.decode(response.responseText);
                me.setSuccessMsg(obj.msg)

            },

            failure: function(response, opts) {
                var obj, msg;
                obj = Ext.decode(response.responseText);
                me.setFailMsg(obj.errorMessage)

            }
        });

    },

    setSuccessMsg: function(msg){
        this.lookupReference('msgField').setValue('<span style="color:green">'+msg+'</span>')
    },

    setFailMsg: function(msg){
        this.lookupReference('msgField').setValue('<span style="color:red">'+msg+'</span>')
    },

    onClose:function(){
        this.getView().close();
        this.fireEvent('onUploadCSVWindowClosed');
    }

})
