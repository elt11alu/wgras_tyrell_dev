Ext.define('VramApp.view.upload.uploadMainWin.Upload', {
    extend: 'Ext.panel.Panel',

    xtype: 'uploadForm',
    title: 'Upload',
    controller: 'uploadcontroller',
    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
    

    items: [{
        xtype: 'form',
        border:true,
        bodyPadding:10,
        reference: 'uploadForm',
        layout: 'anchor',
         defaults: {
             anchor: '50%'
 },
        flex: 1,
        margin: 5,
        items: [
            {
                xtype: 'displayfield',
                name: 'infoField',
                value: '<h2>Upload Shapefile</h2>'
            },
            {
                xtype: 'displayfield',
                name: 'instructions',
                value: '<b>Instructions</b><br>' +
                '<ol>' +
                '<li>Choose zipped shapefile to upload.</li>' +
                '<li>Press preview to see the details of the file.</li>' +
                '<li>Follow instructions to upload the geometry to the database.</li>' +
                '</ol>' +
                '<i><b>Note: The zipfile can only contain one shapefile.</b></i>'
            },
            {
                xtype: 'filefield',
                name: "file",
                reference: 'shpfilefield',
                allowBlank: false,
                fieldLabel: 'Choose shapefile (zip)'
            }, {
                xtype: 'button',
                scale: 'large',
                reference: 'previewShapefileBtn',
                anchor: '20%',
                text: 'Preview shapefile',
                formBind: true,
                handler: "onSubmitShapeFile"

            }, {
                xtype: 'displayfield',
                margin: 3,
                maxHeight:200,
                reference: 'MsgField',
                style: 'opacity:0;',
                scrollable:true
            }]

    },{
        xtype: 'form',
        border:true,
        bodyPadding:10,
        reference: 'uploadCoverageForm',
        layout: 'anchor',
         defaults: {
            anchor: '50%'
         },
        flex: 1,
        margin: 5,
        items:[
            {
                xtype: 'displayfield',
                name: 'infoField',
                value: '<h2>Upload GeoTIFF</h2>'
            },
            {
                xtype: 'displayfield',
                name: 'instructions',
                value: '<b>Instructions</b><br>' +
                '<ol>' +
                '<li>Choose GeoTIFF to upload.</li>' +
                '<li>Press preview to see the details of the file.</li>' +
                '<li>Follow instructions to upload the geometry.</li>' +
                '</ol>'
            },
            {
                xtype: 'filefield',
                name: "file",
                reference: 'geotifffilefield',
                allowBlank: false,
                fieldLabel: 'Choose GeoTIFF'
            }, {
                xtype: 'button',
                scale: 'large',
                anchor: '20%',
                text: 'Preview GeoTIFF',
                formBind: true,
                handler: "onSubmitGeotiff"

            }
        ]
    },
        /*{
        xtype: 'form',
        border:true,
        bodyPadding:10,
        reference: 'uploadCsvForm',
        layout: 'anchor',
         defaults: {
            anchor: '50%'
         },
        flex: 1,
        margin: 5,
        items:[
            {
                xtype: 'displayfield',
                name: 'infoField',
                value: '<h2>Upload CSV</h2>'
            },
            {
                xtype: 'displayfield',
                name: 'instructions',
                value: '<b>Instructions</b><br>' +
                '<ol>' +
                '<li>Choose CSV to upload.</li>' +
                '<li>Press preview to see the details of the file.</li>' +
                '<li>Follow instructions to upload the geometry.</li>' +
                '</ol>'
            },
            {
                xtype: 'filefield',
                name: "file",
                reference: 'csvfilefield',
                allowBlank: false,
                fieldLabel: 'Choose GeoTIFF'
            }, {
                xtype: 'button',
                scale: 'large',
                anchor: '20%',
                text: 'Preview CSV',
                formBind: true,
                handler: "onSubmitCSV"

            }
        ]
    }*/

    ]


});
