Ext.define("VramApp.view.upload.uploadMainWin.UploadController", {
    extend: 'Ext.app.ViewController',
    alias: "controller.uploadcontroller",
    requires: [
        'VramApp.view.upload.uploadShpWin.UploadShpWindow',
        'VramApp.view.upload.uploadGeoTIFFWin.UploadGeoTIFFWindow',
        'VramApp.view.upload.uploadCSVWin.UploadCSVWindow',
        'VramApp.utilities.HTTPRequests'
        
    ],

    listen: {
        controller: {
            '*':{
                onUploadShapefileWindowClosed : 'disableShapefileSubmitButton',
                onUploadGeoTIFFWindowClosed : 'disableGeoTIFFSubmitButton',
                onUploadCSVWindowClosed : 'disableCSVSubmitButton'
            }
        }
    },

    init: function() {
        
    },

    disableShapefileSubmitButton: function(){
        this.lookupReference('shpfilefield').validate()
    },

    disableGeoTIFFSubmitButton: function(){
        this.lookupReference('geotifffilefield').validate()
    },

    disableCSVSubmitButton: function(){
        this.lookupReference('csvfilefield').validate()
    },

    onSubmitShapeFile: function() {
        var me, form;
        me = this;
        me.lookupReference('MsgField').setValue(' ');
        if (this.getView()) {
            form = this.lookupReference('uploadForm').getForm();
            form.submit({
                url: "/vector/zip",
                waitMsg: 'Extracting information...',
                success: function(form, action) {
                    me.setSuccessMessage(action.result.msg);
                    me.displayShpInfo(action.result, me);
                },
                failure: function(form, action) {
                    me.setErrorMessage(action.result.msg);
                }
            });
        }
    },

    setErrorMessage: function(message) {
        var field;
        field = this.lookupReference('MsgField');
        field.setStyle({
            "opacity": 0
        });
        field.setValue('<span style="color:red;font-size:125%;">Error: ' + message + '</span>');
        field.animate({
            duration: 1000,
            to: {
                opacity: 0.5
            }
        });
    },

    setSuccessMessage: function(message) {
        var field;
        field = this.lookupReference('MsgField');
        field.setStyle({
            "opacity": 0
        });
        field.setValue('<span style="color:green;font-size:125%;">' + message + '</span>');
        field.animate({
            duration: 1000,
            to: {
                opacity: 0.5
            }
        });
    },

    displayShpInfo: function(jsonResp, me) {
        var store, model, attrObj, win;

        win = Ext.create({
            xtype: 'uploadshpwindow'
        });


        store = Ext.StoreManager.lookup('shpattrstore');

        attrObj = {
            'nbrOfAttributes': jsonResp.nbrOfAttributes,
            'nbrOfGeometries': jsonResp.nbrOfGeometries,
            'shapeFileName': jsonResp.fileName,
            'proj': jsonResp.projection,
            'workspaces': jsonResp.workspaces,
            'mongoId': jsonResp.mongoId
        };

        /*** Loading the store ***/
        store.removeAll();
        jsonResp.attributes.forEach(function(item, index, array) {
            model = Ext.create('VramApp.model.ShpAttrModel');
            model.set({
                'attrName': item.attrName,
                'dataType': item.dataType
            });
            store.add(model);
        }, this);

        me.fireEvent('shpUploaded', attrObj);
        win.show()
    },

    openWin: function() {
        var win;
        win = Ext.create({
            xtype: 'uploadshpwindow'
        }).show()
    },





    //------------ GeoTIFF Functionality ------------//

    onSubmitGeotiff:function() {
        var me, form, statistics;
        me = this;
        me.lookupReference('MsgField').setValue(' ')
        if (this.getView()) {
            form = this.lookupReference('uploadCoverageForm').getForm();
            form.submit({
                url: "/raster/geotiff",
                waitMsg: 'Extracting information...',
                success: function(form, action) {
                    statistics = action.result;
                    me.displayGeoTIFFInfo(statistics)
                },
                failure: function(form, action) {
                }
            });
        }

    },

    displayGeoTIFFInfo: function(statistics){
        var win = Ext.create({
            xtype: 'uploadgeotiffwindow'
        }).show();
        this.fireEvent('geotiffUploaded', statistics)
         
    },

    //------------ CSV Functionality ------------//

    onSubmitCSV: function(){
        var me, form, csvInfo;
        me = this;
        me.lookupReference('MsgField').setValue(' ')
        if (this.getView()) {
            form = this.lookupReference('uploadCsvForm').getForm();
            form.submit({
                url: "/vector/csv",
                waitMsg: 'Extracting information...',
                success: function(form, action) {
                    csvInfo = action.result;
                    me.displayCSVInfo(csvInfo)
                },
                failure: function(form, action) {
                }
            });
        }
    },

    displayCSVInfo: function(csvInfo){
        var fields, win, store;


        win = Ext.create({
            xtype: 'uploadcsvwindow'
        }).show();
        this.fireEvent('csvUploaded', csvInfo)

        store = Ext.StoreManager.lookup('csvattrstore');
        fields = csvInfo.fields;
        store.removeAll()
        fields.forEach(function(item, index, array) {
            model = Ext.create('VramApp.model.CsvAttrModel');
            model.set({
                'attrName': item
            });
            store.add(model);
        }, this);
    }

});
