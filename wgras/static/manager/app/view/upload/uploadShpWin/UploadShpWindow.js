Ext.define('VramApp.view.upload.uploadShpWin.UploadShpWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'Ext.grid.Panel',
        'Ext.grid.column.Widget',
        'Ext.layout.container.Accordion',
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.panel.Panel',
        "VramApp.view.upload.uploadShpWin.UploadShpWindowController",
        'VramApp.view.upload.uploadShpWin.UploadShpWindowModel'
    ],

    xtype: 'uploadshpwindow',


    controller: 'uploadshpwindowcontroller',

    viewModel: 'uploadshpmodel',

    /*window specific configs*/
    autoShow: false,
    constrain: true,
    width: 600,
    scrollable: true,
    bodyPadding: 5,
    glyph: 0xf0c1,
    closable: true,
    title: 'Shapefile to PostGis',

    items: [{
        xtype: 'panel',
        layout: {
            type: 'accordion',
            titleCollapse: true,
            animate: true,
            activeOnTop: false
        },
        defaults: {
            bodyPadding: 5,
            margin: 5
        },
        items: [{
            title: 'Attribute fields',
            xtype: 'grid',
            viewConfig: {
                markDirty: false
            },
            border: 1,
            height: 200,
            margin: 5,
            scrollable: true,
            columnLines: true,
            flex: 1,
            store: 'shpattrstore',
            columns: [{
                xtype: 'rownumberer'
            }, {
                text: 'Attribute',
                dataIndex: 'attrName',
                flex: 1
            }, {
                text: 'Datatype',
                dataIndex: 'dataType',
                flex: 1
            }]
        }, {
            title: 'Details',
            xtype: 'form',
            layout: 'anchor',
            defaults: {
                anchor: '80%'
            },
            items: [{
                xtype: 'displayfield',
                reference: 'shapeFileName',
                fieldLabel: 'File name'

            }, {
                xtype: 'displayfield',
                reference: 'projField',
                hidden: true,
                fieldLabel: 'Projection'
            }, {
                xtype: 'displayfield',
                reference: 'nbrOfGeometries',
                fieldLabel: 'No. of Geometries'

            }, {
                xtype: 'displayfield',
                reference: 'nbrOfAttributes',
                fieldLabel: 'No. of Attributes'
            },
                {
                    xtype: 'hiddenfield',
                    name: 'mongoId',
                    reference: 'mongoId'
                }]
        }, {
            xtype: 'form',
            title: 'Upload',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Upload',
                    layout: 'anchor',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            xtype: 'displayfield',
                            value: '<b>Instructions: </b>' +
                            '<ol>' +
                            '<li>Check shapefile details</li>' +
                            '<li>Provide required information</li>' +
                            '<li>Press upload to upload the geometry to the database </li>' +
                            '</ol>'
                        }, {
                            xtype: 'textfield',
                            fieldLabel: 'Layer name',
                            name: 'layerName',
                            reference: 'layerName',
                            allowBlank: false,
                            regex: new RegExp("^[a-zA-Z0-9_]{3,20}$"),
                            regexText: 'Use only a-z A-Z _ and 0-9'
                        }, {
                            xtype: 'combo',
                            fieldLabel: 'Existing workspace',
                            name: 'existingWorkspaceName',
                            reference: 'existingWorkspaceName',
                            displayField: 'name',
                            valueField: 'workspaceId',
                            store: {
                                type: 'workspacestore'
                            },
                            id: 'useExistingWorkspace',
                            allowBlank: false
                        }, {
                            xtype: 'combo',
                            valueField: 'name',
                            displayField: 'name',
                            fieldLabel: 'Category (create new or select from list)',
                            reference: 'category',
                            regex: new RegExp("^[a-zA-Z0-9 .,]{3,50}$"),
                            regexText: 'Use only a-z A-Z  and 0-9',
                            allowBlank: false,
                            bind: {
                                store: '{categorystore}'
                            }
                        }, {
                            xtype: 'textarea',
                            fieldLabel: 'Description',
                            reference: 'description',
                            regex: new RegExp("^[a-zA-Z0-9 .,\']{3,50}$"),
                            regexText: 'Use only a-z A-Z  and 0-9',
                            allowBlank: true
                        }, {
                            xtype: 'displayfield',
                            reference: 'msgField'
                        }, {
                            xtype: 'button',
                            text: 'upload',
                            handler: 'onUploadToDb',
                            formBind: true
                        }
                    ]
                }
            ]

        }


        ]
    }

    ],
    buttons: [{
        text: 'Close',
        handler: 'onBeforeClose'
    }]
});
