Ext.define("VramApp.view.upload.uploadShpWin.UploadShpWindowController", {
    extend: 'Ext.app.ViewController',
    alias: "controller.uploadshpwindowcontroller",

    requires: [
        'VramApp.utilities.Loader'
    ],

    listen: {
        controller: {
            '*': {
                shpUploaded: 'onShpUpload'
            }
        }
    },

    init: function (view) {
    },

    onShpUpload: function (attrObj) {
        if (attrObj) {
            this.setValues(attrObj);
        }

    },

    setValues: function (obj) {
        var nbrOfAttrField, nbrOfGeomField, fileNameField, projField;
        try {
            this.lookupReference('nbrOfAttributes').setValue(obj.nbrOfAttributes);
            this.lookupReference('nbrOfGeometries').setValue(obj.nbrOfGeometries);
            this.lookupReference('shapeFileName').setValue(obj.shapeFileName);
            this.lookupReference('mongoId').setValue(obj.mongoId);
            this.lookupReference('projField').setValue(obj.proj);
        } catch (err) {
            console.log(err);
        }

    },

    onUploadToDb: function () {

        var layerName, shpFileName, workspaceId, me, loadMsk, mongoId, category, description;
        layerName = this.lookupReference('layerName').getValue();
        shpFileName = this.lookupReference('shapeFileName').getValue();
        workspaceId = this.lookupReference('existingWorkspaceName').getValue();
        mongoId = this.lookupReference('mongoId').getValue();
        description = this.lookupReference('description').getValue();
        category = this.lookupReference('category').getValue();
        loadMsk = VramApp.utilities.Loader.getLoader(this.getView(), 'Creating layer');
        me = this;

        $.get('layer/exists/'.concat(workspaceId, '/', layerName), function (resp) {
            if (resp.exists) {
                Ext.Msg.alert('Status', 'Layer name already exists, choose another name');
            } else {
                loadMsk.show();
                Ext.Ajax.request({
                    url: '/vector/zip/createLayer',
                    method: 'POST',
                    params: {
                        layerName: layerName,
                        shpfileName: shpFileName,
                        workspaceId: workspaceId,
                        mongoId: mongoId,
                        category: category,
                        description: description
                    },
                    success: function (response, opts) {
                        var obj, msg;
                        obj = Ext.decode(response.responseText);
                        msg = obj.msg;
                        me.setSuccessMsg(msg);
                        me.fireEvent('layerUploaded');
                        loadMsk.hide();
                        me.showAlertAfterUpload(msg);

                    },

                    failure: function (response, opts) {
                        var obj, msg;
                        try {
                            obj = Ext.decode(response.responseText);
                            msg = obj.msg;
                        } catch (err) {
                            msg = 'An error occured, pleas try later.'
                        } finally {
                            me.setFailMsg(msg);
                            loadMsk.hide();
                            me.showAlertAfterUpload(msg);
                        }

                    }
                });
            }
        })
    },

    onBeforeClose: function () {

        this.onClose()
    },

    setSuccessMsg: function (msg) {
        this.lookupReference('msgField').setValue('<span style="color:green">' + msg + '</span>');
        Ext.Msg.alert('Upload', msg);
    },

    setFailMsg: function (msg) {
        this.lookupReference('msgField').setValue('<span style="color:red">' + msg + '</span>');
        Ext.Msg.alert('Upload', msg);
    },

    showAlertAfterUpload: function (msg) {
        var me = this;
        Ext.Msg.show({
            title: 'Upload',
            message: msg,
            buttons: Ext.Msg.OK,
            fn: function (btn) {
                if (btn === 'ok') {
                    me.onClose()
                }
            }
        });
    },

    onClose: function () {
        this.fireEvent('onUploadShapefileWindowClosed');
        this.getView().close();

    }

});
