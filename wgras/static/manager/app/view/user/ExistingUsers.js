/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.user.ExistingUsers', {
    extend: 'Ext.panel.Panel',
    //extend: 'Ext.grid.Panel',
    xtype: 'existingusers',
    reference: 'usersGrid',
    controller: 'existinguserscontroller',

    requires: [
        'VramApp.store.UsersStore',
        'VramApp.view.user.ExistingUsersController'
    ],

    title: 'Existing users',
    bodyPadding:10,

    items: [
        {
            xtype: 'displayfield',
            value: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; The list below shows the currently added users. Here you can search for users and send email by clicking the email button to the right in the list. To manage (add, edit etc), please navigate to the country or organization panel.</p>'
        },
        {
            xtype: 'grid',
            reference: 'existingusersgrid',
            maxHeight:400,
            scrollable:true,
            tools: [
                {
                    type: 'refresh',
                    tooltip: 'Refresh',
                    handler: 'reloadGrid'
                },
                {
                    type: 'print',
                    tooltip: 'Print list'
                }

            ],
            tbar: [
                {

                    xtype: 'textfield',
                    reference: 'nameFilterField',
                    id: 'userNameFilterField',
                    fieldLabel: 'Search',
                    flex: 1,
                    margin: 2,
                    enableKeyEvents: true,
                    listeners: {
                        keyup: 'onNameFilterKeyup',
                        buffer: 100
                    }

                },
                {
                    xtype: 'displayfield',
                    reference: 'userGridMsgField',
                    flex: 1
                }
            ],
            store: {
                type: 'userstore'
            },


            columns: [
                {
                    text: 'Username',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'username',
                    sorter: function (rec1, rec2) {
                        var rec1Name = rec1.get('username'),
                            rec2Name = rec2.get('username');

                        if (rec1Name > rec2Name) {
                            return 1;
                        }
                        if (rec1Name < rec2Name) {
                            return -1;
                        }
                        return 0;
                    }
                },

                {
                    text: 'Role',
                    flex: 1,
                    sortable: false,
                    dataIndex: 'type'
                },
                {
                    text: 'Created',
                    flex: 1,
                    sortable: false,
                    dataIndex: 'created'
                },
                {
                    text: 'Organization',
                    flex: 1,
                    sortable: false,
                    dataIndex: 'organization'
                },
                {
                    text: 'Country',
                    flex: 1,
                    sortable: true,
                    dataIndex: 'countryName'
                },
                {
                    text: 'Email',
                    width: 80,
                    xtype: 'widgetcolumn',
                    padding: 3,
                    widget: {
                        glyph: 0xf003,
                        width: 40,
                        xtype: 'button',
                        dataIndex: 'userId',
                        tooltip: 'Send email',
                        style: {
                            backgroundColor: 'LightBlue',
                            border: 0
                        },
                        handler: 'onEmail'

                    }
                },
                {
                        text: 'Delete',
                        width: 80,
                        xtype: 'widgetcolumn',
                        padding:3,
                        widget: {
                            glyph: 0xf014,
                            width: 40,
                            xtype: 'button',
                            dataIndex: 'userId',
                            tooltip: 'Delete user',
                            style: {
                                backgroundColor: 'LightCoral',
                                border: 0
                            },
                            handler: 'onDeleteUser'

                        }
                }

            ]
        }
    ]


});
