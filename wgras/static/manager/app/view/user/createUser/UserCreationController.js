/**
 * Created by anton on 5/19/16.
 */

Ext.define('VramApp.view.user.createUser.UserCreationController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.usercreationcontroller',

    init:function(){
    },



    onCreateUserSubmit: function(btn) {
        try {
            var me = this;
            var request = VramApp.utilities.HTTPRequests;
            var form = btn.up('form')
            var values = form.getValues();
            var userName = values.username;
            var role = values.role;
            var managementarea = values.managementarea;
            var email = values.email;
            var pw1 = values.pw1;
            var pw2 = values.pw2;
            if (pw1 === pw2) {
                var user = JSON.stringify({
                    username: userName,
                    role: role,
                    password: values.pw2,
                    managementareaId: managementarea,
                    email: email
                });


                var url = '/users';
                var success = function(response, opts) {
                    me.reloadGrid();
                    me.setUserCreationSuccessMessage('User \'' + userName + '\' created');
                };

                var failure = function(response, opts) {
                    var jsonResp = Ext.decode(response.responseText);
                    me.setUserCreationErrorMessage(jsonResp.errorMessage);
                };

                request.POST(url, user, success, failure);

            } else {
                me.setUserCreationErrorMessage('Passwords must match')
            }
        } catch (err) {
            console.log(err)
        }
    },

    loadManagementAreasForCountry: function(combo, record) {
        var countryId = record.get('countryId');
        var url = '/users/country/'.concat(countryId);
        var store = Ext.data.StoreManager.lookup('managementareabycountry');
        store.load({
            url: url
        })
    },

    setUserCreationErrorMessage: function(msg) {
        try{
            this.lookupReference('creationFormMsgField').setValue('<div style="color:red;">' + msg + '</div>');
            this.lookupReference('userGridMsgField').setValue('<div style="color:red;">' + msg + '</div>');
        }catch(err){
            console.log(err)
        }

    },

    setUserCreationSuccessMessage: function(msg) {
        try{
            this.lookupReference('creationFormMsgField').setValue('<div style="color:green;">' + msg + '</div>');
            this.lookupReference('userGridMsgField').setValue('<div style="color:green;">' + msg + '</div>');
        }catch(err){
            console.log(err)
        }

    },

    reloadGrid: function() {
        Ext.data.StoreManager.lookup('existingsusersstore').reload()
    }
})
