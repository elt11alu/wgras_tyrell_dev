Ext.define('VramApp.view.wgrasheader.WgrasHeader', {
    extend: 'Ext.panel.Panel',

    requires: [
        'VramApp.view.wgrasheader.WgrasHeaderController'
    ],

    xtype: 'wgrasheader',
    controller: 'wgrasheadercontroller',

dockedItems: [{
        xtype: 'toolbar',
        layout: { pack: 'center'},
        dock: 'top',
        items: [{
            text:'Logout',
            glyph: 0xf08b,
            handler: 'onLogout'
        },
        {
            text: 'Map',
            glyph: 0xf279,
            handler: 'onGoToMap'
        }]
    }]

});