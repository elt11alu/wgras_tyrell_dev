/**
 * Created by anton on 2015-11-04.
 */
Ext.define('VramApp.BaseLayers', {
    singleton:true,

    getBaseLayers:function(){
        var layers, osmLayer, bingMapLayer;

        bingMapLayer = new ol.layer.Tile({
            selectable: false,
            isBaseLayer: true,
            title: 'Satellite',
            source: new ol.source.BingMaps({
                key: 'Avq0f1YpFleQZB6Jcl-gaWKjxnE6y9ynoJxoQUP1I4rU49ikW1m_CkgKx8lgn9ZG',
                imagerySet: 'Aerial'
            })
        });
        osmLayer = new ol.layer.Tile({
            selectable: false,
            title: 'OSM',
            isBaseLayer: true,
            source: new ol.source.OSM()
        });

        layers.push(bingMapLayer);
        layers.push(osmLayer);

        return layers;
    }
});