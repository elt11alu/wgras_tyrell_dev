Ext.define('VramApp.GeometryDataTypes',{
    singleton:true,

    LayerTypes:{
        RASTER:'raster',
        VECTOR:'vector',
        WFS:'WFS',
        WMS:'WMS',
        BASELAYER:'baseLayer'
    },

    geomTypes:{
        point:'Point',
        line:'LineString',
        multiline:  'MultiLineString',
        linearring:'LinearRing',
        polygon:'Polygon',
        multipolygon: 'MultiPolygon',
        multipoint:'MultiPoint',
        geometrycollection: 'GeometryCollection',
        circle:'Circle'
    }

});