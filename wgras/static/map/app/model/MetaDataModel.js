Ext.define('VramApp.model.MetaDataModel', {

    extend: 'Ext.data.Model',

    fields: [{
        name: 'layerIdentifier',
        type: 'string'
    }, {
        name: 'layerInfo',
        type: 'string'
    },{
        name: 'layerTitle',
        type: 'string'
    },{
        name: 'category',
        type: 'string'
    }]
});
