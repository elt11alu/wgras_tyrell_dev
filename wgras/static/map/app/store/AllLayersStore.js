/**
 * Created by anton on 9/15/16.
 */

Ext.define('VramApp.store.AllLayersStore', {

    // This store contains metadata for all available layers in the system based on the users priviliges

    extend: 'Ext.data.Store',
    requires: [
        'VramApp.model.RasterLayerModel',
        'VramApp.readers.WMSReaderV130',
        'VramApp.WGRASUrl'
    ],
    alias: 'store.allLayersStore',
    model: 'VramApp.model.RasterLayerModel',
    storeId: 'allLayersStore',

    autoLoad: true,

    proxy: {
        type: 'ajax',

        //url: VramApp.WGRASUrl.BASEURL().concat('wms?request=getCapabilities'),
        url: '/metadata/all',
        reader: {
            type: 'json',
            rootProperty: 'layers',
        }

        //reader: 'wmsreader130'
    },

    /*listeners: {
        load: {
            fn: function(){ console.log('loading store alllayers...'); }
        }
    }*/


});
