/**
 * Created by antonlu66 on 11/16/2015.
 */
Ext.define('VramApp.store.AttributeStore', {
    extend: 'Ext.data.Store',

    alias:'store.attributeStore',

    fields: [
        {attr: 'NAME_0', type: 'string'}

    ]
});