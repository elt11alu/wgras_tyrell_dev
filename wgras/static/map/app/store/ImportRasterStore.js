Ext.define('VramApp.store.ImportRasterStore', {

    extend: 'Ext.data.Store',
    requires: [
        'VramApp.model.RasterLayerModel',
        'VramApp.readers.WMSReaderV130',
        'VramApp.WGRASUrl'
    ],
    alias: 'store.importrasterstore',
    model: 'VramApp.model.RasterLayerModel',
    storeId: 'rasterStore',

    autoLoad: false,

    proxy: {
        type: 'ajax',

        //url: VramApp.WGRASUrl.BASEURL().concat('wms?request=getCapabilities'),
        url: '/metadata/wms',
        reader: {
            type: 'json',
            rootProperty: 'layers',
        }

        //reader: 'wmsreader130'
    }
});
