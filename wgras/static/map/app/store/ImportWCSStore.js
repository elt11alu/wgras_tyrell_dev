/**
 * Created by ludvi on 2016-02-29.
 */

Ext.define('VramApp.store.ImportWCSStore', {

    extend: 'Ext.data.Store',
    alias:'store.importwcsstore',
    storeId:'wcsstore',
    requires:[
        'VramApp.readers.WCSReader100',
        'VramApp.model.WCSLayerModel'
    ],
    model: 'VramApp.model.WCSLayerModel',

    autoLoad: true,

    proxy: {
        type: 'ajax',

        headers: {
            'Accept': 'application/json'
        },

            url: VramApp.WGRASUrl.BASEURL().concat('wcs?request=getCapabilities&version=1.0.0'),

        reader: 'wcsreader100'
    }
});
