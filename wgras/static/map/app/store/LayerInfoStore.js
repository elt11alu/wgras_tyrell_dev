Ext.define('VramApp.store.LayerInfoStore', {

    extend: 'Ext.data.Store',
    alias: 'store.layerinfostore',


    autoLoad: false,
    fields: [{
        name: 'layerName'
    }, {
        name: 'layerTitle'
    }, {
        name: 'workspaceName'
    }, {
        name: 'layerInfo'
    }, {
        name: 'layerType'
    }, {
        name: 'crsForBoundingBox'
    }, {
        name: 'bboxMinX'
    }, {
        name: 'bboxMinY'
    }, {
        name: 'bboxMaxX'
    }, {
        name: 'bboxMaxY'
    }],
    
    proxy: {
        type: 'ajax',
        headers:{
        'Accept': 'application/json'
        },
        url: VramApp.WGRASUrl.LAYERINFOURL,

        reader: {
            type: 'json'
            
        }
    }
})