/**
 * Created by anton on 9/13/16.
 */

Ext.define('VramApp.store.LayerTreeStore', {
    extend: 'Ext.data.TreeStore',

    alias: 'store.layertreestore',
    storeId: 'layertreestore',
    root: {
        text: 'Layers',
        expanded: true,

        children: [
            {
                displayName: 'Base Maps',
                folderName: 'baseLayers',
                isBaseLayerFolder: true,
                children: []
            },
            {
                displayName: 'Additional maps',
                folderName: 'overlays',
                expanded: true,
                isOverLayFolder: true,
                children: []
            }
        ]
    }
});
