/**
 * Created by anton on 2016-01-16.
 */
Ext.define('VramApp.store.OwsConnectionStore', {
    extend: 'Ext.data.Store',

    alias: 'store.owsconnectionstore',

    requires: [
        'Ext.data.proxy.JsonP',
        'VramApp.model.RasterLayerModel',
        'VramApp.readers.WMSReaderV111',
        'VramApp.readers.WMSReaderV130'
    ],

    fields:[
        {name:'layerName'},
        {name:'layerTitle'},
        {name:'abstract'}
    ],
    storeId:'owsconnectionstore',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        actionMethods:{create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
        url: '/process/externalservice',
        paramsAsJson:true,
         headers: {'Content-Type': "application/json" },
        reader: {
            type: 'json',
            rootProperty:'layers'
        }
    }
});