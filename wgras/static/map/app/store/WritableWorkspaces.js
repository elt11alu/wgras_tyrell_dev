/**
 * Created by anton on 2016-08-20.
 */

Ext.define('VramApp.store.WritableWorkspaces', {

    extend: 'Ext.data.Store',
    alias: 'store.writableworkspaces',

    fields: [
        {
            name: 'name'
        }, {
            name: 'workspaceId'
        }
    ],


    storeId: 'writableworkspaces',


    autoLoad: true,

    proxy: {
        type: 'ajax',
        headers: {
            'Accept': 'application/json'
        },
        url: '/workspaces/writable',

        reader: {
            type: 'json',
            rootProperty: 'workspaces'
        }

    }

});

