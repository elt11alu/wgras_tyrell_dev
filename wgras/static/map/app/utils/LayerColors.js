/**
 * Created by anton on 2015-09-28.
 */

Ext.define('VramApp.utils.LayerColors', {

    singleton: true,

    colors: {
        black: '#000000',
        military: '#333300',
        darkGreen: '#003300',
        darkBlue: '#003366',
        middleBlue: '#000080',
        violet: '#333399',
        darkGrey: '#333333',
        darkRed: '#800000',
        orange: '#FF6600',
        kaki: '#808000',
        green: '#008000',
        cyan: '#008080',
        blue: '#0000FF',
        darkViolet: '#666699',
        grey: '#808080',
        red: '#FF0000',
        lightOrange: '#FF9900',
        neonGreen: '#99CC00',
        hipster: '#339966',
        turqoise: '#33CCCC',
        neonBlue: '#3366FF',
        hipsterViolet: '#800080',
        dull: '#969696',
        hollyWood: '#FF00FF',
        yellow: '#FFCC00',
        brightYellow: '#FFFF00',
        strikeGreen: '#00FF00',
        wow: '#00FFFF',
        ocean: '#00CCFF',
        creature: '#993366',
        antique: '#C0C0C0',
        baby: '#FF99CC',
        sunset: '#FFCC99',
        yellowish: '#FFFF99',
        lagoon: '#CCFFCC',
        brightLagoon: '#CCFFFF',
        deepBlue: '#99CCFF',
        ticTac: '#CC99FF',
        white: '#FFFFFF'
    },


    getAllColorsObject: function () {
        return this.colors
    },

    getAllColorsArray: function () {
        var colArray = [];
        var k;
        for (k in this.colors) {
            colArray.push(this.colors[k].split('#')[1]);
        }
        return colArray;
    },

    formatColortoArray: function (colorHex, alpha) {
        var color = colorHex;


        if(colorHex.indexOf('#')<0){
           var color =  '#'+colorHex;

        }

        var colArray = ol.color.asArray(color);

        if (alpha) {
            colArray[3] = alpha;
        }
        return colArray;
    },

    rgbArrayToString:function(rgbarray){
        return rgbarray.toString();
    },

    rgb2hex: function (rgb) {
        //rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
        return  "#" +
        ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[3],10).toString(16)).slice(-2);
    },

    hex2Rgb: function (hex) {
        var r = parseInt((cutHex(hex)).substring(0, 2), 16), g = ((cutHex(hex)).substring(2, 4), 16), b = parseInt((cutHex(hex)).substring(4, 6), 16)
        return r + '' + b + '' + b;
    },
    cutHex: function (hex) {
        return (hex.charAt(0) == "#") ? hex.substring(1, 7) : hex
    }

})
