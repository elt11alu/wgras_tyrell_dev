/**
 * Created by anton on 8/18/16.
 */

Ext.define('VramApp.utils.Loader', {
    singleton: true,

    getLoader: function (target_component, msg) {
        return new Ext.LoadMask({
            msg: msg,
            target: target_component
        });
    }
});
