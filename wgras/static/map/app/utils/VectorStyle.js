/**
 * Created by anton on 2015-09-28.
 */

Ext.define('VramApp.utils.VectorStyle', {
    requires: ['VramApp.utils.LayerColors'],
    singleton: true,

    defaultStyle: function () {
        var colorClass = VramApp.utils.LayerColors;
        var fillColorHex = colorClass.colors.white;
        var strokeColorHex = colorClass.colors.black;
        var pointFillHex = colorClass.colors.red;
        var fillColorrgba = colorClass.formatColortoArray(colorClass.colors.white, 0.5);
        var strokeColorrgba = colorClass.formatColortoArray(colorClass.colors.black, 1);
        var pointFillrgba = colorClass.formatColortoArray(colorClass.colors.red, 0.5);
        var style = new ol.style.Style({
            fill: new ol.style.Fill({
                color: fillColorrgba
            }),
            stroke: new ol.style.Stroke({
                width: 2,
                color: strokeColorrgba
            }),
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: pointFillrgba
                }),
                stroke: new ol.style.Stroke({
                    width: 1,
                    color: strokeColorrgba
                }),
                radius: 4
            })
        });

        var styleInfoObj = {
            fill: {
                color: fillColorHex,
                opacity: fillColorrgba[3]
            },
            stroke: {
                color: strokeColorHex,
                opacity: strokeColorrgba[3],
                width: 2
            },
            image: {
                fill: {
                    color: pointFillHex,
                    opacity: pointFillrgba[3]
                },
                stroke: {
                    color: strokeColorHex,
                    opacity: strokeColorrgba[3],
                    width: 1
                },
                radius: 4
            }
        };
        var styleObj = {
            style: style,
            styleInfo: styleInfoObj
        };


        return styleObj;
    },

    getNewOlStyle: function (newStyle) {

        var colorClass = VramApp.utils.LayerColors;
        var transformFactor = 10;

        /*Opacity- these must be embedded in rgba of colors*/
        var fillOpacity = newStyle.fill.opacity/transformFactor;
        var strokeOpacity = newStyle.stroke.opacity/transformFactor;
        var imageFillOpacity = newStyle.fill.opacity/transformFactor;
        var imageStrokeOpacity = newStyle.stroke.opacity/transformFactor;

        /*Colors*/
        var fillColor = colorClass.formatColortoArray(newStyle.fill.color, fillOpacity);
        var strokeColor = colorClass.formatColortoArray(newStyle.stroke.color, strokeOpacity);
        var imageFillColor = colorClass.formatColortoArray(newStyle.image.fill.color, imageFillOpacity);
        var imageStrokeColor = colorClass.formatColortoArray(newStyle.image.stroke.color, imageStrokeOpacity);


        /*Widths*/
        var strokeWidth = newStyle.stroke.width;
        var imageStrokeWidth = newStyle.image.stroke.width;

        /*Radius*/
        var imageRadius = newStyle.image.radius;

        var style = new ol.style.Style({
            fill: new ol.style.Fill({
                color: fillColor
            }),
            stroke: new ol.style.Stroke({
                width: strokeWidth,
                color: strokeColor
            }),
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: imageFillColor
                }),
                stroke: new ol.style.Stroke({
                    width: imageStrokeWidth,
                    color: imageStrokeColor
                }),
                radius: imageRadius
            })
        });

        var styleInfoObj = {
            fill: {
                color: newStyle.fill.color,
                opacity: fillOpacity
            },
            stroke: {
                color: newStyle.stroke.color,
                opacity: strokeOpacity,
                width: strokeWidth
            },
            image: {
                fill: {
                    color: newStyle.image.fill.color,
                    opacity: imageFillOpacity
                },
                stroke: {
                    color: newStyle.image.stroke.color,
                    opacity: imageStrokeOpacity,
                    width: imageStrokeWidth
                },
                radius: imageRadius
            }
        };
        var styleObj = {
            style: style,
            styleInfo: styleInfoObj
        };


        return styleObj;



    }


});
