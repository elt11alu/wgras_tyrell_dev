/**
 * Created by anton on 2015-12-23.
 */
Ext.define('VramApp.view.about.AboutController',{
    extend: 'Ext.app.ViewController',
    alias: 'controller.about',

    init:function(){
        var textarea = this.lookupReference('aboutText');
        this.getView().on('afterrender',function(){
            this.getViewModel().getStore('aboutStore').load(function(records,op,succ){
            textarea.setValue( records[0].get('about'));
        },this)},this);

        this.getView().on('close',function(){
            this.fireEvent('toggleHeadPanelButtons', false);
        },this);


    }




});
