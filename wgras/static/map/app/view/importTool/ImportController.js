Ext.define('VramApp.view.importTool.ImportController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.util.Filter'
    ],

    alias: 'controller.importcontroller',

    stores: [
        'VramApp.store.LayerStore',
        'VramApp.store.ImportWorkspaceStore',
        'VramApp.store.ImportRasterStore',
        'VramApp.store.ImportVectorStore'

    ],

    activeLayerType: VramApp.GeometryDataTypes.LayerTypes.RASTER,

    init: function (view) {

        this.loadWorkspaceStore();

        var me = this;
        view.on({
            beforedestroy: me.enableButtons,
            beforeclose: me.enableButtons,
            scope: me
        })

    },


    loadWorkspaceStore: function () {
        this.lookupReference('workspaceCombo').getStore().load();
    },

    enableButtons: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        return true;
    },

    startImport: function (button) {
        var me = this;
        var grid = button.up('window').down('grid');
        var selected = grid.getSelectionModel().getSelection();
        this.removeErrorMsg();
        if (selected.length > 0) {
            var window = Ext.create('VramApp.view.importTool.ImportWindow');
            var form = window.down('form');
            window.layerType = me.activeLayerType;
            var layerNames = [];
            for (var ii = 0; ii < selected.length; ii++) {
                var layerName = selected[ii].get('layerName');
                var title = selected[ii].get('layerTitle');
                var bbox = [selected[ii].get('bboxMinX'),selected[ii].get('bboxMinY'),selected[ii].get('bboxMaxX'),selected[ii].get('bboxMaxY')];
                var layerType = selected[ii].get('layerType');
            
                
                form.add({
                    fieldLabel: title,
                    name:'inputname',
                    allowBlank: false,
                    layerName: layerName,
                    layerTitle: title,
                    bbox: bbox,
                    layerType: layerType
                })
            }
            window.show();
        } else {
            this.setErrorMessage('Please, select data layer before importing.')
        }
    },

    setErrorMessage: function (errorMsg) {
        this.lookupReference('importErrorMsg').setValue('<div style="color:red">' + errorMsg + '</div>').show();
    },

    removeErrorMsg: function () {
        this.lookupReference('importErrorMsg').setValue('');
    },


    closeWindow: function (button) {

        button.up('window').close();
    },

    onWorkspaceChange: function (combo, record, eOpts) {
        var me = this;
        var store = combo.up('window').down('grid').getStore();
        var name = record.get('name');
        store.removeFilter('workspaceFilter');
        store.setFilters(me.getFilter(name));

    },

    getFilter: function (workspaceName) {
        return new Ext.util.Filter({
            id: 'workspaceFilter',
            filterFn: function (item) {
                var match = false;
                var workspace = item.get('workspaceName');
                //var workspace = item.get('Name').split(':')[0];
                if (workspace) {
                    match = workspace === workspaceName;
                }
                return match;
            }
        })
    },

    onDataTypeChange: function (checkBox, checked) {
        var me = this;
        var grid = this.lookupReference('refimportgrid');

        if (checked && checkBox.geometry === VramApp.GeometryDataTypes.LayerTypes.RASTER) {
            me.activeLayerType = checkBox.geometry;
            var rasterStore = Ext.create('VramApp.store.ImportRasterStore');

            grid.reconfigure(rasterStore, me.getColumns());
            grid.getStore().reload();
        } else if (checked && checkBox.geometry === VramApp.GeometryDataTypes.LayerTypes.VECTOR) {
            me.activeLayerType = checkBox.geometry;
            var vectorStore = Ext.create('VramApp.store.ImportVectorStore');

            grid.reconfigure(vectorStore, me.getColumns());
            grid.getStore().reload()
        }


    },

    getColumns: function () {

        return [{
            text: 'Title',
            flex: 1,
            dataIndex: 'layerTitle'
                }, {
            text: 'Name',
            flex: 1,
            dataIndex: 'layerName'
                }, {
            text: 'info',
            flex: 1,
            dataIndex: 'layerInfo',
            hidden: true
                }, {
            text: 'type',
            flex: 1,
            dataIndex: 'layerType',
            hidden: true
                }, {
            text: 'crs',
            flex: 1,
            dataIndex: 'crsForBoundingBox',
            hidden: true
                }, {
            text: 'bbminx',
            flex: 1,
            dataIndex: 'bboxMinX',
            hidden: true
                }, {
            text: 'bbminy',
            flex: 1,
            dataIndex: 'bboxMinY',
            hidden: true
                }, {
            text: 'bbmaxx',
            flex: 1,
            dataIndex: 'bboxMaxX',
            hidden: true
                }, {
            text: 'bbmaxy',
            flex: 1,
            dataIndex: 'bboxMaxY',
            hidden: true
                }];
    }


});