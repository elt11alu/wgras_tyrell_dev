Ext.define('VramApp.view.importTool.ImportGrid', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Ext.grid.plugin.RowExpander',
        'Ext.selection.CheckboxModel',
        'VramApp.store.ImportRasterStore',
        'VramApp.store.ImportVectorStore',
        'VramApp.view.importTool.ImportGridController'
    ],
    alias: 'widget.importgrid',
    title: 'Available layers',
    border: true,
    reference: 'refimportgrid',
    rowLines: true,
    xtype: 'wgrasimportgrid',
    maxHeight: 300,
    minHeight: 250,
    controller: 'importgridcontroller',
    store: {
        type: 'importrasterstore'
    },
    loadMask: true,
    scrollable: true,
    viewConfig: {
        loadingText: "Loading layers..."
    },

    plugins: [{
        ptype: 'rowexpander',
        rowBodyTpl: new Ext.XTemplate(
            '<p><b>Category:</b> {workspaceName}</p>',
            '<p><b>Full name:</b> {layerName}</p>',
            '<p><b>Summary:</b> {layerInfo}</p>',
            '<p><b>Layer type:</b> {layerType}</p>', {})
    }],

    selModel: {
        selType: 'checkboxmodel',
        checkOnly: false,
        mode: 'SINGLE'
    },

    tools: [{
        type: 'refresh',
        handler: 'onRefresh',
        tooltip: 'Reload Data'
    }],


    columns: [{
        text: 'Title',
        flex: 1,
        dataIndex: 'layerTitle'
    }, {
        text: 'Name',
        flex: 1,
        dataIndex: 'layerName',
        hidden: true
    }, {
        text: 'info',
        flex: 1,
        dataIndex: 'layerInfo',
        hidden: true
    }, {
        text: 'workspace',
        flex: 1,
        dataIndex: 'workspaceName',
        hidden: true
    }, {
        text: 'type',
        flex: 1,
        dataIndex: 'layerType',
        hidden: true
    }, {
        text: 'crs',
        flex: 1,
        dataIndex: 'crsForBoundingBox',
        hidden: true
    }, {
        text: 'bbminx',
        flex: 1,
        dataIndex: 'bboxMinX',
        hidden: true
    }, {
        text: 'bbminy',
        flex: 1,
        dataIndex: 'bboxMinY',
        hidden: true
    }, {
        text: 'bbmaxx',
        flex: 1,
        dataIndex: 'bboxMaxX',
        hidden: true
    }, {
        text: 'bbmaxy',
        flex: 1,
        dataIndex: 'bboxMaxY',
        hidden: true
    }]
});