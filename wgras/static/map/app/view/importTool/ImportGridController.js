Ext.define('VramApp.view.importTool.ImportGridController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.importgridcontroller',

    stores: [
        //'VramApp.store.LayerStore',
        'VramApp.store.ImportRasterStore',
        'VramApp.store.ImportVectorStore'
    ],

    init: function () {
      
        this.control({
            'importgrid': {
                'afterrender': this.onStart
            }
        }, this)

    },

    
    onRefresh: function(){
    this.view.store.reload();
    },

    onStart: function(){

        this.view.store.load({
            scope: this,
            callback: function(records, operation, success) {
                if(!success){
                    Ext.Msg.show({
                        title:'Error',
                        message: 'The application is not connected to the map server. Unable to request data.',
                        icon: Ext.Msg.WARNING

                    });
                }
            }
        });
    }



})