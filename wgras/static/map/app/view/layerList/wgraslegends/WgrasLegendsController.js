/**
 * Created by antonlu66 on 11/12/2015.
 */
Ext.define('VramApp.view.layerList.wgraslegends.WgrasLegendsController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wgraslegends',

    /**
     * Called when the view is created
     */



    listen: {
        controller: {
            '*': {
                /*togglelegend can be fired by any controller, to indicate a layer have been toggled and legend should be added removed,
                 * params:
                 * add/remove -boolean
                 * layerName, displayName - string*/
                toggleLegend: 'toggleLegends'
            }


        }
    },

    init: function () {


    },

    toggleLegends: function (checked, layerName,displayName,dataType, workspaceName, layerIdentifier, external) {

        if(dataType===VramApp.GeometryDataTypes.LayerTypes.WMS){
            if(checked){
                this.addLegendGraphic(layerName,displayName, workspaceName, layerIdentifier, external);
            }else{
                this.removeLegendGraphic(layerName,displayName, layerIdentifier);
            }
        }

    },

    addLegendGraphic:function(layerName,displayName, workspaceName, layerIdentifier, external){
        var URL;
        if(displayName === 'Fires48hrs' || displayName==='Fires24hrs' ){
            URL = 'resources/mapIcons/activefire.svg'
            this.getView().getStore().add({displayName:displayName,legendImgUrl:URL,layerName:layerName});
        }else{
            URL = VramApp.WGRASUrl.BASEURL().concat('wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=').concat(workspaceName, ':', layerName);
            if(typeof layerName == 'undefined' || typeof displayName == 'undefined' || typeof workspaceName == 'undefined' || typeof layerIdentifier == 'undefined' || external ==true){
                URL = false;
            }
            this.getView().getStore().add({displayName:displayName,legendImgUrl:URL,layerName:layerName, layerIdentifier:layerIdentifier});
        }

    },

    removeLegendGraphic:function(layerName,displayName, layerIdentifier){
        var index = this.getView().getStore().findExact('layerIdentifier',layerIdentifier);
        var model = this.getView().getStore().getAt(index);
        this.getView().getStore().remove(model);
    }

});