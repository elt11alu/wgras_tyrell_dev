Ext.define('VramApp.view.map.MapView', {
    extend: 'GeoExt.component.Map',
    requires: [
        'VramApp.utils.VectorStyle',
        'VramApp.view.map.MapViewController'
    ],
    xtype: 'mapView',
    controller: 'mapController',
    id: 'mapView',
    pointerRest: true,
    map: new ol.Map({
        layers: [
            /*new ol.layer.Tile({
                selectable: false,
                isBaseLayer: true,
                title: 'Satellite',
                source: new ol.source.BingMaps({
                    key: 'Avq0f1YpFleQZB6Jcl-gaWKjxnE6y9ynoJxoQUP1I4rU49ikW1m_CkgKx8lgn9ZG',
                    imagerySet: 'Aerial'
                })
            }),
            new ol.layer.Tile({
                selectable: false,
                title: 'OSM',
                isBaseLayer: true,
                source: new ol.source.OSM()
            }),
            new ol.layer.Vector({
                selectable: false,
                isBaseLayer: false,
                title: 'Asia borders',
                source: new ol.source.Vector({
                    url: 'resources/layers/asia.geojson',
                    format: new ol.format.GeoJSON()
                }),
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 100, 50, 0.0)'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 2,
                        color: 'rgba(0, 0, 0, 0.8)'
                    })
                })
            }),
            new ol.layer.Tile({
                title: 'Active fires',
                geoServerName: 'fires',
                isBaseLayer: false,
                source: new ol.source.TileWMS({
                    url: 'https://firms.modaps.eosdis.nasa.gov/wms/',
                    projection: 'EPSG:3857',
                    params: {
                        'LAYERS': 'fires48',
                        'VERSION': '1.1.1',
                        'SRS': 'EPSG:3857'
                    }
                    //serverType: 'geoserver'
                })
            }),
            new ol.layer.Tile({
                title: 'Elevation',
                geoServerName: 'fires',
                isBaseLayer: false,
                source: new ol.source.TileWMS({
                    url: 'http://localhost:8080/geoserver/Armenia/wms',
                    projection: 'EPSG:3857',
                    params: {
                        'LAYERS': 'Armenia:elevationArmeniaNorth',
                        'VERSION': '1.1.1',
                        'SRS': 'EPSG:3857'
                    },
                    serverType: 'geoserver'
                })
            }),
            new ol.layer.Vector({
                selectable: false,
                isBaseLayer: false,
                title: 'Armenia roads',
                source: new ol.source.Vector({
                    url: 'resources/layers/arm_roads.json',
                    format: new ol.format.GeoJSON()
                }),
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 100, 50, 0.0)'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 2,
                        color: 'rgba(0, 0, 0, 0.8)'
                    })
                })
            }),
            new ol.layer.Vector({
                selectable: false,
                isBaseLayer: false,
                title: 'Armenia lakes',
                source: new ol.source.Vector({
                    url: 'resources/layers/arm_lakes.json',
                    format: new ol.format.GeoJSON()
                }),
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 100, 50, 0.0)'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 2,
                        color: 'rgba(0, 0, 0, 0.8)'
                    })
                })
            }),
            new ol.layer.Vector({
                selectable: false,
                isBaseLayer: false,
                title: 'Armenia rivers',
                source: new ol.source.Vector({
                    url: 'resources/layers/arm_rivers.json',
                    format: new ol.format.GeoJSON()
                }),
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 100, 50, 0.0)'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 2,
                        color: 'rgba(0, 0, 0, 0.8)'
                    })
                })
            }),
            //https://firms.modaps.eosdis.nasa.gov/wms/?SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&LAYERS=fires48&width=512&height=512&BBOX=-20037508.3427892480,-20037508.3427892480,20037508.3427892480,20037508.3427892480&SRS=EPSG:3857
            /*new ol.layer.Vector({
             selectable:false,
             title: 'Europe borders',
             source: new ol.source.Vector({
             url: 'resources/layers/europe.json',
             format: new ol.format.GeoJSON()
             }),
             style: new ol.style.Style({
             fill: new ol.style.Fill({
             color: 'rgba(255, 100, 50, 0.0)'
             }),
             stroke: new ol.style.Stroke({
             width: 2,
             color: 'rgba(0, 0, 0, 0.8)'
             })
             })
             }),
             new ol.layer.Heatmap({
             selectable:false,
             title: 'TBE cases',
             source: new ol.source.Vector({
             url: 'resources/layers/points3.geojson',
             format: new ol.format.GeoJSON()

             }),
             opacity: 0.9
             }),
             new ol.layer.Image({
             selectable:false,
             title:'Elevation',
             source: new ol.source.ImageStatic({
             url: 'resources/layers/mountains.png',
             imageExtent: new ol.proj.transformExtent([44.522486, 40.676252, 44.427727, 40.604386], 'EPSG:4326', "EPSG:3857")
             }),
             opacity:0.7
             }),
            new ol.layer.Vector({
                selectable: true,
                title: 'Villages',
                source: new ol.source.Vector({
                    url: 'resources/layers/Eq_av_an_loss_simplified.geojson',
                    format: new ol.format.GeoJSON()
                }),
                style: function (feature, resolution) {
                    var aal = parseInt(feature.get('eq_aal_tot'));
                    var fillC = 'rgba(255,255,255,1)';
                    if (aal < 1250) {
                        fillC = 'rgba(255,255,255,1)';
                    }
                    if (aal > 1250) {
                        fillC = 'rgba(255, 255, 153,1)';
                    }
                    if (aal > 2500) {
                        fillC = 'rgba(153, 255, 51,1)';
                    }
                    if (aal > 5000) {
                        fillC = 'rgba(255, 102, 0,1)';
                    }
                    if (aal > 10000) {
                        fillC = 'rgba(255, 51, 0,1)';
                    }

                    return [new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: fillC
                        }),
                        stroke: new ol.style.Stroke({
                            width: 1,
                            color: 'rgba(0, 0, 0, 0.8)'
                        })
                    })];
                }
            })*/


        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([0, 0]),
            projection: 'EPSG:3857',
            minZoom:3,
            zoom: 3
        })
    })


});