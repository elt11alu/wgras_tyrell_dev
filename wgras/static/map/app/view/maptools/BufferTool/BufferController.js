Ext.define('VramApp.view.maptools.BufferTool.BufferController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.buffercontroller',
    requires: [
        'VramApp.InputValidationModel',
        'VramApp.WGRASUrl',
        'VramApp.utils.VectorStyle',
        'VramApp.MapUtils'
    ],

    currentRecord: {},
    previousRecord: {},

    init: function (view) {
        this.bufferLayers = [];
        var me = this;
        view.on({
            beforedestroy: me.enableButtons,
            beforeclose: me.enableButtons,
            scope: me
        });
        this.loadAvailableVectorLayers()
        this.attachGridListener()

    },

    loadAvailableVectorLayers: function () {
        Ext.data.StoreManager.lookup('vectorstore').load()
    },

    attachGridListener: function () {
        var me = this;
        this.lookupReference('bufferlayergrid').on('select', function (grid, record) {
            me.previousRecord = me.currentRecord;
            me.currentRecord = record;
            me.toggleLayerVisibility(me.previousRecord, false);
            me.toggleLayerVisibility(record, true);
            me.panToLayer(record);
            me.setLayerAsSelectable(record, true)
            me.setLayerAsSelectable(me.previousRecord, false)
        })
    },

    enableButtons: function () {

        this.fireEvent('toggleHeadPanelButtons', false);
    },


    /** BUFFER LAYER **/

    onBufferLayer: function () {
        var distance, me, loader;
        loader = VramApp.utils.Loader.getLoader(this.getView(), 'Creating buffer...');
        try {

            distance = this.getViewModel().get('bufferDistanceLayer');
            me = this;

            if (distance > 0) {
                var layerId = this.currentRecord.get('layerId');

                $.ajax({
                    url: '/process/buffer/layer',
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        layerId: layerId,
                        distance: distance
                    }),
                    context: me,
                    dataType: 'json',
                    beforeSend: function () {
                        loader.show();
                    },
                    success: function (data) {
                        loader.hide();
                        this.onBufferLayerSuccess(data, this.getViewModel().get('layerBufferKeepAttributes'))
                    },
                    error: function (data) {
                        loader.hide();
                        this.onFail(data)
                    }
                })
            }
        } catch (err) {
            loader.hide()
        }

    },

    onBufferLayerSuccess: function (data, keepAttributes) {
        var format, bufferedFeatures, features;
        features = data.features;
        format = new ol.format.GeoJSON();
        bufferedFeatures = format.readFeatures(features, {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});


        this.promptSave(bufferedFeatures)
    },
    onFail: function (data) {
        console.warn(data)
    },
    promptSave: function (features) {
        var nbrOfFeatures = features.length;
        Ext.MessageBox.show({
            title: 'Save layer?',
            msg: 'Buffer returned '.concat(nbrOfFeatures, ' features.', ' Do you wish to save to a new layer? If no, the layer will still be added to the map as a temporary layer.'),
            buttons: Ext.MessageBox.YESNO,
            scope: this,
            fn: function (btn) {
                this.afterPrompt(btn, features)
            },
            icon: Ext.MessageBox.QUESTION
        });
    },

    afterPrompt: function (btn, features) {
        if (btn === 'yes') {
            this.showCreateLayerWindow(features)
        } else {
            this.addAsTempLayer(features)
        }
    },

    addAsTempLayer: function (features) {
        var layer = new ol.layer.Vector({
            source: new ol.source.Vector({features: features})

        });
        var required = {
            layer: layer,
            layerTitle: 'temp_buffer_lyr',
            layerName: 'temp_buffer_lyr'
        };
        var options = {
            visible: true,
            folder: 'Temp buffered'
        };
        VramApp.MapUtils.addWFSOverlay(required, options);
        this.onClose();
    },

    showCreateLayerWindow: function (features) {
        var ancestorLayerId = this.currentRecord.get('layerId');
        Ext.create({
            xtype: 'createnewlayerwithgeometrywindow',
            features: features,
            layerId: ancestorLayerId
        }).show();
        this.onClose();
    },


    panToLayer: function (record) {
        VramApp.MapUtils.panToLayerById(record.get('layerId'));
    },

    toggleLayerVisibility: function (record, visible) {
        if (!$.isEmptyObject(record)) {
            var layer = VramApp.MapUtils.getLayerByDBId(record.get('layerId'));
            if (layer) {
                layer.setVisible(visible)
            }
        }

    },

    setLayerAsSelectable: function(record, selectable){
        if (!$.isEmptyObject(record)) {
            var layerId = record.get('layerId');
            var layer = VramApp.MapUtils.getLayerByDBId(layerId);
            if (layer) {
                layer.set('selectable', selectable)
                this.fireEvent('layerSelectableChange', layerId, selectable)
            }
        }
        
    },

    /** BUFFER ONE FEATURE **/

    onBufferFeature: function () {
        var distance, me, loader, selected;
        selected = VramApp.MapUtils.getSelectInteraction().getFeatures().getArray();
        me = this;
        if (selected.length > 0) {

            loader = VramApp.utils.Loader.getLoader(this.getView(), 'Creating buffer...');
            try {

                distance = this.getViewModel().get('bufferDistanceFeature');
                me = this;

                if (distance > 0) {

                    var layerId = this.currentRecord.get('layerId');
                    var featureId = selected[0].get('ogc_fid');
                    if (typeof layerId != 'undefined' && typeof featureId != 'undefined') {

                        /* The layer and feature should exist in database */
                        $.ajax({
                            url: '/process/buffer/feature',
                            type: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify({
                                layerId: layerId,
                                featureId: featureId,
                                distance: distance
                            }),
                            context: me,
                            dataType: 'json',
                            beforeSend: function () {
                                loader.show();
                            },
                            success: function (data) {
                                loader.hide();
                                this.onBufferFeatureSuccess(data.feature)
                            },
                            error: function (data) {
                                loader.hide();
                                this.onFail(data)
                            }
                        })
                    }else{
                        /* We have a temporary layer */
                        
                    }


                }
            } catch (err) {
                console.log(err);
                loader.hide()
            }

        }

    },

    onBufferFeatureSuccess: function(feature){
        var format = new ol.format.GeoJSON();
        var bufferedFeatures = format.readFeatures(feature,{dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
        this.promptSave(bufferedFeatures)

    },



    onClose: function(){
        this.getView().close()
        this.setLayerAsSelectable(this.currentRecord, false)
        this.setLayerAsSelectable(this.previousRecord, false)
    }

});