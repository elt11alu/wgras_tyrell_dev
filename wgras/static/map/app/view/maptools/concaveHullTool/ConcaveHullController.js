/**
 * Created by anton on 12/13/16.
 */

Ext.define('VramApp.view.maptools.concaveHullTool.ConcaveHullController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.concavehullcontroller',

    tempLayer: null,

    init: function () {
        this.loadStore();

    },

    loadStore: function () {
        var store = this.getViewModel().getStore('pointLayers');
        var found = store.loadStore();
        if (found == 0) {
            Ext.MessageBox.alert('Point layers', 'No point layers found. Note that you have to check the layers in the layer list to the left to use this tool.')
        }
    },

    onPointLayerSelection: function (combo, record) {
        var layerId = this.getViewModel().get('currentRecord').get('layerId');
        var layer = VramApp.MapUtils.getLayerByDBId(layerId);
        var extent = layer.getSource().getExtent();
        VramApp.MapUtils.panToExtent(extent);
    },

    onCalculate: function (slider, event) {
        try {
            var calcType = this.getViewModel().get('calcType');
            var selectedRecord = this.getViewModel().get('currentRecord');
            var maxEdge = slider.getValue();
            var me = this;
            this.getTempLayer().getSource().clear();

            if (selectedRecord != null) {
                var layerId = selectedRecord.get('layerId');
                if (layerId != 'undefined') {
                    var layer = VramApp.MapUtils.getLayerByDBId(layerId);
                    if (layer) {
                        var features = layer.getSource().getFeatures();
                        if (features.length > 0) {
                            var format = new ol.format.GeoJSON();
                            var f_coll = format.writeFeaturesObject(features, {
                                dataProjection: 'EPSG:4326',
                                featureProjection: 'EPSG:3857'
                            });
                            //console.log(f_coll);

                            switch (calcType) {
                                case 'convex':
                                    me.calculateConvexHull(f_coll);
                                    break;
                                case 'concave':
                                    me.calculateConcaveHull(f_coll, maxEdge);
                                    break;
                                default:

                            }
                        } else {
                            throw 'No features in layer'
                        }

                    } else {
                        throw 'Layer not found'
                    }

                } else {
                    throw 'Layer id is undefined'
                }

            } else {
                throw 'Selected record is null'
            }

        } catch (err) {
            console.warn(err)
        }
    },

    calculateConvexHull: function (jsonGeometries) {
        var convexHull;
    },

    calculateConcaveHull: function (jsonGeometries, edge) {
        /*var loader = VramApp.utils.Loader.getLoader(this.getView(),'Calculating').show();
         var maxEdge = edge;
         var concaveHull = turf.concave(jsonGeometries, maxEdge, 'miles');
         var format = new ol.format.GeoJSON();
         var features = format.readFeatures(concaveHull, {dataProjection:'EPSG:4326', featureProjection:'EPSG:3857'});
         this.getTempLayer().getSource().clear();
         this.getTempLayer().getSource().addFeatures(features)
         loader.hide();*/
        var loader = VramApp.utils.Loader.getLoader(this.getView(),'Calculating').show();
        var me = this;
        var success = function (resp) {
            loader.hide();
            var obj = Ext.decode(resp.responseText);
            var geom = obj.geoms[0];
            console.log(geom[0])
            var format = new ol.format.WKT();
            var ol_feat = format.readFeatures(geom[0], {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
            console.log(ol_feat);
            console.log(ol_feat[0].getGeometry().getType())
            me.getTempLayer().getSource().clear();
            me.getTempLayer().getSource().addFeatures(ol_feat)


        }
        var fail = function (resp) {
            loader.hide();
            console.log(resp)

        }
        var data = {
            target_percent: edge/10
        }
        console.log(data.target_percent)


        VramApp.HTTPRequests.POST('/process/concavehull', success, fail,JSON.stringify(data))
    },

    getTempLayer: function () {
        if (this.tempLayer == null) {
            this.tempLayer = new ol.layer.Vector({
                source: new ol.source.Vector()
            });
            VramApp.MapUtils.getOlMap().addLayer(this.tempLayer)
        }
        return this.tempLayer


    },

    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close();
    }
});
