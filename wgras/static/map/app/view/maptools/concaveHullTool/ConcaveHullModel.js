/**
 * Created by anton on 12/13/16.
 */

Ext.define('VramApp.view.maptools.concaveHullTool.ConcaveHullModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.concavehullmodel',

    stores: {


        pointLayers: {

            fields: [{name: 'layerTitle', type: 'string'}, {name: 'layerId', type: 'string'}],


            autoLoad: false,

            loadStore: function () {
                this.removeAll();
                var me = this;
                var map = VramApp.MapUtils.getOlMap();
                var layers = map.getLayers().getArray();
                var found = 0;
                $.each(layers, function (ind, lyr) {
                    if (lyr.getSource() instanceof ol.source.Vector) {
                        var features = lyr.getSource().getFeatures();
                        console.log('features: ', features.length)
                        if (features.length > 0) {
                            var geom = features[0].getGeometry();
                            console.log(geom.getType())
                            if (geom.getType() === 'Point' || geom.getType() === 'MultiPoint') {
                                me.add({layerTitle: lyr.get('title'), layerId: lyr.get('layerId')})
                                found ++;
                            }
                        }

                    }

                });
                return found;
            }
        }
    },

    data: {
        instructions: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Select input points. ' +
        '<i>Note: Only visible layers are listed in the menu below. If a certain layer is not listed, please make it visible' +
        'in the layer list and reload the menu by using the reload button above.</i></p>',

        currentRecord: null,
        calcType: 'concave',
        maxEdge: 5000
    }
});
