/**
 * Created by anton on 12/13/16.
 */

/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.maptools.concaveHullTool.ConcaveHullWindow', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.data.Store',
        'Ext.form.RadioGroup',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'Ext.layout.container.VBox',
        'VramApp.view.maptools.concaveHullTool.ConcaveHullController',
        'VramApp.view.maptools.concaveHullTool.ConcaveHullModel'
    ],

    autoShow: true,
    closable: false,
    title: 'Concave and Convex Hull of points',

    xtype: 'concavehulltool',

    layout: {
        type: 'vbox',
        pack: 'start',
        align: 'stretch'
    },
    bodyPadding: 5,
    //icon: VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/pointInPolygon.svg',

    viewModel: {
        type: 'concavehullmodel'
    },

    controller: 'concavehullcontroller',

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        flex: 1,
        items: [
            {
                xtype: 'displayfield',
                bind: {
                    value: '{instructions}'
                }
            }

        ]
    }],

    items: [

        {
            xtype: 'combo',
            reference: 'inputlayer',
            fieldLabel: 'Create polygon from',
            displayField: 'layerTitle',
            queryMode: 'local',
            bind: {
                store: '{pointLayers}',
                selection: '{currentRecord}'
            },
            listeners: {
                'select': 'onPointLayerSelection'
            }

        },
        {
            xtype: 'radiogroup',
            fieldLabel: 'Type',
            name: 'calcType',
            items: [
                {boxLabel: 'Convex hull', inputValue: 'convex'},
                {boxLabel: 'Concave hull', inputValue: 'concave', checked: true}
            ],
            bind: {
                value: '{calcType}'
            }
        },
        {
            xtype: 'slider',
            width: 200,
            value: 5,
            increment: 1,
            minValue: 0,
            maxValue: 10,
            fieldLabel: 'Max edge',
            bind:{
                value: '{maxEdge}'
            },
            listeners:{
              dragend:'onCalculate'
            }
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Area',
            value: 0,
            reference: 'areaField'
        },
        {
            xtype: 'displayfield',
            fieldLabel: 'Enclosed points',
            value: 0,
            reference: 'nbrOfPoints'
        },
        {
            xtype: 'displayfield',
            reference: 'errorMessageField',
            value: ''
        },
        {
            xtype: 'button',
            text: 'Create polygon',
            scale: 'medium',
            handler: 'onCalculate'
        }
    ],
    buttons: [
        {
            text: 'close',
            handler: 'onClose',
            dock: 'bottom'
        }
    ]
});
