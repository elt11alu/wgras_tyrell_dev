/**
 * Created by anton on 6/13/16.
 */

Ext.define('VramApp.view.maptools.directions.Directions', {
    extend: 'Ext.window.Window',

    requires: [
        'VramApp.view.maptools.directions.DirectionsController'
    ],

    controller: 'directionscontroller',
    xtype: 'directionstool',
    autoShow: true,
    widht: 150,
    closable: false,
    layout: 'anchor',

    title: 'Directions',
    bodyPadding: 10,

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [
            {
                xtype: 'button',
                text: 'close',
                handler: 'onClose'

            }
        ]
    }],


    items: [
        {
            xtype: 'fieldset',
            title: 'Directions',
            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    xtype: 'displayfield',
                    value: 'Click on map to choose from and to point'
                },
                {
                    xtype: 'combo',
                    store: ['foot', 'bike', 'car'],
                    value: 'car',
                    reference: 'vehicle',
                    fieldLabel: 'Travel mode'
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Distance format',
                    columns: 2,
                    items: [
                        {boxLabel: 'Km', name: 'distance-format', inputValue: 'kilometer', checked: true},
                        {boxLabel: 'M', name: 'distance-format', inputValue: 'meter'}
                    ],
                    listeners: {
                        change: 'onDistanceFormatChange'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: 'Distance',
                    reference: 'distance',
                    value: ''
                },
                
                {
                    xtype: 'displayfield',
                    reference: 'mainMsgField'
                }
            ]
        }


    ]
});
