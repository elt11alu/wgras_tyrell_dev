Ext.define('VramApp.view.maptools.drawTool.DrawToolController', {
    extend: 'Ext.app.ViewController',
    requires: [
        'VramApp.InputValidationModel',
        'VramApp.utils.VectorStyle',
        'VramApp.LayerUpdate'
    ],
    alias: 'controller.drawtoolcontroller',

    init: function (view) {
        this.control({
            'window': {
                afterrender: 'addInteraction',
                close: 'removeInteraction',
                beforedestroy: 'removeInteraction'
            }
        });
    },


    interaction: {},
    drawLayer: {},

    map: function () {
        return VramApp.MapUtils.getOlMap();
    },

    onGeomTypeToggle: function (container, button, pressed) {
        if (pressed) {
            if (button.isPoint) {
                this.drawGeometry('Point');
            }
            if (button.isLine) {
                this.drawGeometry('LineString');
            }
            if (button.isPolygon) {
                this.drawGeometry('Polygon');
            }
        }
    },

    closeandsave: function (button) {
        var layerId = button.up('form').down('combo').getValue();
            var layer = this.getDrawLayer();
            this.cloneLayer(layer, layerId);

    },


    drawGeometry: function (geomType) {
        var me = this;
        var drawLayer = me.getDrawLayer();
        var drawLayersStore = Ext.data.StoreManager.lookup('privatelayerstore');
        drawLayersStore.clearFilter();

        me.map().removeInteraction(me.interaction);


        switch (geomType) {

            case "Point":
                me.interaction = new ol.interaction.Draw({
                    type: 'Point',
                    source: drawLayer.getSource()
                });
                me.interaction.on('drawstart', function(){
                    me.lookupReference('lineBtn').setDisabled(true);
                    me.lookupReference('polygonBtn').setDisabled(true);
                });
                drawLayersStore.filterBy(function(record){
                   return record.get('type') === 'Point'
                });
                break;
            case "LineString":

                me.interaction = new ol.interaction.Draw({
                    type: 'LineString',
                    source: drawLayer.getSource()
                });
                me.interaction.on('drawstart', function(){
                    me.lookupReference('pointBtn').setDisabled(true);
                    me.lookupReference('polygonBtn').setDisabled(true);
                });
                drawLayersStore.filterBy(function(record){
                   return record.get('type') === 'LineString'
                });
                break;
            case "Polygon":

                me.interaction = new ol.interaction.Draw({
                    type: 'Polygon',
                    source: drawLayer.getSource()
                });
                me.interaction.on('drawstart', function(){
                    me.lookupReference('lineBtn').setDisabled(true);
                    me.lookupReference('pointBtn').setDisabled(true);
                });
                drawLayersStore.filterBy(function(record){
                   return record.get('type') === 'Polygon'
                });
                break;
            default:
                break;
        }


        me.map().addInteraction(me.interaction);


    },

    //called on open/close draw window, interactions are also added and removed inside drawGeometry method.
    removeInteraction: function () {
        var me;
        me = this;
        me.map().removeInteraction(me.interaction);
        me.map().removeLayer(me.getDrawLayer());
        me.toggleSelectInteraction(true);
        me.toggleDrawButton(false);
        this.fireEvent('toggleHeadPanelButtons', false);
    },


    //called on open/close draw window, interactions are also added and removed inside drawGeometry method.
    addInteraction: function () {
        var me = this;
        me.toggleSelectInteraction(false);
        me.addDrawLayer();
        me.drawGeometry("Point");
        me.toggleDrawButton(true);
    },

    //called on open/close draw window
    toggleDrawButton: function (condition) {
        var btn = Ext.getCmp('tools-drawButton');
        btn.setDisabled(condition);
    },

    //called on open/close of draw window
    toggleSelectInteraction: function (condition) {
        var me = this;
        var select = {};
        me.map().getInteractions().forEach(function (interaction) {
            if (interaction instanceof ol.interaction.Select) {
                select = interaction;
            }
        });
        select.setActive(condition)
    },

    addDrawLayer: function () {
        var me = this;
        var layer = me.getDrawLayer();

        if (!layer) {
            layer = new ol.layer.Vector({
                source: new ol.source.Vector({})
            });
            me.layerIndentifier = VramApp.MapUtils.addTempLayer(layer);
        }

    },

    getDrawLayer: function () {
        return VramApp.MapUtils.getTempLayer(this.layerIndentifier);
    },

    cloneLayer: function (layer,layerId) {
        var features, newLayer, loader, success, fail, me;
        me=this;
        features = layer.getSource().getFeatures();
        loader = VramApp.utils.Loader.getLoader(this.getView(), 'Saving...')
        newLayer = new ol.layer.Vector({
            title: 'whatever',
            source: new ol.source.Vector({})
        });
        
        for (var ii = 0; ii < features.length; ii++) {
            var feature = features[ii].clone();
            newLayer.getSource().addFeature(feature);
        }
        loader.show();
        success = function(resp){
            var reloadLayer = VramApp.MapUtils.getLayerByDBId(layerId);
            if(reloadLayer){
                reloadLayer.getSource().clear();
                me.toggleSelectInteraction(true);
                loader.hide();
                me.getView().destroy();
            }
        };
        fail = function(jqXHR){
            loader.hide();
            Ext.Msg.alert('Status', 'Your layer could not be saved, try again later.');
        };
        VramApp.LayerUpdate.postFeaturesToExistingLayer(newLayer,layerId, success, fail);

    }


});