/**
 * Created by antonlu66 on 11/18/2015.
 */
Ext.define('VramApp.view.maptools.elevationtool.ElevationToolController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.elevationtool',

    requires: [
        'VramApp.LayerLoader'
    ],

    /**
     * Called when the view is created
     */
    init: function (view) {
        this.loadElevationData();
        this.addListenerToGrid();
        this.addListenerToSlider();
        this.addListenerToNumberField();

    },
    aa: [],

    addListenerToNumberField: function () {
        var listener = function (field, event) {
            alert()
            this.lookupReference('currentElevationValue').setValue(field.getValue());
        };

        this.lookupReference('maxElevationInputField').on('keyup', listener, this);
    },

    addListenerToSlider: function () {
        var sliderListener = function (slider, event) {
            this.lookupReference('currentElevationValue').setValue(slider.getValue());
        };

        this.lookupReference('elevationValueSlider').on('drag', sliderListener, this);
    },


    addListenerToGrid: function () {
        var grid;
        grid = this.lookupReference('elevationLayerGrid');
        grid.getSelectionModel().on('select', this.onElevationDataSelection, this);
    },

    loadElevationData: function () {
        var grid, store;
        grid = this.lookupReference('elevationLayerGrid');
        store = grid.getStore();
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                grid.getStore().getData().eachKey(function (key, record, index, length) {

                    var successCallback = function (response) {
                        var obj = Ext.decode(response.responseText);
                        var statistics = obj.layer;

                        record.set('minValue', parseInt(statistics.minValue));
                        record.set('maxValue', parseInt(statistics.maxValue));
                        record.set('meanValue', parseInt(statistics.meanValue));
                        record.set('stdvValue', parseInt(statistics.stdvValue))

                    };

                    var failCallback = function (response) {
                        console.warn("Server side failure" + response.status);
                    };
                    this.getElevationDataInfo(record.get('layerName'), record.get('workspaceName'), successCallback, failCallback)
                }, this)
            }
        });

    },

    onElevationDataSelection: function (selmodel, record, index, eOpts) {

        var layerName = record.get('layerName');
        var workspaceName = record.get('workspaceName');
        var minValue = parseInt(record.get('minValue'), 10);
        var maxValue = parseInt(record.get('maxValue'), 10);
        var bbox = VramApp.MapUtils.reprojectExtentTo3857([
            parseFloat(record.get('bboxMinX')),
            parseFloat(record.get('bboxMinY')),
            parseFloat(record.get('bboxMaxX')),
            parseFloat(record.get('bboxMaxY'))], 'EPSG:4326');
        this.lookupReference('elevationValueSlider').setMaxValue(maxValue);
        this.lookupReference('elevationValueSlider').setMinValue(minValue);
        this.clearMap();
        this.addElevationDataToMap(workspaceName, layerName, minValue, maxValue, bbox);
        this.activateGetFeatureInfoClick();

    },

    addElevationDataToMap: function (workspaceName, layerName, minValue, maxValue, bbox) {

        var layer = VramApp.LayerLoader.getGeoServerTiledWMS(workspaceName, layerName);
        //VramApp.MapUtils.getLayers().insertAt(5,layer);
        this.layer = layer;

        var elevation = layer.getSource();
        var raster = new ol.source.Raster({
            sources: [elevation],
            operationType: 'pixel',
            operation: function (pixels, data) {
                var pixel = pixels[0];
                var p = pixel[0];


                if (pixel[0] < data.thresholdMax && pixel[0] > data.thresholdMin) {
                    pixel[2] = 255;
                    //pixel[3] = 190;

                } else {
                    pixel[3] = 0;
                }
                return pixel;
            },
            lib: {}

        });


        var rasterImage = new ol.layer.Image({
            opacity: 0.7,
            source: raster
        });
        this.rasterImage = rasterImage;

        this.lookupReference('currentElevationValue').on('change', function (field) {
            raster.changed();
        }, this);

        raster.on('beforeoperations', function (event) {
            // the event.data object will be passed to operations
            var data = event.data;
            data.thresholdMax = Math.max(0, Math.min((255, 255 * (this.lookupReference('currentElevationValue').getValue() - minValue) / (maxValue - minValue))))//(this.lookupReference('currentElevationValue').getValue()*(255/maxValue));
            data.thresholdMin = 0;


        }, this);

        //VramApp.MapUtils.getLayers().insertAt(11,rasterImage);
        VramApp.MapUtils.getOlMap().addLayer(rasterImage);
        VramApp.MapUtils.panToExtent(bbox);

    },

    onExtractPolygons: function () {

            var record = this.lookupReference("elevationLayerGrid").getSelectionModel().getSelection()[0];
            var layerName = record.get('layerName');
            var maxValue = this.lookupReference('currentElevationValue').getValue();
            var minValue = 0;
            var bboxMinX = record.get('bboxMinX');
            var bboxMinY = record.get('bboxMinY');
            var bboxMaxX = record.get('bboxMaxX');
            var bboxMaxY = record.get('bboxMaxY');
            var data = JSON.stringify({
                layerName:layerName,
                maxValue: 150,
                minValue: minValue,
                bboxMinX:bboxMinX,
                bboxMinY:bboxMinY,
                bboxMaxX:bboxMaxX,
                bboxMaxY:bboxMaxY,
                xCoord:this.coordinates[0],
                yCoord:this.coordinates[1]
            });
            var view = this.getView();
            var loadMask = new Ext.LoadMask({
                msg: 'Extracting polygons...',
                target: view
            }).show();
        try {
            var successCallback = function (response) {
                try {
                    //var obj = Ext.decode(response.responseText);
                    var wkt = response.responseText

                    var format = new ol.format.WKT();
                    var features = format.readFeatures(wkt);

                    if (features.length > 0) {
                        transformed = new ol.Collection([features[0].getGeometry().transform('EPSG:4326','EPSG:3857')])
                        var layer = new ol.layer.Vector({
                            source: new ol.source.Vector({
                                features: features
                            })
                        });
                        var bbox = layer.getSource().getExtent();
                        Ext.Msg.prompt('Layer', 'Please enter a layer name:', function (btn, text) {
                            if (btn == 'ok') {
                                var required = {
                                    layer: layer,
                                    layerTitle: text,
                                    layerName: text
                                };
                                var options = {
                                    folder: 'Flooding',
                                    bbox: bbox,
                                    remote: false,
                                    visible: true
                                }
                                VramApp.MapUtils.addWFSOverlay(required, options);

                            }
                        });
                    } else {
                        Ext.Msg.alert('Elevation', 'No polygons were found for this height');
                    }
                } catch (err) {
                    console.log(err);
                    Ext.Msg.alert('Elevation', 'Something went wrong, try again');
                }
                loadMask.hide();
            };
            var failCallback = function (response) {
                console.log("Server side failure" + response.status);
                loadMask.hide();
            };

            VramApp.LayerLoader.getContourLayer(data, successCallback, failCallback);
        }catch(err){
            loadMask.hide();
            console.log(err);
        }


    },

    activateGetFeatureInfoClick: function () {
        var map = VramApp.MapUtils.getOlMap();
        var me = this;
        this.key ? map.unByKey(this.key) : false;
        var view = map.getView();
        var source = this.layer.getSource();
        this.key = map.on('singleclick', function (evt) {
            me.coordinates = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326')
            var viewResolution = /** @type {number} */ (view.getResolution());
            var url = source.getGetFeatureInfoUrl(
                evt.coordinate, viewResolution, 'EPSG:3857',
                {'INFO_FORMAT': 'application/json'});

            VramApp.LayerLoader.getWMSFeatureInfo(url, this.handleWMSFeatureInfoResponse, this);
        }, this);
    },

    handleWMSFeatureInfoResponse: function (response, me) {
        var obj = Ext.decode(response.responseText);
        if (obj.features.length > 0) {
            var properties = obj.features[0].properties;
            for (var p in properties) {
                me.lookupReference('currentElevationValue').setValue(properties[p]);
            }
        }

    },

    getElevationDataInfo: function (layerName, workspaceName, successCallback, failCallback) {

        VramApp.LayerLoader.getElevationDataInfo(layerName, workspaceName, successCallback, failCallback);
    },

    clearMap: function () {
        var map = VramApp.MapUtils.getOlMap();
        this.layer ? map.removeLayer(this.layer) : false;
        this.rasterImage ? map.removeLayer(this.rasterImage) : false;
        this.key ? map.unByKey(this.key) : false;
    },


    transformeElevationLevel: function (value) {
        return value * 100;
    },

    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.clearMap();
        this.getView().close();
    }
});
