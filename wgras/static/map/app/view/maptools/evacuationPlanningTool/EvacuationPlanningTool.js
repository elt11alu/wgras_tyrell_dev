/**
 * Created by anton on 4/8/16.
 */

Ext.define('VramApp.view.maptools.evacuationPlanningTool.EvacuationPlanningTool', {
    extend: 'Ext.window.Window',

    requires: [
        'VramApp.view.maptools.evacuationPlanningTool.EvacuationPlanningToolController'
    ],


    xtype: 'evacuationplanningtool',
    glyph: 0xf071,


    controller: 'evacuationplanning',

    /*Specific configs*/
    width: 300,
    title: 'Evacuation planning',
    autoShow: true,
    closable: false,
    bodyPadding: 10,
    viewModel: true,

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'bottom',
        items: [
            {
                xtype: 'button',
                text: 'close',
                reference: 'closeBtn',
                handler: 'onClose'

            },
            {
                xtype: 'displayfield',
                value: '',
                reference: 'mainMsgField'

            }
        ]
    }],

    items: [
        {
            xtype: 'panel',
            reference: 'mainpanel',
            items: [
                {
                    xtype: 'combo',
                    fieldLabel: 'City',
                    reference: 'city',
                    store: ['Lund:Sweden', 'Constitution:Chile'],
                    value: 'Lund:Sweden'
                }, {
                    xtype: 'checkboxfield',
                    boxLabel: 'Use constraint',
                    reference: 'useconstraint',
                    name: 'useconstraint',
                    inputValue: '0',
                    id: 'checkbox1',
                    tooltip: 'sdfsaf'
                },
                {
                    xtype: 'combo',
                    store: ['distance', 'quantity'],
                    fieldLabel: 'Constraint Method',
                    value: 'distance',
                    bind: {
                        disabled: '{!useconstraint.checked}'
                    }
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'Constraint value',
                    value: 500,
                    minValue: 0,
                    bind: {
                        disabled: '{!useconstraint.checked}'
                    }
                },
                {
                    xtype: 'numberfield',
                    fieldLabel: 'Number of loops',
                    value: 22,
                    minValue: 0,
                    bind: {
                        disabled: '{!useconstraint.checked}'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Calculate',
                    reference: 'submitbtn',
                    handler: 'onCalculate'
                }
            ]
        },

    ]
});
