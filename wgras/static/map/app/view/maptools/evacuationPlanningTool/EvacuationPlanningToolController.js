/**
 * Created by anton on 4/8/16.
 */


Ext.define('VramApp.view.maptools.evacuationPlanningTool.EvacuationPlanningToolController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.evacuationplanning',

    requires: [],

    /**
     * Called when the view is created
     */
    init: function (view) {

    },


    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close();
    },

    onCalculate: function () {
        var me = this;
        var cities = {'Lund:Sweden':'lund', 'Constitution:Chile':'chile'}
        var panel = this.lookupReference('mainpanel');
        var city = this.lookupReference('city').getValue();
        var data = JSON.stringify({'city': cities[city]})
        var loadMask = new Ext.LoadMask({
            msg: 'Processing...',
            target: panel
        });
        try {

            loadMask.show();
            me.toggleCloseBtn(true);
            var success = function (resp) {
                var obj = Ext.decode(resp.responseText);
                var statusUrl = obj.progressStatus;
                me.checkStatus(statusUrl, loadMask)
            };
            var fail = function (resp) {
                me.toggleCloseBtn(false);
                loadMask.hide();
                me.setMainFailMessage('The process could not be executed,<br> please try again later.')
            };
            VramApp.LayerLoader.getEvacuationPlanningLayer(data, success, fail)

        } catch (e) {
            me.toggleCloseBtn(false);
            loadMask.hide();
            me.setMainFailMessage('The process could not be executed, please try again later.')
        }

    },

    checkStatus: function (url, loadMask) {
        var me = this;
        var success = function (resp, id) {
            var obj = Ext.decode(resp.responseText);
            if (obj.state == 'SUCCESS') {
                clearInterval(id);
                loadMask.hide();
                loadMask.msg = 'Process status:<br>'+ 'Building geometries..';
                loadMask.show();
                setTimeout(function(){
                    Ext.Msg.alert('Process', 'The process finished and the result was added to map');
                    me.addResultToMap(obj.result);
                    loadMask.hide();
                    me.toggleCloseBtn(false);
                }, 2000);


            } else if (obj.state == 'FAILURE') {
                clearInterval(id);
                loadMask.hide();
                me.toggleCloseBtn(false);
                me.setMainFailMessage('The process could not be executed, please try again later.')
            }else{
                loadMask.hide();
                loadMask.msg = 'Process status:<br>'+obj.status;
                loadMask.show();
            }
        };
        var fail = function (resp, id) {
            clearInterval(id);
            console.warn('Something went wrong');
            loadMask.hide();
            me.toggleCloseBtn(false);
            me.setMainFailMessage('The process could not be executed, please try again later.')
        };
        var id = window.setInterval(function () {
            VramApp.HTTPRequests.GET(url, success, fail, id)
        }, 1000);

    },

    addResultToMap: function (kmlArray) {
        var format = new ol.format.KML({extractStyles: true});
        var num = 0;
        kmlArray.forEach(function (el, ind, arr) {
            num++;
            var solution = el.solutionDesc;
            var random = Math.floor(Math.random() * 10000) + 1
            var features = format.readFeatures(el.geom, {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'})
            var layer = new ol.layer.Vector({
                source: new ol.source.Vector({
                    features: features
                })
            });
            var bbox = layer.getSource().getExtent();
            var req = {
                layer: layer,
                layerTitle: solution.concat('_id_:', random),
                layerName: solution.concat('_id_:', random)
            };
            var opt = {
                visible: true,
                remote: false,
                ancestor: false,
                bbox: bbox,
                abstract: '',
                folder: 'EvacuationPlanning'
            };
            VramApp.MapUtils.addWFSOverlay(req, opt)
            VramApp.MapUtils.panToExtent(bbox)

        })
    },



    toggleCloseBtn: function (cond) {
        this.lookupReference('closeBtn').setDisabled(cond)
    },

    setMainSuccessMessage: function (msg) {
        this.lookupReference('mainMsgField').setValue('<p style="color:cadetblue">' + msg + '</p>')
    },

    setMainFailMessage: function (msg) {
        this.lookupReference('mainMsgField').setValue('<p style="color:indianred">' + msg + '</p>')
    }

});
