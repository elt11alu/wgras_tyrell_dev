/**
 * Created by anton on 5/23/16.
 */

Ext.define('VramApp.view.maptools.floodFill.FloodFillMainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.floodfillmaincontroller',
    id: 'floodfillmaincontroller',

    requires: [
        'VramApp.LayerLoader',
        'VramApp.MapUtils'
    ],

    currentSelectedRecord: false,
    bboxLayerIdentifier: false,

listen: {
        controller: {
            '*': {
                elevationError: 'setMainFailMessage',
            }
        }
    },

    init: function (view) {
        this.currentTab = this.lookupReference('toolTabPanel').getActiveTab().toolId;
        this.addTabListener(view);
        this.loadElevationData();
        this.addListenerToGrid();
    },

    addTabListener: function(){
       this.lookupReference('toolTabPanel').on({
           tabChange: this.onTabChange,
           scope: this
       });
    },

    onTabChange: function(tabs, newTab, oldTab){
        var grid;
        this.currentTab = newTab.toolId;
        grid = this.lookupReference('elevationdatagrid');
        grid.getSelectionModel().deselectAll()
        this.fireEvent('elevationDataDeselected')
    },

    loadElevationData: function () {
        var grid, store;
        grid = this.lookupReference('elevationdatagrid');
        store = grid.getStore();
        store.load()
    },

    addListenerToGrid: function () {
        var grid;
        grid = this.lookupReference('elevationdatagrid');
        grid.getSelectionModel().on('select', this.onElevationDataSelection, this);
    },

    onElevationDataSelection: function (selmodel, record, index, eOpts) {
        //this.loadStatisticsForRaster(record.get('layerName'), record.get('workspaceName'), record);
        this.fireEvent('elevationDataSelected', this.currentTab, record);
        this.zoomToLayer(record);

    },

    loadStatisticsForRaster: function (layerName, workspaceName, record) {
        var succesCallback, failCallback, loadingSpinner, statistics, me;
        me = this;

        if (this.currentTab == 'localfloodfill') {


            loadingSpinner = this.getLoadMask('Loading statistics for layer...');

            succesCallback = function (response) {
                loadingSpinner.hide();
                var obj = Ext.decode(response.responseText);
                statistics = obj.statistics;
                record.set('minValue', parseInt(statistics.minValue));
                record.set('maxValue', parseInt(statistics.maxValue));
                record.set('meanValue', parseInt(statistics.meanValue));
                record.set('stdvValue', parseInt(statistics.stdvValue));
                me.setMainSuccessMessage('Layer loaded successfully.');
                me.fireEvent('elevationDataSelected', me.currentTab, record)
            };

            failCallback = function (response) {
                loadingSpinner.hide();
                var obj = Ext.decode(response.responseText);
                me.setMainFailMessage('Layer could not load, try later.');
            };
            loadingSpinner.show()
            this.doStatisticsRequest(layerName, workspaceName, succesCallback, failCallback);
        }
        else {
            me.fireEvent('elevationDataSelected', me.currentTab, record)
        }
    },

    doStatisticsRequest: function (layerName, workspaceName, successCallback, failCallback) {
        VramApp.LayerLoader.getElevationDataInfo(layerName, workspaceName, successCallback, failCallback);
    },

    zoomToLayer: function (record) {
        var bbox4326 = VramApp.MapUtils.getExtentByLayerId(record.get('layerId'));
        var bbox = VramApp.MapUtils.reprojectExtentTo3857(bbox4326, 'EPSG:4326');
        VramApp.MapUtils.panToExtent(bbox);
        this.createVisualbbox(bbox)
    },

    createVisualbbox: function (bbox) {
        var layer, poly, style;
        this.removeLastVisualbbox();

        poly = new ol.Feature({
            geometry: new ol.geom.Polygon.fromExtent(bbox)
        });

        style = this.getBboxStyle();

        layer = new ol.layer.Vector({
            source: new ol.source.Vector({
                features: [poly]
            })
        });
        this.bboxLayerIdentifier = VramApp.MapUtils.addTempLayer(layer, {'style': style})
    },

    removeLastVisualbbox: function(){
      this.bboxLayerIdentifier ? VramApp.MapUtils.removeLayerByLayerIdentifier(this.bboxLayerIdentifier):false;
    },

    getLoadMask: function (loadMessage) {
        var view = this.getView()
        return new Ext.LoadMask({
            msg: loadMessage,
            target: view
        });
    },

    getBboxStyle: function () {
        return new ol.style.Style({
            image: new ol.style.Circle({
                fill: new ol.style.Fill({
                    color: 'rgba(255,255,255,0.0)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgba(255,255,255,1)',
                    width: 1
                }),
                radius: 5
            }),
            fill: new ol.style.Fill({
                    color: 'rgba(255,255,255,0.0)'
                }),
            stroke: new ol.style.Stroke({
                    color: 'rgba(255,255,255,1)',
                    width: 1
                })
        })
    },

    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.fireEvent('closingFloodFillTool')
        this.removeLastVisualbbox();
        this.getView().close()
    },

    setMainSuccessMessage: function(msg){
        this.lookupReference('mainMsgField').setValue('<p style="color:green">'+msg+'</p>')
    },

    setMainFailMessage: function(msg){
        this.lookupReference('mainMsgField').setValue('<p style="color:red">'+msg+'</p>')
    }


});
