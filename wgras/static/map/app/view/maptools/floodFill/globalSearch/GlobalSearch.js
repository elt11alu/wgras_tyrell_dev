/**
 * Created by anton on 5/20/16.
 */

Ext.define('VramApp.view.maptools.floodFill.globalSearch.GlobalSearch', {
    extend: 'Ext.form.Panel',
    requires: [
        'Ext.button.Button',
        'VramApp.view.maptools.floodFill.globalSearch.GlobalSearchController'
    ],
    xtype: 'globalsearchform',
    controller: 'globalsearch.globalsearchcontroller',
    bodyPadding: 10,
    items: [
        {
            xtype: 'fieldcontainer',
            padding: 10,
            //fieldLabel: 'Area of interest',
            name: 'BBOX',
            items: [
                {
                    xtype: 'button',
                    text: 'Clear BBOX',
                    reference: 'buttonBBox',
                    handler: 'clearBBox',
                    disabled: 'true'
                },
                {
                    xtype: 'button',
                    text: 'Clear all points',
                    margin: '0 0 0 20',
                    reference: 'buttonCAP',
                    handler: 'clearAllPoints',
                    disabled: 'true'
                }
                /*{
                    xtype: 'checkboxfield',
                    name: 'checkbox1',
                    reference: 'bBoxCheck',
                    boxLabel: 'Show Bounding Box',
                    handler: 'bBoxVisible',
                    checked: true,
                    disabled: 'true'
                }*/
            ]
        },

        {
            xtype: 'fieldcontainer',
            name: 'waterIncrease',
            padding: 10,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'slider',
                    width: 200,
                    fieldLabel: 'Water level increase (meter):',
                    labelAlign: 'top',
                    reference: 'waterSlider',
                    minValue: 0,
                    maxValue: 10,
                    //increment: 2
                    decimalPrecision: 1
                },
                {
                    xtype: 'displayfield',
                    reference: 'currentWaterValue',
                    //fieldLabel:'Current value',
                    value: 0
                }
            ]
        },
        {
            xtype: 'button',
            text: 'Extract polygons',
            margin: '0 0 0 10',
            handler: 'onFloodFill'
        },
        {
            xtype: 'displayfield',
            margin: 3,
            reference: 'MsgField',
            style: 'opacity:0;'
        }
    ]
});
