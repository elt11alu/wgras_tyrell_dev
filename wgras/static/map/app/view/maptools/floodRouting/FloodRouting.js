/**
 * Created by ludvi on 2016-02-03.
 */

Ext.define('VramApp.view.maptools.floodRouting.FloodRouting', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'VramApp.view.maptools.floodRouting.FloodRoutingController',
        'VramApp.view.maptools.floodRouting.FloodRoutingModel'
    ],

    xtype: 'floodroutingtool',
    alias: 'widget.floodroutingtool',

    /*Specific configs*/
    width: 380,
    height: 320,
    glyph: 0xf1b9,
    title: 'Flood Routing tool',
    autoShow: true,
    closable: false,

    controller: 'floodroutingcontroller',

    items: [
        {
            xtype: 'combo',
            width: 250,
            reference: 'inputLayerSelector',
            fieldLabel: 'Input flood areas',
            labelAlign: 'top',
            displayField: 'title',
            padding: 10,
            queryMode: 'local',
            store: 'importedVectorLayers',
            editable: false,
            multiSelect: false,

            listeners: {
                'select': 'onInputLayerSelection'
            }

        },
        {
            xtype: 'segmentedbutton',
            height: 30,
            width: 300,
            listeners: {
                toggle: 'onAddressTypeToggle'
            },
            items: [{
                text: 'Google',
                pressed: true,
                isGoogle: true

            }, {
                text: 'OSM',
                isOSM: true

            }, {
                text: 'What3Words',
                isW3W: true
            }]
        },
        {
            xtype: 'fieldcontainer',
            name: 'PointA',
            padding: 10,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [

                {
                    xtype: 'textfield',
                    width: 250,
                    fieldLabel: 'From',
                    labelWidth: 40,
                    name: 'addressA',
                    emptyText: 'Enter adress or click in map',
                    enableKeyEvents: true,
                    listeners: {
                        keypress: 'onEnterKey'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Clear',
                    margin: '0 0 0 10',
                    reference: 'buttonA',
                    handler: 'clearPointA',
                    disabled: 'true'
                }
            ]
        },
        {
            xtype: 'fieldcontainer',
            name: 'PointB',
            padding: 10,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'textfield',
                    width: 250,
                    fieldLabel: 'To',
                    labelWidth: 40,
                    name: 'addressB',
                    emptyText: 'Enter adress or click in map',
                    enableKeyEvents: true,
                    listeners: {
                        keypress: 'onEnterKey'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Clear',
                    margin: '0 0 0 10',
                    reference: 'buttonB',
                    handler: 'clearPointB',
                    disabled: 'true'
                }
            ]
        },
        {
            xtype: 'displayfield',
            margin: 3,
            reference: 'MsgField',
            style: 'opacity:0;'
        }
    ],
    buttons: [
        {
            xtype: 'button',
            text: 'Route!',
            reference: 'buttonRoute',
            handler: 'onFloodRoute',
            dock: 'bottom',
            disabled: 'true'
        },
        {
            text: 'close',
            handler: 'onClose',
            dock: 'bottom'
        }
    ]
});


