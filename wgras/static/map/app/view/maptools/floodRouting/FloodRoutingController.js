/**
 * Created by ludvi on 2016-02-03.
 */


Ext.define('VramApp.view.maptools.floodRouting.FloodRoutingController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.floodroutingcontroller',

    init: function () {
        this.addressType = 'Google';
        var me = this;
        me.transform2WGS = ol.proj.getTransform('EPSG:3857', 'EPSG:4326');
        me.transform2Web = ol.proj.getTransform('EPSG:4326', 'EPSG:3857');
        me.startCoord = null;
        me.endCoord = null;

        me.listenerKey = me.map().on('click', function (evt) {
            if (me.startCoord === null) {
                me.setStartCoord(me.transform2WGS(evt.coordinate));
            }
            else if (me.startCoord !== null && me.endCoord == null) {
                me.setEndCoord(me.transform2WGS(evt.coordinate));
            }
            else if (me.startCoord !== null && me.endCoord !== null) {
                me.endCoord = null;
                me.clearResult();
                me.clearOverlay();
                me.coordBSelected(true);
                me.setStartCoord(me.transform2WGS(evt.coordinate));
            }
        })
    },
    map: function () {
        return VramApp.MapUtils.getOlMap();
    },
    setStartCoord: function (coord) {
        if (this.startCoord === null) {
            this.startCoord = coord;
            this.getCountryCode('start', coord);
            var transCoord = this.transform2Web(coord);
            this.overResult = '<b>From</b>';
            this.overlayAB('a', transCoord);
            this.coordASelected(false);
        } else {
            this.clearPointA();
            this.enableRoute(true);
            this.setStartCoord(coord);
        }
        if (this.endCoord !== null) {
            this.BBoxExtent2 = this.createBBox();
            this.enableRoute(false);
        }
    },
    setEndCoord: function (coord) {
        if (this.endCoord === null) {
            this.endCoord = coord;
            this.getCountryCode('end', coord);
            var transCoord = this.transform2Web(coord);
            this.overResult = '<b>To</b>';
            this.overlayAB('b', transCoord);
            this.coordBSelected(false);
            this.BBoxExtent2 = this.createBBox();
            this.enableRoute(false);
        } else {
            this.clearPointB();
            this.setEndCoord(coord);
        }
    },
    overlayAB: function (s, coord) {
        this.tooltipMainDiv = this.createMainDiv();
        this.tooltipArrow = this.createArrow();
        if (s == 'a') {
            this.AOverlay = this.getOverlay();
            this.map().addOverlay(this.AOverlay);

            this.tooltipMainDiv.innerHTML = this.overResult;
            this.tooltipMainDiv.appendChild(this.tooltipArrow);
            this.AOverlay.setElement(this.tooltipMainDiv);
            this.AOverlay.setPosition(coord);
        }
        if (s == 'b') {
            this.BOverlay = this.getOverlay();
            this.map().addOverlay(this.BOverlay);

            this.tooltipMainDiv.innerHTML = this.overResult;
            this.tooltipMainDiv.appendChild(this.tooltipArrow);
            this.BOverlay.setElement(this.tooltipMainDiv);
            this.BOverlay.setPosition(coord);
        }
    },
    createMainDiv: function () {
        var div = document.createElement('div');
        div.style.color = "rgb(0,0,0)";
        div.style.background = "rgb(255,204,0)";
        div.style.padding = "5px 4px";
        div.style.opacity = "0.8";
        div.style.border = "2px solid";
        div.style.borderColor = "#FFCC00";
        div.style.borderRadius = "4px";

        return div;
    },
    createArrow: function () {
        var arrow = document.createElement('div');
        arrow.style.width = 0;
        arrow.style.height = 0;
        arrow.style.borderTop = "8px solid rgb(255,204,0)";
        arrow.style.borderRight = "4px solid transparent";
        arrow.style.borderLeft = "4px solid transparent";
        arrow.style.content = "";
        arrow.style.position = "absolute";
        arrow.style.marginLeft = "-4px";
        arrow.style.bottom = "-7px";
        arrow.style.left = "50%";

        return arrow;
    },
    getOverlay: function () {
        var overlay = new ol.Overlay({
            offset: [0, -7],
            positioning: 'bottom-center'
        });
        return overlay;
    },
    createBBox: function () {
        var squareExtent, startTrans, endTrans, iniX1, iniY1, iniX2, iniY2, xDiff, yDiff, x1, y1, x2, y2,
            width, height, totDiff;

        startTrans = this.transform2Web(this.startCoord);
        endTrans = this.transform2Web(this.endCoord);

        iniX1 = Math.min(startTrans[0], endTrans[0]);
        iniY1 = Math.min(startTrans[1], endTrans[1]);
        iniX2 = Math.max(startTrans[0], endTrans[0]);
        iniY2 = Math.max(startTrans[1], endTrans[1]);

        xDiff = iniX2 - iniX1;
        yDiff = iniY2 - iniY1;

        x1 = iniX1 - ((xDiff * 0.3) + 5000);
        y1 = iniY1 - ((yDiff * 0.3) + 5000);
        x2 = iniX2 + ((xDiff * 0.3) + 5000);
        y2 = iniY2 + ((yDiff * 0.3) + 5000);

        height = y2 - y1;
        width = x2 - x1;

        totDiff = Math.max(width, height) - Math.min(width, height);

        if (width > height) {
            squareExtent = [x1, y1 - (totDiff / 2), x2, y2 + (totDiff / 2)];
        }
        if (width < height) {
            squareExtent = [x1 - (totDiff / 2), y1, x2 + (totDiff / 2), y2];
        }
        if (width == height) {
            squareExtent = [x1, y1, x2, y2];
        }

        return this.transform2WGS(squareExtent);
    },
    coordASelected: function (b) {
        var pointAButton = this.lookupReference('buttonA');
        pointAButton.setDisabled(b);
    },
    coordBSelected: function (b) {
        var pointBButton = this.lookupReference('buttonB');
        pointBButton.setDisabled(b);
    },
    enableRoute: function (b) {
        var routeButton = this.lookupReference('buttonRoute');
        routeButton.setDisabled(b);
    },
    clearPointA: function () {
        this.clearResult();
        this.enableRoute(true);
        this.startCoord = null;
        this.map().removeOverlay(this.AOverlay);
        this.coordASelected(true);
    },
    clearPointB: function () {
        this.clearResult();
        this.enableRoute(true);
        this.map().removeOverlay(this.BOverlay);
        this.endCoord = null;
        this.coordBSelected(true);
    },
    setErrorMessage: function (message) {
        var field = this.lookupReference('MsgField');
        field.setStyle({
            "opacity": 1
        });
        field.setValue('<span style="color:red;font-size:100%;font-style:italic">Error: ' + message + '</span>');
        field.animate({
            duration: 6000,
            to: {
                opacity: 0
            }
        });
    },
    onFloodRoute: function () {
        var me, BBoxX1, BBoxY1, BBoxX2, BBoxY2, roadTable, viewParams, loadListener, cntCode;

        me = this;
        this.clearResult();
        this.getView().setLoading(true);

        if (this.startCnt != this.endCnt) {
            this.getView().setLoading(false);
            this.setErrorMessage('Only possible to rout within one country');
        }
        else {
            cntCode = this.startCnt.toLowerCase();
        }

        if (this.floodLayerName == undefined) {
            this.setErrorMessage('No flood areas selected');
            this.getView().setLoading(false);
        }

        BBoxX1 = this.BBoxExtent2[0];
        BBoxY1 = this.BBoxExtent2[1];
        BBoxX2 = this.BBoxExtent2[2];
        BBoxY2 = this.BBoxExtent2[3];

        roadTable = cntCode + '_ways';

        viewParams = [
            'x1:' + this.startCoord[0], 'y1:' + this.startCoord[1],
            'x2:' + this.endCoord[0], 'y2:' + this.endCoord[1],
            'BBoxX1:' + BBoxX1, 'BBoxY1:' + BBoxY1,
            'BBoxX2:' + BBoxX2, 'BBoxY2:' + BBoxY2,
            'floodAreas:' + this.floodLayerName, 'roads:' + roadTable
        ];

        loadListener = function () {
            me.getView().setLoading(false);
        };

        this.resultLayer = VramApp.LayerLoader.getGeoServerImageWMSWithViewParams('FloodRoutingW', 'FloodRoutingW:FloodRouting', viewParams, loadListener);

        VramApp.MapUtils.addLayerToMap(this.resultLayer, 'floodRouteLayer');
    },
    onAddressTypeToggle: function (container, button, pressed) {
        if (pressed) {
            if (button.isGoogle) {
                this.addressType = 'Google';
            }
            if (button.isOSM) {
                this.addressType = 'OSM';
            }
            if (button.isW3W) {
                this.addressType = 'W3W';
            }
        }
    },
    onInputLayerSelection: function (combo1, geoSLayer) {
        if (this.floodLayer) {
            this.floodLayer.setVisible(false);
        }
        this.floodLayerName = geoSLayer.get('geoServerName').split(':')[1];
        var floodLayerTitle = geoSLayer.get('title');
        this.floodLayer = VramApp.MapUtils.getLayerByName(floodLayerTitle);
        this.floodLayer.setVisible(true);
    },
    getCountryCode: function(s, coord) {
        var httpString, me, countryCode;
        me = this;
        httpString = '/getAddressFromCoord/params?lat=' + coord[1] + '&lng=' +coord[0];

        Ext.Ajax.request({
            url: httpString,
            success: function (response, opts) {
                countryCode = Ext.decode(response.responseText)['cnt'];
                if (s == 'start') {
                    me.startCnt = countryCode;
                }
                else if (s == 'end') {
                    me.endCnt = countryCode;
                }
            },
            failure: function (response, opts) {
                me.setErrorMessage('server-side failure with status code ' + response.status);
            }
        });
    },

    onAddressSelector: function (addressString, fieldName) {
        var httpString, me;
        httpString = '/getCoordFromAddress/params?address=' + addressString + '&method=' + this.addressType;
        me = this;

        Ext.Ajax.request({
            url: httpString,
            success: function (response, opts) {
                if (fieldName == 'addressA') {
                    me.addressCoordA = Ext.decode(response.responseText);
                    var coordA = [me.addressCoordA['lng'], me.addressCoordA['lat']];
                    me.setStartCoord(coordA);
                    if (me.endCoord !== null) {
                        me.onFloodRoute();
                    }
                } else if (fieldName == 'addressB') {
                    me.addressCoordB = Ext.decode(response.responseText);
                    var coordB = [me.addressCoordB['lng'], me.addressCoordB['lat']];
                    me.setEndCoord(coordB);
                    me.onFloodRoute();
                }
            },
            failure: function (response, opts) {
                me.setErrorMessage('server-side failure with status code ' + response.status);
            }
        });
    },
    onEnterKey: function (f, e) {
        if (e.getKey() == e.ENTER) {
            if (VramApp.InputValidationModel.validateLocationSearch(f.lastValue)) {
                this.onAddressSelector(f.lastValue, f.name);
            }
            else {
                this.setErrorMessage('Unallowed characters in address')
            }
        }
    },
    clearResult: function () {
        this.resultLayer ? VramApp.MapUtils.getOlMap().removeLayer(this.resultLayer) : false;
    },
    clearOverlay: function () {
        this.map().getOverlays().clear();
    },
    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.floodLayer ? this.floodLayer.setVisible(false) : false;
        this.clearPointA;
        this.clearPointB();
        ol.Observable.unByKey(this.listenerKey);
        this.clearResult();
        this.clearOverlay();
        this.getView().close();
    }
});