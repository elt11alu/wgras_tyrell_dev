/**
 * Created by ludvi on 2016-02-18.
 */
Ext.define('VramApp.view.maptools.floodRouting.FloodRoutingModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.floodRouting',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'FloodRouting',
            autoLoad: true
        }
        */
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});