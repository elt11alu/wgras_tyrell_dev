/**
 * Created by anton on 2015-11-01.
 */
    Ext.define('VramApp.view.maptools.geoLocationTool.GeoLocationTool', {
        extend: 'Ext.form.field.Text',
        xtype: 'geolocationtool',

        requires: [
            'VramApp.view.maptools.geoLocationTool.GeoLocationController',
            'VramApp.view.maptools.geoLocationTool.GeoLocationModel'
        ],
        inputType:'search',
        width:300,
        emptyText:'adress or city or country',
        controller:'geolocationcontroller',
        name:'locationSearch',
        viewModel: {
            type: 'geolocation'
        },
        fieldLabel:'Map search',
        labelStyle:'font-weight:bold',
        labelPad:2


});