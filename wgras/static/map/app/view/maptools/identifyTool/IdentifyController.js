/**
 * Created by anton on 2015-09-15.
 */

Ext.define('VramApp.view.maptools.identifyTool.IdentifyController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.identifycontroller',


    mapUtils: function () {
        return VramApp.MapUtils;
    },

    init: function () {
        var me = this;
        var map = VramApp.MapUtils.getOlMap();
        this.listenerFunc = function (event) {
            me.getFeatureProperties(event, me)
        };
        this.selectListenerKey = this.mapUtils().getSelectInteraction().on('select', me.listenerFunc);

        this.getView().on({
            beforeclose: this.enableButtons,
            beforedestroy: this.enableButtons,
            scope: this
        });




    },

    enableButtons: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
    },

    getFeatureProperties: function (event, me) {

        if (event.selected.length > 0) {
            try{
                var properties = event.selected[0].getProperties();
                if (properties) {

                    var list = '<table class="identityTable" style="width:100%"><tr><th>Attr</th><th>Value</th></tr>'
                    for (var p in properties) {
                        if(p!='geometry' && p!='the_geom'){
                            list += '<tr>';
                            list += '<td>' + p + '</td>';
                            list += '<td>' + properties[p] + '</td>';
                            list += '</tr>';
                        }
                    }
                    list+= '</table>';

                    me.getViewModel().setData({
                        attributeList: list
                    });
                }
            }catch(err){
                console.warn(err)
            }

        }


    },
    onClose: function () {
        var me = this;
        this.mapUtils().getSelectInteraction().un('select', me.listenerFunc);
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close();
    }
});