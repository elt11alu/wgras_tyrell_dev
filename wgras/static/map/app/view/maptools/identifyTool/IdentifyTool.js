/**
 * Created by anton on 2015-09-15.
 */

Ext.define('VramApp.view.maptools.identifyTool.IdentifyTool', {
    extend: 'Ext.window.Window',
    requires:[
        'Ext.form.field.Display',
        'VramApp.view.maptools.identifyTool.IdentifyController',
        'VramApp.view.maptools.identityTool.IdentityToolModel'
    ],
    alias: 'widget.identifytool',
    xtype: 'identifytoolwindow',
    title: 'Identify',
    glyph: 0xf245,
    autoShow: true,
    width: 250,
    height: 250,
    collapsible:true,
    bodyPadding: 4,
    closable: false,
    scrollable: true,
    controller: 'identifycontroller',

    viewModel: {
        type: 'identitytoolmodel'
    },

    items: [
        {
            xtype: 'displayfield',
            bind: '{attributeList}'
        }
    ],
    buttons: [
        {
            text: 'Close',
            handler:'onClose'

        }]


});
