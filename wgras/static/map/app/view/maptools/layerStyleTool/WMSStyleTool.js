/**
 * Created by antonlu66 on 11/2/2015.
 */
Ext.define('VramApp.view.maptools.layerStyleTool.WMSStyleTool', {


        extend: 'Ext.window.Window',
        xtype: 'wmsstyletool',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.Display',
        'Ext.layout.container.Anchor',
        'Ext.picker.Color',
        'Ext.slider.Single',
        'VramApp.view.maptools.layerStyleTool.WMSStyleToolController'
    ],

    controller:'wmsstyletoolcontroller',



        title:{},
        layer:{},

        glyph: 0xf1fc,

        autoShow: true,

        items: [{
            xtype: 'form',
            reference: 'wmsStyleForm',
            padding: 10,
            layout: 'anchor',
            defaults: {
                anchor: '85%'
            },
            items: [ {
                xtype: 'slider',
                fieldLabel: 'Layer opacity',
                name: 'layerOpacity',
                reference:'wmsopacityslider',
                width: 250,
                value: 2,
                increment: 1,
                minValue: 0,
                maxValue: 10
            }],
            buttons: [{
                text: 'Ok',
                handler:'onStyleClick'

            }, {
                text: 'Close',
                handler:'onCloseClick'
            }]

        }]
    });
