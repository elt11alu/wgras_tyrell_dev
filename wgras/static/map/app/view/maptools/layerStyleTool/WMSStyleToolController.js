/**
 * Created by antonlu66 on 11/2/2015.
 */
Ext.define('VramApp.view.maptools.layerStyleTool.WMSStyleToolController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wmsstyletoolcontroller',

    /**
     * Called when the view is created
     */
    init: function () {
        try {
            var layer = VramApp.MapUtils.getLayerByDBId(this.getView().layerId);
            var opacity = layer.getOpacity();
            this.lookupReference('wmsopacityslider').setValue(opacity * 10);
        } catch (err) {
            console.warn(err)
        }

    },

    onStyleClick: function () {
        try {
            var values = this.lookupReference('wmsStyleForm').getForm().getValues();
            var opacity = parseInt(values.layerOpacity, 10);
            var layer = VramApp.MapUtils.getLayerByDBId(this.getView().layerId);
            layer.setOpacity(opacity / 10);
        } catch (err) {
            console.warn(err);
        }

    },

    onCloseClick: function () {
        this.getView().close();
    }

});