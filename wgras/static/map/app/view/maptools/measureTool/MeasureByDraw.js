/**
 * Created by anton on 2015-09-20.
 */

Ext.define("VramApp.view.maptools.measureTool.MeasureByDraw", {
    requires: ['VramApp.MapUtils'],

    startMeasureByDraw: function (geomType) {
        this.geomType = geomType;
        this.map = VramApp.MapUtils.getOlMap();
        this.drawLayer = this.createDrawLayer();
        this.drawInteraction = this.createDrawInteraction(this.drawLayer.getSource(), this.geomType);
        this.tooltipMainDiv = this.createMainDiv();
        this.tooltipArrow = this.createArrow();
        this.overlay = this.getOverlay();
        this.map.addLayer(this.drawLayer);
        this.map.addInteraction(this.drawInteraction);
        this.map.addOverlay(this.overlay);
        this.listenerKeyDrawEnd = this.drawInteraction.on('drawstart', this.showMeasurement, this);


    },

    stopMeasureByDraw: function () {
        try {
            this.map.removeLayer(this.drawLayer);
            this.map.removeInteraction(this.drawInteraction);
            this.map.removeOverlay(this.overlay);
            ol.Observable.unByKey(this.listenerKeyDrawEnd);
            ol.Observable.unByKey(this.onGeometryChangeListener);
        } catch (e) {
            console.warn(e)
        }
    },

    showMeasurement: function (event) {
        this.drawLayer.getSource().clear();
        var me = this;
        var feature = event.feature;
        //var geometry = feature.getGeometry();
        var length;
        var area;
        var coord;
        var result = "";
        this.onGeometryChangeListener = feature.getGeometry().on('change', function (event) {
            var geometry = event.target;
            if (geometry instanceof ol.geom.LineString) {
                coord = geometry.getLastCoordinate();
                length = me.formatLength(geometry);
                result = "<p style='margin:0'>Length: " + length + "</p>";
                area = 0;
            } else if (geometry instanceof ol.geom.Polygon) {
                coord = geometry.getInteriorPoint().getCoordinates();
                length = 0;
                area = me.formatArea(geometry);
                result = "<p style='margin:0'>Area: " + area + "</p>";
            }

            // result = "<p style='margin:0'>Area: " + area + "</p>";
            //result += "<p style='margin:0'>Length: " + length + "</p>";
            me.tooltipMainDiv.innerHTML = result;
            me.tooltipMainDiv.appendChild(me.tooltipArrow);
            me.overlay.setElement(me.tooltipMainDiv);
            me.overlay.setPosition(coord);
        }, me)


    },

    createDrawLayer: function () {
        var source = new ol.source.Vector();

        var vector = new ol.layer.Vector({
            source: source,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: '#ffcc33',
                    lineDash: [10, 10],
                    width: 2
                }),
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: '#ffcc33'
                    })
                })
            })
        });

        return vector;
    },

    createDrawInteraction: function (source, geomType) {
        var geom, draw;
        if (geomType === "line") {
            geom = "LineString"
        } else {
            geom = "Polygon"
        }
        draw = new ol.interaction.Draw({
            source: source,
            type: geom,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'rgba(0, 0, 0, 1)',
                    lineDash: [10, 10],
                    width: 2
                }),
                image: new ol.style.Circle({
                    radius: 5,
                    stroke: new ol.style.Stroke({
                        color: 'rgba(0, 0, 0, 0.7)'
                    }),
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0.2)'
                    })
                })
            })

        });
        return draw;
    },

    createMainDiv: function () {
        var div = document.createElement('div');
        div.style.color = "rgb(255,255,255)";
        div.style.background = "rgb(0,0,0)";
        div.style.padding = "5px 4px";
        div.style.opacity = "0.8";
        div.style.border = "2px solid";
        div.style.borderColor = "#FFCC00";
        div.style.borderRadius = "4px";

        return div;
    },

    createArrow: function () {

        var arrow = document.createElement('div');
        arrow.style.width = 0;
        arrow.style.height = 0;
        arrow.style.borderTop = "10px solid rgb(0,0,0)";
        arrow.style.borderRight = "10px solid transparent";
        arrow.style.borderLeft = "10px solid transparent";
        arrow.style.content = "";
        arrow.style.position = "absolute";
        arrow.style.marginLeft = "-5px";
        arrow.style.bottom = "-7px";
        arrow.style.left = "50%";


        return arrow;
    },

    getOverlay: function () {

        var overlay = new ol.Overlay({
            offset: [0, -15],
            positioning: 'bottom-center'
        });
        return overlay;
    },

    formatLength: function (line) {
        var length;

        length = Math.round(line.getLength() * 100) / 100;

        var output;
        if (length > 100) {
            output = (Math.round(length / 1000 * 100) / 100) +
                ' ' + 'km';
        } else {
            output = (Math.round(length * 100) / 100) +
                ' ' + 'm';
        }
        return output;
    },

    formatArea: function (polygon) {
        var area;

        area = polygon.getArea();

        var output;
        if (area > 10000) {
            output = (Math.round(area / 1000000 * 100) / 100) +
                ' ' + 'km<sup>2</sup>';
        } else {
            output = (Math.round(area * 100) / 100) +
                ' ' + 'm<sup>2</sup>';
        }
        return output;
    }
})
