/**
 * Created by anton on 2015-09-19.
 */

Ext.define('VramApp.view.maptools.measureTool.MeasureTool', {
    extend: 'Ext.window.Window',
    xtype: 'measuretoolwindow',

    requires: [
        'Ext.button.Segmented',
        'Ext.form.field.Display',
        'Ext.layout.container.VBox',
        'Ext.tab.Panel',
        'VramApp.view.maptools.measureTool.MeasureToolController'
    ],


    title: 'Measure',
    closable: true,
    autoShow: true,
    resizable: false,
    collapsible: true,
    controller: 'measuretoolcontroller',
    layout: 'vbox',
    icon: VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/measure.svg',
    items: [
        {
            xtype: 'tabpanel',
            reference: 'measureToolTabPanel',
            plain: false,
            bodyPadding: 8,
            width: 200,
            minHeight: 150,

            items: [
                {
                    title: 'Object',
                    measureTool: 'bySelection',
                    items: [
                        {
                            xtype: 'displayfield',
                            value: "<p><b>Make sure the map layer is activated by checking checkbox in <i>active</i> column in map data panel. "
                            + "Then click on feature to calculate area and length.</b></p>"
                            + "<p><i>Note: this tool only works for data of type vector. To measure on map use the ruler.</i></p>"
                        }
                    ]

                }, {
                    title: 'Ruler',
                    measureTool: 'byRuler',
                    items: [
                        {
                            xtype: 'segmentedbutton',
                            height: 50,
                            width: 180,
                            listeners: {
                                toggle: 'onGeomTypeToggle'
                            },
                            items: [{
                                text: 'Length',
                                tooltip: 'Measure length',
                                pressed:true,
                                handleLength: true
                            }, {
                                text: 'Area',
                                tooltip: 'Measure area',
                                handleArea: true
                            }]
                        },{
                            xtype: 'displayfield',
                            value: "<p><b>Click on button above to select length or area. Draw on map to start measure.</b></p>"
                            + "<p><i>Double click on mouse button to see result.</i></p>"
                        }]
                }
            ]

        }],
    buttons: [{

        text: 'close',
        handler: 'onClose',
        glyph: 0xf00d

    }]

});