/**
 * Created by anton on 2015-09-19.
 */

Ext.define('VramApp.view.maptools.measureTool.MeasureToolController', {
    extend: 'Ext.app.ViewController',
    alias: "controller.measuretoolcontroller",

    requires: [
        'VramApp.MapUtils',
        'VramApp.view.maptools.measureTool.MeasureBySelection',
        "VramApp.view.maptools.measureTool.MeasureByDraw"
    ],

    geomType: 'line',

    init: function (view) {
        var me = this;
        this.measureselecttool = Ext.create('VramApp.view.maptools.measureTool.MeasureBySelection');
        this.measuredrawtool = Ext.create('VramApp.view.maptools.measureTool.MeasureByDraw');
        this.activateSelectMeasurement(true);
        var tabpanel = this.lookupReference('measureToolTabPanel');
        view.on({
            close: function () {
                me.onClose();
                return false;//prevents default close action
            }
        });
        tabpanel.on({
            tabchange: this.onTabChange,
            scope: this
        });
    },

    onGeomTypeToggle: function (container, button, pressed) {
        if (pressed) {
            button.handleLength ? this.setGeomTypeAsLine() : this.setGeomTypeAsPolygon()
        }
    },

    onTabChange: function (tabs, newTab, oldTab) {
        var useTool = newTab.measureTool;

        if (useTool === 'bySelection') {

            this.activateSelectMeasurement(true);
            this.activateDrawMeasurement(false);

        } else if (useTool === 'byRuler') {

            this.activateSelectMeasurement(false);
            this.activateDrawMeasurement(true);

        }
    },

    setGeomTypeAsPolygon: function () {
        this.geomType = 'polygon';
        this.activateDrawMeasurement(false);
        this.activateDrawMeasurement(true)

    },

    setGeomTypeAsLine: function () {

        this.geomType = 'line';
        this.activateDrawMeasurement(false);
        this.activateDrawMeasurement(true)

    },

    activateSelectMeasurement: function (activate) {
        if (activate) {
            this.measureselecttool.startMeasureBySelect();
        } else {
            if (this.measureselecttool) {
                this.measureselecttool.stopMeasureBySelect();
            }
        }

    },

    activateDrawMeasurement: function (activate) {

        if (activate) {
            this.measuredrawtool.startMeasureByDraw(this.geomType);
        } else {
            if (this.measuredrawtool) {
                this.measuredrawtool.stopMeasureByDraw();
            }
        }

    },


    onClose: function () {
        try {
            this.measureselecttool.stopMeasureBySelect();
            this.measuredrawtool.stopMeasureByDraw();
            this.measuredrawtool.destroy();
            this.measureselecttool.destroy();
            VramApp.MapUtils.getSelectInteraction().setActive(true)
            this.getView().destroy();
            this.fireEvent('toggleHeadPanelButtons', false);
        } catch (error) {
            console.warn(error);
        }
    }
});
