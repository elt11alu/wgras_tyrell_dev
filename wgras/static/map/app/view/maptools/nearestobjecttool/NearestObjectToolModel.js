/**
 * Created by anton on 2015-11-25.
 */
Ext.define('VramApp.view.maptools.nearestobjecttool.NearestObjectToolModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.nearestobjecttool',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'NearestObjectTool',
            autoLoad: true
        }
        */
    },

    data: {
        attributeList:"<p><b>Chose a target layer in which<br> to search by clicking on the name<br> in the list above.<br> "
        + "Then click on map to search for<br> nearest feature from that location.</b></p><br>"
        + "<p><i>Note: this tool only works <br>for data of type vector</i></p>"
    }
});