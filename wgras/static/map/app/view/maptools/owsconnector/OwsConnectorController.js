/**
 * Created by anton on 2016-01-16.
 */
Ext.define('VramApp.view.maptools.owsconnector.OwsConnectorController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.owsconnector',


    /**
     * Called when the view is created
     */
    init: function () {

    },

    onSelectExistingService: function (combo, record) {
        this.getViewModel().set('url', record.get('url'));
        this.loadConnectionStore(record.get('url'))
    },

    onConnect: function (btn) {
        this.loadConnectionStore(this.getViewModel().get('url'));
    },

    loadConnectionStore: function (url) {
        var store = this.getViewModel().getStore('owsconnectionstore');
        store.load({
            scope: this,

            params: {
                url: url
            }
        })
    },

    onAddLayer: function (btn) {
        var record = btn.getWidgetRecord();
        var layer = VramApp.LayerLoader.getExternalTiledWMS_V111(this.getViewModel().get('url'), record.get('layerName'));
        var required = {
            layer: layer,
            layerName: record.get('layerName'),
            layerTitle: record.get('layerTitle')
        };
        var options = {
            folderName: 'External',
            abstract: record.get('abstract'),
            external: true
        };
        VramApp.MapUtils.addWMSOverlay(required, options);

        Ext.MessageBox.alert('Status', 'Data was added to map. To see the layer, please toggle layer visibility in the layer list to the right.');
    },

    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close()
    }
});
