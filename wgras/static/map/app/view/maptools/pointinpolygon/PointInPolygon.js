/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.maptools.pointinpolygon.PointInPolygon', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.button.Button',
        'Ext.data.Store',
        'Ext.form.RadioGroup',
        'Ext.form.field.ComboBox',
        'Ext.form.field.Display',
        'Ext.layout.container.VBox',
        'VramApp.view.maptools.pointinpolygon.PointInPolygonController',
        'VramApp.view.maptools.pointinpolygon.PointInPolygonModel'
    ],

    autoShow:true,
    closable:false,
    title:'Count points inside polygon',

    xtype: 'pointinpolygon',

    layout:'vbox',
    bodyPadding:5,
    icon: VramApp.WGRASAppGlobals.resources()+'resources/mapIcons/pointInPolygon.svg',

    viewModel: {
        type: 'pointinpolygon'
    },

    controller: 'pointinpolygon',

    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [
           /* {
                xtype:'radiogroup',
                fieldLabel:'Input polygon',
                columns:1,

                items:[
                    {boxLabel:'From layer',name:'inputRadio', checked:true}
                ]
            }
            */
            {
                xtype: 'displayfield',
                value: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; Select input polygon and input points.</p>'
            }

        ]
    }],

    items: [
        {
            xtype: 'combo',
            reference: 'inputLayerSelector',
            fieldLabel: 'Input polygon',
            disabled:false,
            displayField: 'layerTitle',
            queryMode: 'local',
            store: Ext.create('Ext.data.Store', {
                loadMask:true,
                fields: [{name: 'layerTitle', type: 'string'},{name:'layerId', type:'string'}]
            }),
            listeners:{
                'select': 'onPolygonLayerSelection'
            }

        },
        {
            xtype: 'combo',
            reference: 'searchInLayerSelector',
            fieldLabel: 'Search in',
            displayField: 'layerTitle',
            queryMode: 'local',
            store: Ext.create('Ext.data.Store', {
                loadMask:true,
                fields: [{name: 'layerTitle', type: 'string'},{name:'layerId', type:'string'}]
            }),
            listeners:{
                'select': 'onPointLayerSelection'
            }

        },
        {
            xtype:'displayfield',
            fieldLabel:'Found features',
            value:0,
            reference:'nbrOfFeatures'
        },
        {
            xtype:'displayfield',
            reference:'errorMessageField',
            value:''
        },
        {
            xtype:'button',
            text:'search',
            scale:'large',
            handler:'performSearch'
        }
    ],
    buttons:[
        {
            text:'close',
            handler:'onClose',
            dock:'bottom'
        }
    ]
});