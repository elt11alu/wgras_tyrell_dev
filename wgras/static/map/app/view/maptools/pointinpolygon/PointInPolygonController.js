/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.maptools.pointinpolygon.PointInPolygonController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.pointinpolygon',

    requires: [
        'VramApp.view.maptools.createNewLayer.withGeometry.CreateNewLayerWindow'
    ],

    vectorLayers: [],
    listenerKey: false,

    init: function () {
        this.initStore();
    },

    performSearch: function () {
        var me = this, loadMask = VramApp.utils.Loader.getLoader(this.getView(), 'Searching...');
        if (!this.polygonLayerId) {
            this.showErrorMessage('Please select input layer');
        }
        else if (!this.pointLayerId) {
            this.showErrorMessage('Please select layer to search in');
        } else {
            loadMask.show();
            console.log(this.polygonLayerId)
            console.log(this.pointLayerId)
            $.ajax({
                timeout: 20000,
                url: 'process/within',
                type: "POST",
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({pointLayerId: me.pointLayerId, polygonLayerId: me.polygonLayerId}),
                crossDomain: true
            }).done(function (json) {
                loadMask.hide()
                me.lookupReference('nbrOfFeatures').setValue(json.features.length);
                console.log(json)
                me.askToSaveLayer(me.geoJsonToOlFeatures(json.features), me.pointLayerId);
            }).fail(function (jqXHR) {
                loadMask.hide()
                fail(jqXHR)

            });
        }


    },

    removeSelectionListener: function () {
        this.listenerKey ? ol.Observable.unByKey(this.listenerKey) : false;
    },

    clearMap: function () {
        this.polygonLayerId ? VramApp.MapUtils.getLayerByDBId(this.polygonLayerId).setVisible(false) : false;
        this.pointLayerId ? VramApp.MapUtils.getLayerByDBId(this.pointLayerId).setVisible(false) : false;
    },

    initStore: function () {
        var inputStore = this.lookupReference('inputLayerSelector').getStore();
        var searchInStore = this.lookupReference('searchInLayerSelector').getStore();
        inputStore.removeAll();
        searchInStore.removeAll();
        var layers = VramApp.MapUtils.getLayers();
        layers.forEach(function (item) {
            if (item.get('dataType') === VramApp.GeometryDataTypes.LayerTypes.WFS) {
                this.vectorLayers.push(item);
                inputStore.add({layerTitle: item.get('title'), layerId: item.get('layerId'), 'layerIdentifier':item.get('layerIdentifier')});
                searchInStore.add({layerTitle: item.get('title'), layerId: item.get('layerId'), 'layerIdentifier':item.get('layerIdentifier')});
            }
        }, this);


    },

    onPolygonLayerSelection: function (combo, rec) {
        this.showErrorMessage('');
        var layerId = rec.get('layerId');
        var layerTitle = rec.get('layerTitle');
        if (layerId === this.pointLayerId) {
            this.showErrorMessage('Input and layer to search in can not be the same');
        } else {
            this.polygonLayerId ? VramApp.MapUtils.getLayerByDBId(this.polygonLayerId).setVisible(false) : true;
            this.polygonLayerId = rec.get('layerId');
            VramApp.MapUtils.getLayerByDBId(this.polygonLayerId).setVisible(true);
        }
    },

    onPointLayerSelection: function (combo, rec) {
        this.showErrorMessage('');
        var layerId = rec.get('layerId');
        var layerTitle = rec.get('layerTitle');
        if (layerId === this.polygonLayerId) {
            this.showErrorMessage('Input and layer to search in can not be the same');
        } else {
            this.pointLayerId ? VramApp.MapUtils.getLayerByDBId(this.pointLayerId).setVisible(false) : true;
            this.pointLayerId = rec.get('layerId');
            VramApp.MapUtils.getLayerByDBId(this.pointLayerId).setVisible(true);
        }
    },

    askToSaveLayer: function (features, ancestorLayerId) {
        var nbr, bbox, required, options, me;
        me = this;
        nbr = features.length;

        Ext.Msg.show({
            title: 'Save Changes?',
            message: 'Found '.concat(nbr, ' features, save?'),
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn === 'yes') {
                    me.showCreateLayerWindow(features, ancestorLayerId)

                }
            }
        });
    },

    showCreateLayerWindow: function (features, ancestorLayerId) {
        Ext.create({
            xtype: 'createnewlayerwithgeometrywindow',
            features: features,
            layerId: ancestorLayerId
        }).show();
    },

    geoJsonToOlFeatures: function (features) {
        var format = new ol.format.GeoJSON()
        var olFeatures = []
        $.each(features, function(index, feature){
            olFeatures.push(format.readFeature(feature, {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'}))
        });
        return olFeatures
    },

    showErrorMessage: function (message) {
        this.lookupReference('errorMessageField').setValue('<span style="color: red">' + message + '</span>')
    },

    onClose: function () {
        this.removeSelectionListener();
        this.clearMap();
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close();
    }
});