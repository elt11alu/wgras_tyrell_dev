/**
 * Created by antonlu66 on 11/23/2015.
 */
Ext.define('VramApp.view.maptools.pointinpolygon.PointInPolygonModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.pointinpolygon',

    stores: {
        /*
        A declaration of Ext.data.Store configurations that are first processed as binds to produce an effective
        store configuration. For example:

        users: {
            model: 'PointInPolygon',
            autoLoad: true
        }
        */
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});