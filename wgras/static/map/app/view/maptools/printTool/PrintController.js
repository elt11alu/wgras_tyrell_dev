Ext.define('VramApp.view.maptools.printTool.PrintController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.printcontroller',

    init: function () {

    },

    onCreatePdf: function (btn) {
        var me = this;
        var loader = VramApp.utils.Loader.getLoader(me.getView(), 'Generating pdf..');
        loader.show();
        window.setTimeout(function () {
            loader.hide();
        }, 3000);

        try {
                var pdf, values, orientation;
                values = me.getFormValues();
                me.getMainIcon()
                pdf = me.getPdfDoc(me.getPdfOrientation(values));
                pdf.setFontSize(20);
                pdf.setTextColor(100);
                pdf.text(20, 20, me.getPdfTitle(values));
                pdf.setFontSize(10);
                pdf.setTextColor(150);
                pdf.text(20, 25, '-' + me.getPdfSubtitle(values));
                pdf.addImage(me.getCanvasData(), 'JPEG', 0, 30, 297, 0);
                me.savePdf(pdf, me.getPdfFileName(values));

        } catch (err) {
            Ext.toast({
                html: '<p><i class="fa fa-exclamation-circle" aria-hidden="true"></i>&nbsp Your browser does not seem to support this functionality.</p>',
                title: 'Print Map',
                width: 200,
                align: 't',
                anchor: me.getView()
            });
            console.warn(err)
        }


    },

    savePdf: function (pdf, fileName) {
        pdf.save(fileName);
    },

    getPdfDoc: function (orientation) {
        return new jsPDF(orientation, undefined, 'a4');
    },

    getCanvasData: function () {
        var canvas;
        canvas = document.getElementsByTagName('canvas')[0]
        return canvas.toDataURL('image/png');
    },

    getPdfOrientation: function (formValues) {
        return formValues.printMapOrientation || 'landscape';
    },

    getPdfTitle: function (values) {
        return values.mapTitle || 'WGRAS';
    },

    getPdfSubtitle: function (values) {
        return values.mapSubTitle || 'Web GIS for Risk Assessment'
    },

    getPdfFileName: function (values) {
        return values.fileName || 'MapPdf';
    },

    getFormValues: function () {
        return this.lookupReference('printmapform').getValues();

    },

    getMainIcon: function () {
        var img = document.getElementById('mainIconImage').src;
        var imgData = 'data:image/png;base64,'+ Base64.encode(img);
        console.log(imgData)
    },

    onCancel: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close()
    }
});
