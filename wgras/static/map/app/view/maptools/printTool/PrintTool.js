Ext.define('VramApp.view.maptools.printTool.Printool', {
    extend: 'Ext.window.Window',
    requires: [
        'VramApp.view.maptools.printTool.PrintController',
    ],
    xtype: 'printtool',
    alias: 'widget.printtool',
    controller: 'printcontroller',
    autoShow: true,
    resizable:false,
    closable:false,
    width: 300,
    title: 'Print map',

    items: [{
        xtype: 'form',
        reference: 'printmapform',
        scrollable: true,
        bodyPadding:10,
        width: 290,

        fieldDefaults: {
            labelAlign: 'left',
            labelWidth: 70,
            msgTarget: 'side'
        },

        items: [{
            xtype: 'fieldset',
            title: 'Map details',
            defaultType: 'textfield',
            defaults: {
                anchor: '95%'
            },

            items: [{
                xtype:'displayfield',
                value: 'Personalize the map, if left blank default values will be used'
            },{
                fieldLabel: 'File name',
                name: 'fileName',
                reference: 'fileNameField',
                emptyText: 'MapPdf'
            },{
                fieldLabel: 'Map title',
                name: 'mapTitle',
                reference: 'mapTitleField',
                emptyText: 'WGRAS'
            }, {
                fieldLabel: 'Subtitle',
                name: 'mapSubTitle',
                reference: 'mapSubTitleField',
                emptyText: 'Web GIS for Risk Assessment'
            },{
                fieldLabel:'Orientation',
                xtype:'combo',
                name:'printMapOrientation',
                forceSelection:true,
                emptyText:'landscape',
                queryMode:'local',
                store:['landscape','portrait']
            }
        ]
        }],
        buttons: [{
            text: 'Pdf',
            margin:3,
            disabled: true,
            formBind: true,
            handler: 'onCreatePdf'
        },
        {
            text: 'Cancel',
            margin:3,
            handler: 'onCancel'
        }]
    }]
});
