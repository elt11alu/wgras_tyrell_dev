/**
 * Created by antonlu66 on 11/16/2015.
 */
Ext.define('VramApp.view.maptools.querybyattribute.QueryByAttributeController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.querybyattribute',


    operatorValue: '',
    layerAttribute: '',
    attributeValue: '',
    queryExpression: '',
    attributeDataType: '',
    tmpLayId: '',

    init: function (view) {
        this.addListenerToCombo();
        this.addListenerToGrid();
        this.addListenerToValueField();
    },

    addListenerToValueField: function () {
        this.lookupReference('valueField').on('keyup', function (field, event) {
            this.attributeValue = field.getValue();
            this.setExpression(field.getValue())
        }, this);
    },

    addListenerToCombo: function () {
        this.lookupReference('layerSelector').on('select', this.onLayerSelect, this);
    },

    onLayerSelect: function (combo, record) {
        var layerId;
        if (this.layer) {
            this.layer.setVisible(false);
        }
        layerId = record.get('layerId');
        this.layerId = layerId;

        this.layer = VramApp.MapUtils.getLayerByDBId(layerId);
        this.layer.setVisible(true);

        this.loadAttributesByLayerId(this.layer, layerId);
    },

    loadAttributesByLayerId: function (layer, layerId) {
        var store = this.getViewModel().getStore('attributeStore');
        store.load({

            url: '/resource/attributes/'.concat(layerId)
        })

    },

    addListenerToGrid: function () {
        var grid = this.lookupReference('attributeGrid');
        grid.getSelectionModel().on('select', this.onAttributeSelection, this);
    },

    onAttributeSelection: function (selmodel, record, index, eOpts) {
        var attr = record.get('name');
        var dataType = record.get('attributeType');
        this.attributeType = dataType;
        this.layerAttribute = attr;
        this.setValidationForTextField(dataType);
        this.setExpression(attr);
    },
    onOperatorSelection: function (btn) {
        this.operatorValue = btn.operatorExpr;
        this.setExpression(btn.operatorExpr);
    },

    setExpression: function (value) {
        var field = this.lookupReference('expressionField');
        this.queryExpression = this.layerAttribute + this.operatorValue + this.attributeValue;
        field.setValue(this.queryExpression);
    },

    onSubmit: function (btn) {

        if (this.attributeValue.length > 0 && this.lookupReference('valueField').validate()) {
            var attrValue = this.attributeValue;

            if (this.layerAttribute.length > 0) {
                var attribute = this.layerAttribute;

                if (this.operatorValue.length > 0) {
                    var operator = this.operatorValue;
                    this.prepareQuery(operator, attribute, attrValue);


                } else {
                    this.setErrorMessage('Please, use a logical operator.');
                }

            } else {
                this.setErrorMessage('Please, use an attribute from the list.');
            }

        } else {
            this.setErrorMessage('Please, use right format for attribute value.');
        }

    },

    prepareQuery: function (op, attr, attrVal) {
        var dataType, me, loader;
        me = this;
        loader = VramApp.utils.Loader.getLoader(this.getView(), 'Query...');
        $.ajax({
            url: '/process/querybyattribute',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({
                layerId: me.layerId,
                attributeName: attr,
                attributeValue: attrVal,
                operator: op
            }),
            context: me,
            dataType: 'json',
            beforeSend: function () {
                loader.show();
            },
            success: function (data) {
                loader.hide();
                this.onSuccessFullQuery(data)
            },
            error: function (data) {
                loader.hide();
                console.warn('Invalid request!')
            }
        })
    },

    onSuccessFullQuery: function(data){
        var format, features;
        format = new ol.format.GeoJSON();
        features = format.readFeatures(data.features, {dataProjection:'EPSG:4326', featureProjection: 'EPSG:3857'});
        this.promptSave(features);
    },

    setValidationForTextField: function (dataType) {
        var validator;
        if (dataType === 'String') {
            validator = function (val) {
                var reg = new RegExp("^[a-zA-Z-_ ]+$");
                return reg.test(val) ? true : 'Must be text';

            }
        } else if (dataType === 'Integer' || dataType === 'Float' || dataType==='Real') {
            validator = function (val) {
                var reg = new RegExp("^[0-9]+$");
                return reg.test(val) ? true : 'Must be number';
            }
        }
        this.lookupReference('valueField').validator = validator;
    },

    promptSave: function (features) {
        var nbrOfFeatures = features.length;
        Ext.MessageBox.show({
            title: 'Save layer?',
            msg: 'Query returned '.concat(nbrOfFeatures, ' features.', ' Do you wish to save to a new layer? If no, the layer will still be added to the map as a temporary layer.'),
            buttons: Ext.MessageBox.YESNO,
            scope: this,
            fn: function (btn) {
                this.afterPrompt(btn, features)
            },
            icon: Ext.MessageBox.QUESTION
        });
    },

    afterPrompt: function (btn, features) {
        if (btn === 'yes') {
            this.showCreateLayerWindow(features)
        } else {
            this.addAsTempLayer(features)
        }
    },

    addAsTempLayer: function (features) {
        var layer = new ol.layer.Vector({
            source: new ol.source.Vector({features: features})

        });
        var required = {
            layer: layer,
            layerTitle: 'temp_query_lyr',
            layerName: 'temp_query_lyr'
        };
        var options = {
            visible: true,
            folder: 'Tmp Query'
        };
        VramApp.MapUtils.addWFSOverlay(required, options);
    },

    showCreateLayerWindow: function (features) {
        var ancestorLayerId = this.layerId;
        Ext.create({
            xtype: 'createnewlayerwithgeometrywindow',
            features: features,
            layerId: ancestorLayerId
        }).show();
    },

    setErrorMessage: function (message) {
        var errorField = this.lookupReference('queryMessage');
        errorField.setValue('<span style="color:red">' + message + '</span>');

    },

    setQueryMessage: function (message) {
        var queryField = this.lookupReference('queryMessage');
        queryField.setValue('<span style="color:cadetblue">' + message + '</span>');
    },

    onClose: function () {
        this.fireEvent('toggleHeadPanelButtons', false);
        this.getView().close();
    }
});
