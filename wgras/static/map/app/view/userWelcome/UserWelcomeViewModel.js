/**
 * Created by anton on 2016-09-12.
 */

Ext.define('VramApp.view.userWelcome.UserWelcomeViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.userwelcomeviewmodel',

    data:
        {
            informationText: '<p><i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>&nbsp; The following list shows your available layers. Search by category in field below</p>',
            user:''
        },

    stores: {
        vector: {

            fields: [{name: 'layerName'}, {name: 'category'}, {name: 'description'}],
            autoLoad: true,

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/metadata/wfs',

                reader: {
                    type: 'json',
                    rootProperty: 'layers'
                }

            }
        },

        raster: {

            autoLoad: true,
            fields: [{name: 'layerName'}, {name: 'category'}, {name: 'description'}],

            proxy: {
                type: 'ajax',
                headers: {
                    'Accept': 'application/json'
                },
                url: '/metadata/wms',

                reader: {
                    type: 'json',
                    rootProperty: 'layers'
                }

            }


        }
    }
});

