/**
 * Created by anton on 8/9/16.
 */

$('document').ready(function () {


    var main = function () {
        initiateTooltip();
        onViewMessage();
        onReplyToMessage();
        onStartNewConversation();
        onDeleteConversation();
        window.setInterval(function () {
            setConversationUpdate();
        }, 3000);




    };

    var initiateTooltip = function () {
        $('[data-toggle="tooltip"]').tooltip({
            delay: {"show": 75, "hide": 50}
        })
    };

    var onViewMessage = function () {
        $('a.messagelink').click(function () {
            var modal = $('#messageModal');
            var conversationId = this.id.split('conversation-')[1];
            var success = function (data) {
                modal.on('show.bs.modal', function (event) {
                    var messages, current_user_name, messageTextArea;
                    messages = data.conversation.messages;
                    current_user_name = $("#current-user-name").val();
                    modal.find('#message-modal-conversation-title').val(data.conversation.title)
                    modal.find('#message-modal-conversation-id').val(data.conversation.conversationId)
                    modal.find('#message-modal-reciever-id').val(messages[0].sender.userId)
                    messageTextArea = modal.find('#message-modal-messagetext');

                    messageTextArea.text('')
                    $.each(messages, function (index, val) {
                        var username = val.sender.username === current_user_name ? 'You' : val.sender.username
                        messageTextArea.append('<b>'.concat(username, ' wrote:', '</b><br>'))
                        messageTextArea.append(val.message)
                        messageTextArea.append('<br><br>')
                    });
                });
                modal.modal('show')
            };
            var fail = function (resp) {

            };
            GETJson('/conversation/'.concat(conversationId), success, fail);
        });
    };

    var onReplyToMessage = function () {
        $('#message-modal-send-btn').click(function (event) {

            var message, recieverId, senderId, conversationId;
            event.preventDefault();
            message = $('#message-modal-reply-text').val();
            recieverId = $('#message-modal-reciever-id').val();
            senderId = $('#current-user-id').val();
            conversationId = $('#message-modal-conversation-id').val();
            if (message.length > 0) {
                var data = JSON.stringify({
                    conversationId: conversationId,
                    messageText: message,
                    recieverId: recieverId,
                    senderId: senderId
                });
                var success = function (data) {
                    updateConversation()
                };
                var fail = function (data) {

                };
                POSTJson('/conversation/message', data, success, fail);
            }

        });
    };

    var onStartNewConversation = function () {
        $('#new-message-send-btn').click(function (event) {

            var sender, message, recipient, title, data, success, fail;
            event.preventDefault();
            sender = $('#new-message-sender-id').val();
            message = $('#new-message-text').val()
            recipient = $('#new-message-reciever').val()
            title = $('#new-message-title').val()

            if (title.length > 0 && message.length > 0) {
                data = JSON.stringify({
                    conversationTitle: title,
                    messageText: message,
                    reciever: recipient,
                    senderId: sender
                });
                success = function (data) {

                };
                fail = function (data) {

                };
                POSTJson('/conversation', data, success, fail);

            } else {
                $('#new-conversation-alert').css('visibility', 'visible')
            }

        });
    };

    var onDeleteConversation = function () {
        $('.conversation-delete-btn').click(function (event) {
            var conversationId = $(this).data().conversationId;
            if (confirm('This will delete the conversation. Proceed?')) {
                var success, fail, data;

                success = function (resp) {
                    console.log(resp);
                };
                fail = function () {

                };
                data = JSON.stringify({conversationId: conversationId});
                DELETEJson('/conversation', data, success, fail)
            } else {

            }
        });
    };

    var updateConversation = function () {
        var modal, conversationId, success, fail;
        modal = $('#messageModal');
        conversationId = modal.find('#message-modal-conversation-id').val();
        success = function (data) {
            var messages, current_user_name, messageTextArea;
            if (data.conversation != null) {
                messages = data.conversation.messages;
                current_user_name = $("#current-user-name").val();
                messageTextArea = modal.find('#message-modal-messagetext');

                messageTextArea.text('');
                $.each(messages, function (index, val) {
                    var username = val.sender.username === current_user_name ? 'You' : val.sender.username;
                    messageTextArea.append('<b>'.concat(username, ' wrote:', '</b><br>'));
                    messageTextArea.append(val.message)
                    messageTextArea.append('<br><br>')
                });
            }


        };
        fail = function (resp) {

        };
        GETJson('/conversation/'.concat(conversationId), success, fail);
    };

    var setConversationUpdate = function () {
        var open = ($("#messageModal").data('bs.modal') || {}).isShown ? true : false;
        if (open) {
            updateConversation()
        }
    };

    var updateConversationsList = function () {
        var success, fail, conversationList, listItem;
        conversationList = $('#conversation-panel-list-group');

        success = function (resp) {
            console.log(resp.conversations);

            $.each(resp.conversations, function(index, conversation){
                listItem = '<div class="list-group-item">' +
                '<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>' +
                '&nbsp;&nbsp; ' +
                '<a href="#" class="messagelink" id=conversation-'+conversation.conversationId+'>'+conversation.title+'</a>' +
                '&nbsp;&nbsp; &nbsp;&nbsp; ' +
                '<button type="button" class="btn btn-default conversation-delete-btn" data-conversation-id='+conversation.conversationId+'><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>' +
                '</div>';
                conversationList.html(listItem)
            });




        };
        fail = function () {
        };


        GETJson('/conversations', success, fail)


    };

    var GETJson = function (url, success, fail) {
        $.getJSON(url)
            .done(function (resp, textStatus, jqXHR) {
                success(resp)
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown)
                fail()
            });

    };

    var POSTJson = function (url, data, success, fail) {
        $.ajax({
            type: "POST",
            contentType: 'application/json',
            url: url,
            data: data,
            success: function (resp) {
                success(resp)
            },
            fail: function () {
                fail()
            },
            dataType: 'json'
        });
    };

    var DELETEJson = function (url, data, success, fail) {
        $.ajax({
            type: "DELETE",
            contentType: 'application/json',
            url: url,
            data: data,
            success: function (resp) {
                success(resp)
            },
            fail: function () {
                fail()
            },
            dataType: 'json'
        });
    };


    main()
});
