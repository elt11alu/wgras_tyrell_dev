from ..models import *


class AccessControl(object):

    def canReadCountry(self, user, isocode):
        if user.type == 'superadmin':
            return True

        elif user.type == 'countryadmin':
            return user.country.isocode == isocode

        elif user.type == 'countryuser':
            return user.country.isocode == isocode

        elif user.type == 'orgadmin':
            return user.organization.country.isocode == isocode

    def canWriteCountry(self, user, isocode):
        if user.type == 'superadmin':
            return True

        elif user.type == 'countryadmin':
            return False

        elif user.type == 'countryuser':
            return False

        elif user.type == 'orgadmin':
            return False

    def canReadOrganization(self, user, orgid):
        if user.type == 'superadmin':
            return True

        elif user.type == 'countryadmin':
            organization = Organization.query.filter_by(orgid=orgid).first()
            return user.country.isocode == organization.country.isocode

        elif user.type == 'countryuser':
            organization = Organization.query.filter_by(orgid=orgid).first()
            return user.organization.country.isocode == organization.country.isocode

        elif user.type == 'orgadmin':
            return True

    def canWriteOrganization(self, user, isocode=None, orgid=None):
        if isocode is not None:
            if user.type == 'superadmin':
                return True

            elif user.type == 'countryadmin':
                return user.country.isocode == isocode

            elif user.type == 'countryuser':
                return False

            elif user.type == 'orgadmin':
                return user.organization.country.isocode == isocode

        elif orgid is not None:
            if user.type == 'superadmin':
                return True

            elif user.type == 'countryadmin':
                org = Organization.query.filter_by(orgid=orgid).first()
                return user.country.isocode == org.country.isocode

            elif user.type == 'countryuser':
                return False

            elif user.type == 'orgadmin':
                org = Organization.query.filter_by(orgid=orgid).first()
                return user.organization.country.isocode == org.country.isocode

    def canReadUser(self, user, userId=None):
        if user.type == 'superadmin':
            return True

        elif user.type == 'countryadmin':
            return True

        elif user.type == 'countryuser':
            return True

        elif user.type == 'orgadmin':
            return True

    def canCreateSuperAdmin(self, user):
        if user.type == 'superadmin':
            return True
        else:
            return False

    def canCreateCountryAdmin(self, user, isocode):
        if user.type == 'superadmin':
            return True
        elif user.type == 'countryadmin':
            return user.country.isocode == isocode
        else:
            return False

    def canCreateCountryUser(self, user, isocode):
        if user.type == 'superadmin':
            return True
        elif user.type == 'countryadmin':
            return user.country.isocode == isocode
        else:
            return False

    def canCreateOrgAdmin(self, user, orgid):
        if user.type == 'superadmin':
            return True
        elif user.type == 'countryadmin':
            org = Organization.query.filter_by(orgid=orgid).first()
            return user.country.isocode == org.country.isocode
        else:
            return False

    def canDeleteSuperadmin(self, user):
        if user.type == 'superadmin':
            return True
        return False

    def canDeleteCountryadmin(self, user, userid):
        if user.type == 'superadmin':
            return True
        else:
            return False

    def canDeleteCountryUser(self, user, userid):
        user_to_delete = User.query.filter_by(userid=userid).first()
        if user.type == 'superadmin':
            return True
        elif user.type == 'countryadmin':
            return user.country.isocode == user_to_delete.country.isocode
        else:
            return False

    def canDeleteOrgAdmin(self, user, userid):
        user_to_delete = User.query.filter_by(userid=userid).first()
        if user.type == 'superadmin':
            return True
        elif user.type == 'countryadmin':
            return user.country.isocode == user_to_delete.organization.country.isocode
        else:
            return False

    def canReadWorkspace(self, user, wsid):
        workspace = Workspace.query.filter_by(wsid=wsid).first()
        return workspace.canBeReadBy(user)

    def canWriteWorkspace(self, user, wsid):
        workspace = Workspace.query.filter_by(wsid=wsid).first()
        return workspace.canBeWrittenBy(user)

    def canReadWorkspaceByName(self, user, wsName):
        workspace = Workspace.query.filter_by(name=wsName).first()
        return workspace.canBeReadBy(user)

    def canWriteWorkspaceByName(self, user, wsName):
        workspace = Workspace.query.filter_by(name=wsName).first()
        return workspace.canBeWrittenBy(user)

    def canAccessManagerApplication(self, user):
        return user.type != 'countryuser'

