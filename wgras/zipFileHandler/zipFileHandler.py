import zipfile
import os


class ZipReader(object):
    """ Unpacks a zipfile available at the filePath """

    def __init__(self, filePath, unzipPath):
        self.filePath = filePath
        self.unzipPath = unzipPath

    def unzip(self):
        if zipfile.is_zipfile(self.filePath):
            with zipfile.ZipFile(self.filePath, "r") as z:
                z.extractall(self.unzipPath)
                z.close()
                return True
        else:
            return False

    def getAllFileNames(self):
        if self.isZipFile():
            with zipfile.ZipFile(self.filePath, "r") as z:
                print z.namelist()
                return z.namelist()
        else:
            return None

    def getUniqueFileNames(self):
        fileNames = self.getAllFileNames()
        names = []
        for name in fileNames:
            if self.looksAsShapeFile(name):
                names.append(name.split('.')[0])
        return list(set(names))

    def isZipFile(self):
        return zipfile.is_zipfile(self.filePath)

    def isShapeFile(self):
        allowed = ('dbf', 'shp', 'prj', 'sbx', 'sbn', 'shx', 'txt')
        valid = True
        for name in self.getAllFileNames():
            print name
            if not name.lower().endswith(allowed):
                valid = False
        return valid

    def looksAsShapeFile(self, fileName):
        allowed = ('dbf', 'shp', 'prj', 'sbx', 'sbn', 'shx')
        return fileName.lower().endswith(allowed)

    def remove_zip(self):
        os.remove(self.filePath)
